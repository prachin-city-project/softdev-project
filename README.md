# D-COFFEE โปรแกรมการขายกาแฟด้วยภาษา Java

เป็นโปรแกรมที่เขียนด้วยภาษา Java เป็นโปรแกรมธุรกิจร้านกาแฟ โดยใช้ apache netbeans และเชื่อม Database ด้วย Sqlite

## สารบัญ Table of Contnets

 - ขั้นตอนการติดตั้งโปรเจ็ค
 - ขั้นตอนการใช้งาน
-  ฟีเจอร์ทั้งหมดและปุ่มต่างๆในหน้า MainFrame


## ขั้นตอนการติดตั้งโปรเจ็ค

    git clone https://gitlab.com/prachin-city-project/softdev-project.git
    
    cd softdev-project

## ขั้นตอนการใช้งาน

 1. รันโปรแกรม
 2. เมื่อรันแล้วโปรแกรมจะขึ้นหน้า Login 
 3. ใส่ผู้ใช้และรหัสผ่านเพื่อเข้าสู่ระบบ
 4. เมื่อ Login สำเร็จ ระบบจะไปที่หน้า  MainFrame

    
## ฟีเจอร์ทั้งหมดและปุ่มต่างๆในหน้า MainFrame
 - **หน้าการขาย** เป็นหน้าแสดงการขายสินค้า โดยหน้านี้มีสินค้าทั้งหมดที่ขายได้ แยกเป็นหมวดหมู่ โดยมีฟังก์ชันการเลือก ความหวาน ขนาด และประเภทได้อีกแล้ว และยังสามารถค้นหาสมาชิก สมัครสมาชิก การใช้โปรโมชั่น สามารถเลือกวิธีการชำระเงินได้ และเมื่อชำระเงินแล้วก็จะมีหน้าใบเสร็จแสดงขึ้นมา
 
 - **สินค้า** เป็นหน้าแสดงสินค้า และรายละเอียดของสินค้าทั้งหมดที่มี เช่น รูปภาพ ชื่อ ราคา หมวดหมู่ และมีปุ่มเพิ่มสินค้า แก้ไขสินค้าและลบสินค้าที่ต้องการได้ รวมไปถึงยังมีแถบและปุ่มค้นหาวัตถุดิบที่ต้องการอีกด้วย
 
 - **วัตถุดิบ** เป็นหน้าแสดงวัตถุดิบที่ใช้ และรายละเอียดต่างๆ เช่น รูปภาพ ชื่อ จำนวนขั้นต่ำ จำนวนคงเหลือ หน่วย รวมไปถึงบอกสถานะของวัตถุดิบ หากหมด หรือเหลือน้อย และมีปุ่มเพิ่ม แก้ไขและลบ วัตถุดิบที่ต้องการได้ รวมไปถึงยังมีแถบและปุ่มค้นหาสินค้าที่ต้องการอีกด้วย

- **เช็ควัตถุดิบ** เป็นหน้าแสดงรายละเอียดการเช็ควัตถุดิบโดยสามารถเลือกดูได้ว่าพนักงานคนไหนเป็นคนเช็ควัตถุดิบในวันที่และเวลาที่เลือก และแสดงรายละเอียดของวัตถุดิบที่พนักงานเช็คได้ เช่น ชื่อ จำนวนขั้นต่ำ จำนวนคงเหลือ และจำนวนปัจจุบัน
 และมีปุ่มเพิ่ม แก้ไขและลบ วัตถุดิบที่ต้องการได้

- **นำเข้าวัตถุดิบ**เป็นหน้าที่แสดงการประวัติการนำเข้าวัตถุดิบและสามารถนำเข้าวัตถุดิบได้ รวมไปถึงสามารถดูรายละเอียดของวัตถุดิบที่นำเข้าได้อีกด้วย

- **ลูกค้า** เป็นหน้าแสดงรายละเอียดเกี่ยวกับลูกค้าที่เป็นสมาชิก โดยมีข้อมูลเกี่ยวกับชื่อ เบอร์โทรศัพท์และแต้มสะสม
 และมีปุ่มเพิ่ม แก้ไขและลบ ลูกค้าที่ต้องการได้ รวมไปถึงยังมีแถบและปุ่มค้นหาลูกค้าที่ต้องการอีกด้วย

- **พนักงาน** เป็นหน้าแสดงรายละเอียดเกี่ยวกับพนักงานทั้งหมด ไม่ว่าจะเป็น ชื่อ เพศ เบอร์โทรศัพท์ ผู้ใช้ รหัสผ่าน และ หน้าที่ของพนักงาน  และมีปุ่มเพิ่ม แก้ไขและลบ พนักงานที่ต้องการได้ รวมไปถึงยังมีแถบและปุ่มค้นหาพนักงานที่ต้องการอีกด้วย

- **หมวดหมู่สินค้า** เป็นหน้าที่แสดงชื่อของหมวดหมู่สินค้าที่มีในร้าน  และมีปุ่มเพิ่ม แก้ไขและลบ หมวดหมู่สินค้าที่ต้องการได้ รวมไปถึงยังมีแถบและปุ่มค้นหาหมวดหมู่สินค้าที่ต้องการอีกด้วย

- **คู่ค้า**เป็นหน้าที่แสดงรายละเอียดของคู่ค้าที่เกี่ยวข้องกับร้าน และมีปุ่มเพิ่ม แก้ไขและลบ คู่ค้าที่ต้องการได้ รวมไปถึงยังมีแถบและปุ่มค้นหาคู่ค้าที่ต้องการอีกด้วย

- **หน้าที่**เป็นหน้าที่แสดงรายละเอียดของหน้าที่ที่มีในร้าน และมีปุ่มเพิ่ม แก้ไขและลบ หน้าที่ที่ต้องการได้

- **ประวัติการขาย** เป็นหน้าแสดงรายละเอียดต่างๆในใบเสร็จ ไม่ว่าจะเป็น วันที่ เวลา พนักงานที่ขาย รายละเอียดลูกค้าสมาชิก ส่วนลด ยอดสุทธิ วิธีการชำระเงิน
และข้อมูลเกี่ยวกับชื่อสินค้าที่สั่ง รวมถึงสามารถเลือกวันที่ต้องการดูได้

- **รายจ่าย** เป็นหน้าแสดงรายละเอียดของรายจ่าย ไม่ว่าจะเป็น วันที่และเวลา ชื่อ ยอดรวมของรายจ่าย และถ้ากดที่ปุ่มดูรายละเอียดจะสามารถดูที่มาของรายจ่ายได้อีกด้วย
- 
- **เข้าออก-งาน** เป็นหน้าแสดงการเช็คชื่อเข้าใช้งาน เพื่อให้พนักงานใช้เช็คเวลาเข้าและออกงาน โดยรายละเอียดจะมี ชื่อของพนักงาน วันที่ เวลาเข้างาน เวลาออกงาน
และสถานะการมาตรงเวลาหรือมาสายของพนักงานได้

- **จ่ายเงินเดือน**เป็นหน้าแสดงชั่วโมงการทำงานของพนักงานเพื่อคำนวณเป็นเงินเดือนและจ่ายเงินเดือนให้พนักงาน

- **รายงาน** เป็นหน้าที่แสดงรายงานต่างๆที่น่าสนใจเกี่ยวกับร้าน ประกอบไปด้วย สินค้าขายดี สินค้าใกล้หมด รายจ่ายทั้งหมด ยอดขาย และกำไร-ขาดทุน โดยแสดงข้อมูลเป็นตารางและกราฟเพื่อให้สามารถนำไปเปรียบเทียบได้

- **ออกจากระบบ** กดเพื่อออกจากระบบ
