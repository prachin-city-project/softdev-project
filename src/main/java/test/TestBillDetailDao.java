/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package test;

import com.pattarapol.databaseproject.dao.BillDao;
import com.pattarapol.databaseproject.dao.BillDetailDao;
import com.pattarapol.databaseproject.dao.MaterialDao;
import com.pattarapol.databaseproject.model.Bill;
import com.pattarapol.databaseproject.model.BillDetail;
import com.pattarapol.databaseproject.model.Material;
import java.util.List;

/**
 *
 * @author Windows10
 */
public class TestBillDetailDao {
    public static void main(String[] args) {
        BillDetailDao bdd = new BillDetailDao();
        for(BillDetail bd:bdd.getAll()){
            System.out.println(bd);
        }
        BillDao bd = new BillDao();
        MaterialDao md = new MaterialDao();
        List<Material> materials = md.getAll();
        Material material0 = materials.get(0);
        Bill bill = bd.get(1);
        BillDetail newBillDetail = new BillDetail(material0.getId(), material0.getName(), 3, 25, 75, bill.getId());
        bdd.save(newBillDetail);
    }
}
