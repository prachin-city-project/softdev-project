/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package test;

import com.pattarapol.databaseproject.dao.CheckMaterialItemDao;
import com.pattarapol.databaseproject.dao.CheckStockDao;
import com.pattarapol.databaseproject.dao.MaterialDao;
import com.pattarapol.databaseproject.model.CheckMaterialItem;
import com.pattarapol.databaseproject.model.CheckStock;
import com.pattarapol.databaseproject.model.Material;
import java.util.List;

/**
 *
 * @author ASUS
 */
public class testCheckStock {
    public static void main(String[] args) {
        CheckMaterialItemDao cmt = new CheckMaterialItemDao();
        for(CheckMaterialItem cm :cmt.getAll()){
            System.out.println(cm);
        }
        MaterialDao mat = new MaterialDao();
        List<Material> materials = mat.getAll();
        Material material0 = materials.get(1);
        CheckStockDao cs = new CheckStockDao();
        CheckStock checkStock = cs.get(1);
        CheckMaterialItem newMeCheckMaterialItem = new CheckMaterialItem(material0.getRemain(), 10, checkStock.getId(), material0.getId());
        cmt.save(newMeCheckMaterialItem);
    }
    
//    ReceiptDetailDao rdd = new ReceiptDetailDao();
//        for(ReceiptDetail rd :rdd.getAll()){
//            System.out.println(rd);
//        }
//         ProductDao pd = new ProductDao();
//        List<Product> products = pd.getAll();
//        Product product0 = products.get(0);
//        ReceiptDao rd = new ReceiptDao();
//        Receipt receipt = rd.get(1);
//        ReceiptDetail newReceiptDetail = new ReceiptDetail(product0.getId(),product0.getName()
//                ,product0.getPrice(),1,product0.getPrice()*1,receipt.getId());
//        rdd.save(newReceiptDetail);
//    }

}
