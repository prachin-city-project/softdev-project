/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package test;

import com.pattarapol.databaseproject.dao.ExpensesDao;
import com.pattarapol.databaseproject.dao.ExpensesDetailDao;
import com.pattarapol.databaseproject.model.Expenses;
import com.pattarapol.databaseproject.model.ExpensesDetail;

/**
 *
 * @author Windows10
 */
public class TestExpensesDetailDao {

    public static void main(String[] args) {
        ExpensesDetailDao edd = new ExpensesDetailDao();
        for(ExpensesDetail ed:edd.getAll()){
            System.out.println(ed);
        }
        ExpensesDao ed = new ExpensesDao();
        Expenses expenses = ed.get(1);
        ExpensesDetail newExpensesDetail = new ExpensesDetail("ค่าเช่าร้าน", 15000, expenses.getId());
        edd.save(newExpensesDetail);
    }
}
