/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JDialog.java to edit this template
 */
package com.pattarapol.softdevproject.component;

import com.pattarapol.databaseproject.model.Product;
import java.awt.Frame;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author ASUS
 */
public class BuyProductDetail extends javax.swing.JDialog {

    private ProductItemPanel productItemPanel;
    private ArrayList<BuyProductable> subscribers = new ArrayList<>();
    private Product product;
    private String name;
    private String size;
    private String type;
    private String sweetLevel;
    private float price;

    /**
     * Creates new form BuyProduct
     */
    public BuyProductDetail(ProductItemPanel productItemPanel, Product product) {
        super((Frame) null, true);
        this.productItemPanel = productItemPanel;
        this.product = product;
        initComponents();
        price = product.getPrice();
        lblProductName.setText(" " + product.getName());
        disableType();
        enableType();
    }

    public void disableType() {
        rbtTypeHot.setEnabled(false);
        rbtTypeCold.setEnabled(false);
        rbtTypeFrappe.setEnabled(false);
    }

    public void enableType() {
        if (product.getType().contains("H")) {
            rbtTypeHot.setEnabled(true);
        }
        if (product.getType().contains("C")) {
            rbtTypeCold.setEnabled(true);
        }
        if (product.getType().contains("F")) {
            rbtTypeFrappe.setEnabled(true);
        }
    }

    private void calculatePrice() {
        if (size.equals("เล็ก") && type.equals("ร้อน")) {
            name = product.getName() + "("+size + " " + type + " " + "หวาน " + sweetLevel+")";
            price = product.getPrice() - 10;
        } else if (size.equals("เล็ก") && type.equals("เย็น")) {
             name = product.getName() + "("+size + " " + type + " " + "หวาน " + sweetLevel+")";
            price = product.getPrice();
        } else if (size.equals("เล็ก") && type.equals("ปั่น")) {
              name = product.getName() + "("+size + " " + type + " " + "หวาน " + sweetLevel+")";
            price = product.getPrice() + 10;
        } else if (size.equals("กลาง") && type.equals("ร้อน")) {
             name = product.getName() + "("+size + " " + type + " " + "หวาน " + sweetLevel+")";
            price = product.getPrice() - 5;
        } else if (size.equals("กลาง") && type.equals("เย็น")) {
              name = product.getName() + "("+size + " " + type + " " + "หวาน " + sweetLevel+")";
            price = product.getPrice() + 5;
        } else if (size.equals("กลาง") && type.equals("ปั่น")) {
              name = product.getName() + "("+size + " " + type + " " + "หวาน " + sweetLevel+")";
            price = product.getPrice() + 15;
        } else if (size.equals("ใหญ่") && type.equals("ร้อน")) {
           name = product.getName() + "("+size + " " + type + " " + "หวาน " + sweetLevel+")";
            price = product.getPrice() ;
        } else if (size.equals("ใหญ่") && type.equals("เย็น")) {
              name = product.getName() + "("+size + " " + type + " " + "หวาน " + sweetLevel+")";
            price = product.getPrice() + 10;
        } else if (size.equals("ใหญ่") && type.equals("ปั่น")) {
              name = product.getName() + "("+size + " " + type + " " + "หวาน " + sweetLevel+")";
            price = product.getPrice() + 20;
        } else {
            price = product.getPrice();
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        rbtSize = new javax.swing.ButtonGroup();
        rbtType = new javax.swing.ButtonGroup();
        rbtSweet = new javax.swing.ButtonGroup();
        jPanel1 = new javax.swing.JPanel();
        rbtSmallSize = new javax.swing.JRadioButton();
        jLabel3 = new javax.swing.JLabel();
        rbtMediumSize = new javax.swing.JRadioButton();
        btnConfirm = new javax.swing.JButton();
        rbtBigSize = new javax.swing.JRadioButton();
        btnCancel = new javax.swing.JButton();
        rbtTypeHot = new javax.swing.JRadioButton();
        rbtTypeCold = new javax.swing.JRadioButton();
        rbtTypeFrappe = new javax.swing.JRadioButton();
        rbtSweet100 = new javax.swing.JRadioButton();
        rbtSweet0 = new javax.swing.JRadioButton();
        rbtSweet50 = new javax.swing.JRadioButton();
        jLabel1 = new javax.swing.JLabel();
        lblProductName = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jPanel1.setBackground(new java.awt.Color(204, 204, 255));

        rbtSize.add(rbtSmallSize);
        rbtSmallSize.setFont(new java.awt.Font("TH SarabunPSK", 0, 24)); // NOI18N
        rbtSmallSize.setText("เล็ก");
        rbtSmallSize.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbtSmallSizeActionPerformed(evt);
            }
        });

        jLabel3.setFont(new java.awt.Font("TH SarabunPSK", 0, 24)); // NOI18N
        jLabel3.setText("ความหวาน :");

        rbtSize.add(rbtMediumSize);
        rbtMediumSize.setFont(new java.awt.Font("TH SarabunPSK", 0, 24)); // NOI18N
        rbtMediumSize.setText("กลาง(+5)");
        rbtMediumSize.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbtMediumSizeActionPerformed(evt);
            }
        });

        btnConfirm.setBackground(new java.awt.Color(204, 255, 204));
        btnConfirm.setFont(new java.awt.Font("TH SarabunPSK", 0, 24)); // NOI18N
        btnConfirm.setText("ยืนยัน");
        btnConfirm.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnConfirmActionPerformed(evt);
            }
        });

        rbtSize.add(rbtBigSize);
        rbtBigSize.setFont(new java.awt.Font("TH SarabunPSK", 0, 24)); // NOI18N
        rbtBigSize.setText("ใหญ่(+10)");
        rbtBigSize.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbtBigSizeActionPerformed(evt);
            }
        });

        btnCancel.setBackground(new java.awt.Color(255, 204, 204));
        btnCancel.setFont(new java.awt.Font("TH SarabunPSK", 0, 24)); // NOI18N
        btnCancel.setText("ยกเลิก");
        btnCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelActionPerformed(evt);
            }
        });

        rbtType.add(rbtTypeHot);
        rbtTypeHot.setFont(new java.awt.Font("TH SarabunPSK", 0, 24)); // NOI18N
        rbtTypeHot.setText("ร้อน(-10)");
        rbtTypeHot.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbtTypeHotActionPerformed(evt);
            }
        });

        rbtType.add(rbtTypeCold);
        rbtTypeCold.setFont(new java.awt.Font("TH SarabunPSK", 0, 24)); // NOI18N
        rbtTypeCold.setText("เย็น");
        rbtTypeCold.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbtTypeColdActionPerformed(evt);
            }
        });

        rbtType.add(rbtTypeFrappe);
        rbtTypeFrappe.setFont(new java.awt.Font("TH SarabunPSK", 0, 24)); // NOI18N
        rbtTypeFrappe.setText("ปั่น(+10)");
        rbtTypeFrappe.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbtTypeFrappeActionPerformed(evt);
            }
        });

        rbtSweet.add(rbtSweet100);
        rbtSweet100.setFont(new java.awt.Font("TH SarabunPSK", 0, 24)); // NOI18N
        rbtSweet100.setText("100 %");
        rbtSweet100.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbtSweet100ActionPerformed(evt);
            }
        });

        rbtSweet.add(rbtSweet0);
        rbtSweet0.setFont(new java.awt.Font("TH SarabunPSK", 0, 24)); // NOI18N
        rbtSweet0.setText("0 %");
        rbtSweet0.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbtSweet0ActionPerformed(evt);
            }
        });

        rbtSweet.add(rbtSweet50);
        rbtSweet50.setFont(new java.awt.Font("TH SarabunPSK", 0, 24)); // NOI18N
        rbtSweet50.setText("50 %");
        rbtSweet50.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbtSweet50ActionPerformed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("TH SarabunPSK", 0, 24)); // NOI18N
        jLabel1.setText("ขนาด :");

        lblProductName.setFont(new java.awt.Font("TH SarabunPSK", 0, 36)); // NOI18N
        lblProductName.setText("รายการสินค้า");

        jLabel2.setFont(new java.awt.Font("TH SarabunPSK", 0, 24)); // NOI18N
        jLabel2.setText("ประเภท :");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(23, 23, 23)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel3)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(rbtSweet0, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(rbtSweet50, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(44, 44, 44)
                                .addComponent(rbtSweet100, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(rbtTypeHot, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(rbtTypeCold, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(44, 44, 44)
                                .addComponent(rbtTypeFrappe, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(lblProductName, javax.swing.GroupLayout.PREFERRED_SIZE, 135, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(rbtSmallSize, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(rbtMediumSize, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGap(44, 44, 44)
                                .addComponent(rbtBigSize, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(141, 141, 141)
                        .addComponent(btnCancel, javax.swing.GroupLayout.PREFERRED_SIZE, 92, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(30, 30, 30)
                        .addComponent(btnConfirm, javax.swing.GroupLayout.PREFERRED_SIZE, 92, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(47, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblProductName, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(rbtSmallSize)
                    .addComponent(rbtMediumSize)
                    .addComponent(rbtBigSize)
                    .addComponent(jLabel1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(rbtTypeHot)
                    .addComponent(rbtTypeCold)
                    .addComponent(rbtTypeFrappe)
                    .addComponent(jLabel2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(rbtSweet50)
                    .addComponent(rbtSweet100)
                    .addComponent(rbtSweet0)
                    .addComponent(jLabel3))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnConfirm, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnCancel, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(19, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void rbtTypeHotActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbtTypeHotActionPerformed
        type = "ร้อน";
    }//GEN-LAST:event_rbtTypeHotActionPerformed

    private void rbtTypeColdActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbtTypeColdActionPerformed
        type = "เย็น";
    }//GEN-LAST:event_rbtTypeColdActionPerformed

    private void rbtTypeFrappeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbtTypeFrappeActionPerformed
        type = "ปั่น";
    }//GEN-LAST:event_rbtTypeFrappeActionPerformed

    private void rbtSweet100ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbtSweet100ActionPerformed
        sweetLevel = "100%";
    }//GEN-LAST:event_rbtSweet100ActionPerformed

    private void rbtSweet0ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbtSweet0ActionPerformed
        sweetLevel = "0%";    }//GEN-LAST:event_rbtSweet0ActionPerformed

    private void rbtSweet50ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbtSweet50ActionPerformed
        sweetLevel = "50%";    }//GEN-LAST:event_rbtSweet50ActionPerformed

    private void rbtSmallSizeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbtSmallSizeActionPerformed
        size = "เล็ก";
    }//GEN-LAST:event_rbtSmallSizeActionPerformed

    private void rbtMediumSizeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbtMediumSizeActionPerformed
        size = "กลาง";
    }//GEN-LAST:event_rbtMediumSizeActionPerformed

    private void rbtBigSizeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbtBigSizeActionPerformed
        size = "ใหญ่";
    }//GEN-LAST:event_rbtBigSizeActionPerformed

    private void btnConfirmActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnConfirmActionPerformed
        if(size == null || type == null || sweetLevel == null){
            JOptionPane.showMessageDialog(this, "กรุณาเลือกรายละเอียดสินค้า");
        }else {
            calculatePrice();
            productItemPanel.setSize(size);
            productItemPanel.setName(name);
            productItemPanel.setSweet(sweetLevel);
            productItemPanel.setType(type);
            productItemPanel.setPrice(price);
            productItemPanel.setStatus("buy");
            dispose();
        }
    }//GEN-LAST:event_btnConfirmActionPerformed

    private void btnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelActionPerformed
       productItemPanel.setStatus("cancel");
        dispose();
    }//GEN-LAST:event_btnCancelActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCancel;
    private javax.swing.JButton btnConfirm;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JLabel lblProductName;
    private javax.swing.JRadioButton rbtBigSize;
    private javax.swing.JRadioButton rbtMediumSize;
    private javax.swing.ButtonGroup rbtSize;
    private javax.swing.JRadioButton rbtSmallSize;
    private javax.swing.ButtonGroup rbtSweet;
    private javax.swing.JRadioButton rbtSweet0;
    private javax.swing.JRadioButton rbtSweet100;
    private javax.swing.JRadioButton rbtSweet50;
    private javax.swing.ButtonGroup rbtType;
    private javax.swing.JRadioButton rbtTypeCold;
    private javax.swing.JRadioButton rbtTypeFrappe;
    private javax.swing.JRadioButton rbtTypeHot;
    // End of variables declaration//GEN-END:variables
}
