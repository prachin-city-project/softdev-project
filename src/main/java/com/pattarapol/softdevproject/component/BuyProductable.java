/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.pattarapol.softdevproject.component;

import com.pattarapol.databaseproject.model.Product;



/**
 *
 * @author ASUS
 */
public interface BuyProductable {
    public void buy(Product product,String productName,String size,String type,String sweetLevel,float price, int qty);  
    
    
}
