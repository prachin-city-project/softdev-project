/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.pattarapol.softdevproject.component;

import com.pattarapol.databaseproject.model.Material;




/**
 *
 * @author ASUS
 */
public interface BuyMaterialable {
    public void buy(Material material,float matPrice, int qty);  
    
    
}
