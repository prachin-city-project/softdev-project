/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JPanel.java to edit this template
 */
package com.pattarapol.softdevproject.pos;

import com.pattarapol.softdevproject.material.*;
import com.pattarapol.databaseproject.model.Category;
import com.pattarapol.databaseproject.model.CheckMaterialItem;
import com.pattarapol.databaseproject.model.Reciept;
import com.pattarapol.databaseproject.model.Employee;
import com.pattarapol.databaseproject.model.Material;
import com.pattarapol.databaseproject.model.RecieptDetail;
import com.pattarapol.databaseproject.report.DateLabelFormatter;
import com.pattarapol.databaseproject.service.CategoryService;
import com.pattarapol.databaseproject.service.RecieptService;
import com.pattarapol.databaseproject.service.EmployeeService;
import com.pattarapol.databaseproject.service.MaterialService;
import java.awt.Font;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Properties;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.table.AbstractTableModel;
import javax.swing.text.html.CSS;
import org.jdatepicker.impl.JDatePanelImpl;
import org.jdatepicker.impl.JDatePickerImpl;
import org.jdatepicker.impl.UtilDateModel;

/**
 *
 * @author ASUS
 */
public class HistoryRecieptPanel extends javax.swing.JPanel {

    private RecieptService recieptService = new RecieptService();
    private List<Reciept> list = recieptService.getReciepts();
    private Reciept reciept;
    private List<RecieptDetail> recieptDetails;
   private AbstractTableModel model;
    private UtilDateModel model1;
    private UtilDateModel model2;
    /**
     * Creates new form MaterialFrame
     */
    public HistoryRecieptPanel() {

        initComponents();
        initDatePicker();
        tblReciept.getTableHeader().setFont(new Font("TH SarabunPSK", Font.PLAIN, 20));
        tblDetail.getTableHeader().setFont(new Font("TH SarabunPSK", Font.PLAIN, 20));
        tblReciept.setRowHeight(25);
        tblDetail.setRowHeight(25);
        LoadTableReciept();
        LoadTableDetail();

    }

    private void LoadTableDetail() {
        tblDetail.setModel(new AbstractTableModel() {

            @Override
            public String getColumnName(int column) {
                String[] columnNames = {"ชือ", "ราคา", "จำนวน", "ยอดรวม"};
                return columnNames[column];
            }

            @Override
            public int getRowCount() {
                return recieptDetails == null ? 0 : recieptDetails.size();
            }

            @Override
            public int getColumnCount() {
                return 4;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                RecieptDetail recieptDetail = recieptDetails.get(rowIndex);
                switch (columnIndex) {
                    case 0:
                        return (recieptDetail == null) ? "" : recieptDetail.getProductName();
                    case 1:
                        return (recieptDetail == null) ? "" : recieptDetail.getProductPrice();
                    case 2:
                        return (recieptDetail == null) ? "" : recieptDetail.getQty();
                    case 3:
                        return (recieptDetail == null) ? "" : recieptDetail.getTotalPrice();
                    default:
                        return "Unknown";
                }
            }

        });
    }

    private void LoadTableReciept() {
       model = new AbstractTableModel() {

            @Override
            public String getColumnName(int column) {
                String[] columnNames = {"วัน เวลา", "พนักงานขาย", "สมาชิก", "ส่วนลด", "ยอดสุทธิ", "วิธีการชำระ"};
                return columnNames[column];
            }

            @Override
            public int getRowCount() {
                return list.size();
            }

            @Override
            public int getColumnCount() {
                return 6;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                Reciept re = list.get(rowIndex);

                switch (columnIndex) {
                    case 0:
                        return re.getCreatedDate();
                    case 1:
                        return re.getEmployeeName();
                    case 2:
                                if(re.getMemberId() == 0){
                                    return "-";
                                }else{
                                    return re.getMemberName();
                                }        
                    case 3:
                        return re.getDiscount();
                    case 4:
                        return re.getTotal();
                    case 5:
                        return re.getPayment();

                    default:
                        return "Unknown";
                }
            }

        };
       tblReciept.setModel(model);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        scoHis = new javax.swing.JScrollPane();
        tblReciept = new javax.swing.JTable();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblDetail = new javax.swing.JTable();
        pnlDatePicker1 = new javax.swing.JPanel();
        pnlDatePicker2 = new javax.swing.JPanel();
        btnProcess = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();
        btnView = new javax.swing.JButton();

        setBackground(new java.awt.Color(204, 204, 255));

        scoHis.setFont(new java.awt.Font("TH SarabunPSK", 0, 20)); // NOI18N

        tblReciept.setFont(new java.awt.Font("TH SarabunPSK", 0, 20)); // NOI18N
        tblReciept.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null}
            },
            new String [] {
                "Date Time", "Employee", "Member", "Discount", "Total", "Payment"
            }
        ));
        scoHis.setViewportView(tblReciept);
        if (tblReciept.getColumnModel().getColumnCount() > 0) {
            tblReciept.getColumnModel().getColumn(0).setMinWidth(200);
            tblReciept.getColumnModel().getColumn(0).setMaxWidth(250);
        }

        tblDetail.setFont(new java.awt.Font("TH SarabunPSK", 0, 20)); // NOI18N
        tblDetail.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Name", "Price", "Qty", "SubTotal"
            }
        ));
        jScrollPane1.setViewportView(tblDetail);

        pnlDatePicker1.setBackground(new java.awt.Color(204, 204, 255));

        pnlDatePicker2.setBackground(new java.awt.Color(204, 204, 255));

        btnProcess.setBackground(new java.awt.Color(204, 255, 204));
        btnProcess.setFont(new java.awt.Font("TH SarabunPSK", 0, 18)); // NOI18N
        btnProcess.setText("ตกลง");
        btnProcess.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnProcessActionPerformed(evt);
            }
        });

        jLabel3.setFont(new java.awt.Font("TH SarabunPSK", 0, 36)); // NOI18N
        jLabel3.setText("รายการใบเสร็จ");

        btnView.setBackground(new java.awt.Color(149, 216, 244));
        btnView.setFont(new java.awt.Font("TH SarabunPSK", 0, 18)); // NOI18N
        btnView.setText("ดูรายละเอียด");
        btnView.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnViewActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(9, 9, 9)
                        .addComponent(jLabel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(pnlDatePicker1, javax.swing.GroupLayout.PREFERRED_SIZE, 215, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(pnlDatePicker2, javax.swing.GroupLayout.PREFERRED_SIZE, 215, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnProcess)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 49, Short.MAX_VALUE)
                        .addComponent(btnView, javax.swing.GroupLayout.PREFERRED_SIZE, 104, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(scoHis))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 587, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(pnlDatePicker2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 36, Short.MAX_VALUE)
                        .addComponent(pnlDatePicker1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(jLabel3, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(btnProcess)
                        .addComponent(btnView)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(scoHis, javax.swing.GroupLayout.DEFAULT_SIZE, 579, Short.MAX_VALUE)
                    .addComponent(jScrollPane1))
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnViewActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnViewActionPerformed
        int index = tblReciept.getSelectedRow();
        if (index >= 0) {
            recieptDetails = list.get(index).getRecieptDetails();
        }
        refreshTable();
        System.out.println(recieptDetails);

    }//GEN-LAST:event_btnViewActionPerformed

    private void btnProcessActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnProcessActionPerformed
        String pattern = "yyyy-MM-dd";
        SimpleDateFormat formater = new SimpleDateFormat(pattern);
        System.out.println("" + formater.format(model1.getValue()) + " " + formater.format(model2.getValue()));
        String begin = formater.format(model1.getValue());
        String end = formater.format(model2.getValue());
        list = recieptService.getReciepts(begin, end);
        model.fireTableDataChanged();
    }//GEN-LAST:event_btnProcessActionPerformed

private void initDatePicker() {
        model1 = new UtilDateModel();
        Properties p1 = new Properties();
        p1.put("text.today", "Today");
        p1.put("text.month", "Month");
        p1.put("text.year", "Year");
        JDatePanelImpl datePanel1 = new JDatePanelImpl(model1, p1);
        JDatePickerImpl datePicker1 = new JDatePickerImpl(datePanel1, new DateLabelFormatter());
        pnlDatePicker1.add(datePicker1);
        model1.setSelected(true);

        model2 = new UtilDateModel();
        Properties p2 = new Properties();
        p2.put("text.today", "Today");
        p2.put("text.month", "Month");
        p2.put("text.year", "Year");
        JDatePanelImpl datePanel2 = new JDatePanelImpl(model2, p2);
        JDatePickerImpl datePicker2 = new JDatePickerImpl(datePanel2, new DateLabelFormatter());
        pnlDatePicker2.add(datePicker2);
        model2.setSelected(true);
    }

    private void refreshTable() {
        list = recieptService.getReciepts();
        ((AbstractTableModel) tblDetail.getModel()).fireTableDataChanged();
        ((AbstractTableModel) tblReciept.getModel()).fireTableDataChanged();
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnProcess;
    private javax.swing.JButton btnView;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JPanel pnlDatePicker1;
    private javax.swing.JPanel pnlDatePicker2;
    private javax.swing.JScrollPane scoHis;
    public javax.swing.JTable tblDetail;
    private javax.swing.JTable tblReciept;
    // End of variables declaration//GEN-END:variables
}
