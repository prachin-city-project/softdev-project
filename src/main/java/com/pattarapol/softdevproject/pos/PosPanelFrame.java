/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JPanel.java to edit this template
 */
package com.pattarapol.softdevproject.pos;

import com.pattarapol.databaseproject.model.Member;
import com.pattarapol.databaseproject.model.Product;
import com.pattarapol.databaseproject.model.Reciept;
import com.pattarapol.databaseproject.model.RecieptDetail;
import com.pattarapol.databaseproject.service.EmployeeService;
import com.pattarapol.databaseproject.service.MemberService;
import com.pattarapol.databaseproject.service.ProductService;
import com.pattarapol.databaseproject.service.RecieptService;
import com.pattarapol.softdevproject.component.BuyProductable;
import com.pattarapol.softdevproject.component.ProductListBakeryPanel;
import com.pattarapol.softdevproject.component.ProductListPanel;
import com.pattarapol.softdevproject.member.AddNewMemberDialog;
import com.sun.net.httpserver.Headers;

import java.awt.Font;
import java.awt.Image;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.List;
import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author ASUS
 */
public class PosPanelFrame extends javax.swing.JPanel implements BuyProductable {

    ArrayList<Product> products;
    ProductService productService = new ProductService();
    RecieptService receiptService = new RecieptService();
    EmployeeService employeeService = new EmployeeService();
    Reciept receipt;

    private final ProductListPanel productListPanel;
    private ProductListBakeryPanel productListBakeryPanel;
    private MemberService memberService;
    private List<Member> list;
    private Member editedMember;

    public void setReceipt(Reciept receip) {
        this.receipt = receipt;
    }

    /**
     * Creates new form PosPanel
     */
    public PosPanelFrame() {
        initComponents();
        memberService = new MemberService();
        list = memberService.getMember();
        btnClear.setVisible(false);
        btnCancelMember.setVisible(false);
        btnUserPoint.setVisible(false);
        lblMember.setText("");
        receipt = new Reciept();

        receipt.setEmployee(EmployeeService.currentEmployee);
        tblReceipDerail.getTableHeader().setFont(new Font("TH SarabunPSK", Font.PLAIN, 20));
        tblReceipDerail.setRowHeight(30);
        tblReceipDerail.setModel(new AbstractTableModel() {
            String heaed[] = {"ชื่อ", "ราคา", "จำนวน", "ราคารวม"};

            @Override
            public String getColumnName(int column) {
                return heaed[column];
            }

            @Override
            public int getRowCount() {
                return receipt.getRecieptDetails().size();
            }

            @Override
            public int getColumnCount() {
                return 4;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                ArrayList<RecieptDetail> receiptDetails = receipt.getRecieptDetails();
                RecieptDetail receiptDetail = receiptDetails.get(rowIndex);
                switch (columnIndex) {
                    case 0:
                        return receiptDetail.getProductName();
                    case 1:
                        return receiptDetail.getProductPrice();
                    case 2:
                        return receiptDetail.getQty();
                    case 3:
                        return receiptDetail.getTotalPrice();

                    default:
                        return "";

                }

            }

            @Override
            public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
                ArrayList<RecieptDetail> receiptDetails = receipt.getRecieptDetails();
                RecieptDetail receiptDetail = receiptDetails.get(rowIndex);
                if (columnIndex == 2) {
                    int qty = Integer.parseInt((String) aValue);
                    if (qty < 1) {
                        return;
                    }
                    receiptDetail.setQty(qty);
                    receipt.calculateTotal();
                    refreshReceipt();
                }

            }

            @Override
            public boolean isCellEditable(int rowIndex, int columnIndex) {
                switch (columnIndex) {
                    case 2:
                        return true;
                    default:
                        return false;
                }
            }

        });
        productListPanel = new ProductListPanel();
        productListBakeryPanel = new ProductListBakeryPanel();
        productListPanel.addOnBuyProduct(this);
        productListBakeryPanel.addOnBuyProduct(this);
        scrProductList.setViewportView(productListPanel);
        srcBakery.setViewportView(productListBakeryPanel);

    }

    private void refreshReceipt() {
        tblReceipDerail.revalidate();
        tblReceipDerail.repaint();
        lblSubTotal.setText("฿ " + receipt.getSubTotal() + "0");
        lblTotal.setText("฿ " + receipt.getTotal() + "0");
        lblDiscount.setText("฿ " + receipt.getDiscount() + "0");
        lblMember.setText(" ");
        btnClear.setVisible(false);
        btnCancelMember.setVisible(false);
        btnUserPoint.setVisible(false);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel3 = new javax.swing.JPanel();
        tab = new javax.swing.JTabbedPane();
        scrProductList = new javax.swing.JScrollPane();
        srcBakery = new javax.swing.JScrollPane();
        jPanel2 = new javax.swing.JPanel();
        lblSubTotal = new javax.swing.JLabel();
        btnCalculate = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        tblReceipDerail = new javax.swing.JTable();
        btnSearch = new javax.swing.JButton();
        btnRegister = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        lblDiscount = new javax.swing.JLabel();
        lblTotal = new javax.swing.JLabel();
        lblMember = new javax.swing.JLabel();
        btnClear = new javax.swing.JButton();
        btnUserPoint = new javax.swing.JButton();
        btnCancelMember = new javax.swing.JButton();

        setBackground(new java.awt.Color(204, 204, 255));

        jPanel3.setBackground(new java.awt.Color(204, 204, 255));

        tab.setBackground(new java.awt.Color(204, 255, 255));
        tab.setFont(new java.awt.Font("TH SarabunPSK", 0, 20)); // NOI18N
        tab.addTab("เครื่องดื่ม", scrProductList);
        tab.addTab("ขนม", srcBakery);

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addComponent(tab, javax.swing.GroupLayout.DEFAULT_SIZE, 609, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(tab, javax.swing.GroupLayout.PREFERRED_SIZE, 871, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(12, Short.MAX_VALUE))
        );

        jPanel2.setBackground(new java.awt.Color(204, 204, 255));

        lblSubTotal.setFont(new java.awt.Font("TH SarabunPSK", 0, 30)); // NOI18N
        lblSubTotal.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblSubTotal.setText("฿ 0.00");

        btnCalculate.setBackground(new java.awt.Color(204, 255, 204));
        btnCalculate.setFont(new java.awt.Font("TH SarabunPSK", 0, 30)); // NOI18N
        btnCalculate.setText("ชำระเงิน");
        btnCalculate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCalculateActionPerformed(evt);
            }
        });

        tblReceipDerail.setFont(new java.awt.Font("TH Sarabun New", 0, 20)); // NOI18N
        tblReceipDerail.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane2.setViewportView(tblReceipDerail);

        btnSearch.setBackground(new java.awt.Color(204, 255, 204));
        btnSearch.setFont(new java.awt.Font("TH SarabunPSK", 0, 24)); // NOI18N
        btnSearch.setText("ค้นหาสมาชิก");
        btnSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSearchActionPerformed(evt);
            }
        });

        btnRegister.setBackground(new java.awt.Color(204, 255, 204));
        btnRegister.setFont(new java.awt.Font("TH SarabunPSK", 0, 24)); // NOI18N
        btnRegister.setText("สมัครสมาชิก");
        btnRegister.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRegisterActionPerformed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("TH SarabunPSK", 0, 30)); // NOI18N
        jLabel1.setText("สมาชิก : ");

        jLabel2.setFont(new java.awt.Font("TH SarabunPSK", 0, 30)); // NOI18N
        jLabel2.setText("ยอดรวม : ");

        jLabel3.setFont(new java.awt.Font("TH SarabunPSK", 0, 30)); // NOI18N
        jLabel3.setText("ส่วนลด : ");

        jLabel4.setFont(new java.awt.Font("TH SarabunPSK", 0, 30)); // NOI18N
        jLabel4.setText("ยอดสุทธิ : ");

        lblDiscount.setFont(new java.awt.Font("TH SarabunPSK", 0, 30)); // NOI18N
        lblDiscount.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblDiscount.setText("฿ 0.00");

        lblTotal.setFont(new java.awt.Font("TH SarabunPSK", 0, 30)); // NOI18N
        lblTotal.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblTotal.setText("฿ 0.00");

        lblMember.setFont(new java.awt.Font("TH SarabunPSK", 0, 30)); // NOI18N
        lblMember.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblMember.setText("ตี๋");

        btnClear.setBackground(new java.awt.Color(255, 204, 204));
        btnClear.setFont(new java.awt.Font("TH SarabunPSK", 0, 30)); // NOI18N
        btnClear.setText("ยกเลิก");
        btnClear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnClearActionPerformed(evt);
            }
        });

        btnUserPoint.setBackground(new java.awt.Color(204, 255, 204));
        btnUserPoint.setFont(new java.awt.Font("TH SarabunPSK", 0, 30)); // NOI18N
        btnUserPoint.setText("ใช้แต้ม");
        btnUserPoint.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUserPointActionPerformed(evt);
            }
        });

        btnCancelMember.setBackground(new java.awt.Color(255, 204, 204));
        btnCancelMember.setFont(new java.awt.Font("TH SarabunPSK", 0, 18)); // NOI18N
        btnCancelMember.setText("ยกเลิกสมาชิก");
        btnCancelMember.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelMemberActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(btnSearch, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnRegister, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(lblSubTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 260, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 712, Short.MAX_VALUE)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnCancelMember)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(lblMember, javax.swing.GroupLayout.PREFERRED_SIZE, 260, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel3)
                            .addComponent(jLabel4))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblDiscount, javax.swing.GroupLayout.DEFAULT_SIZE, 618, Short.MAX_VALUE)
                            .addComponent(lblTotal, javax.swing.GroupLayout.DEFAULT_SIZE, 618, Short.MAX_VALUE)))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                        .addComponent(btnClear, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnUserPoint, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnCalculate, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 543, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 53, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnRegister, javax.swing.GroupLayout.PREFERRED_SIZE, 53, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(lblMember)
                    .addComponent(btnCancelMember, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblSubTotal)
                    .addComponent(jLabel2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(lblDiscount))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(lblTotal))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnCalculate, javax.swing.GroupLayout.PREFERRED_SIZE, 66, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnUserPoint, javax.swing.GroupLayout.PREFERRED_SIZE, 66, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnClear, javax.swing.GroupLayout.PREFERRED_SIZE, 66, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(16, 16, 16))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(8, 8, 8)
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnCalculateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCalculateActionPerformed
        if (receipt.getRecieptDetails().isEmpty()) {
            JOptionPane.showConfirmDialog(this, "กรุณาเลือกสินค้าก่อน", "ไม่สามารถชำระเงินได้ ",
                    JOptionPane.ERROR_MESSAGE, JOptionPane.ERROR_MESSAGE);
        } else {
            openPayment();
        }


    }//GEN-LAST:event_btnCalculateActionPerformed

    private void openPayment() {
        //        JFrame frame = (JFrame) SwingUtilities.getRoot(this);
        PaymentDialog paymentDialog = new PaymentDialog(this, receipt);
        paymentDialog.setLocationRelativeTo(this);
        paymentDialog.setVisible(true);
        paymentDialog.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosed(WindowEvent e) {
                if (receipt.getPayment().equals("เงินสด")) {
                    openPaymentCashtDialog();

//                    } else if (receipt.getPayment().equals("บัตรเครดิต")) {
                } else if (receipt.getPayment().equals("โอนจ่าย")) {
                    System.out.println("" + receipt);
                    openPaymentPromptpayDialog();

                } else if (receipt.getPayment().equals("บัตรเครดิต")) {
                    System.out.println("" + receipt);
                    Reciept currentReciept = receiptService.addNew(receipt);
                    clearReceipt();
                    showBill(currentReciept);
                } else {

                }

            }

        });

    }

    private void showBill(Reciept currentReciept) {
        JFrame frame = (JFrame) SwingUtilities.getRoot(this);
        RecieptDialog recieptDialog = new RecieptDialog(frame, currentReciept);
        recieptDialog.setLocationRelativeTo(this);
        recieptDialog.setVisible(true);
        
    }

    private void btnSearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSearchActionPerformed
//        JFrame frame = (JFrame) SwingUtilities.getRoot(this);
        if (receipt.getRecieptDetails().isEmpty()) {
            JOptionPane.showConfirmDialog(this, "กรุณาเลือกสินค้าก่อน", "ไม่สามารถค้นหาสมาชิกได้ ",
                    JOptionPane.ERROR_MESSAGE, JOptionPane.ERROR_MESSAGE);
        } else {
            SearchMemberDialog searchMemberDialog = new SearchMemberDialog(this, receipt);
            searchMemberDialog.setLocationRelativeTo(null);
            searchMemberDialog.setVisible(true);
            searchMemberDialog.setModal(true);
            searchMemberDialog.addWindowListener(new WindowAdapter() {
                @Override
                public void windowClosed(WindowEvent e) {
                    if (receipt.getMember() != null) {
                        lblMember.setText(receipt.getMember().getName());
                        btnCancelMember.setVisible(true);
                        btnUserPoint.setVisible(true);
                    }

                }
            });
        }


    }//GEN-LAST:event_btnSearchActionPerformed

    private void openPaymentCashtDialog() {
        PaymentCashDialog paymentCashDialog = new PaymentCashDialog(this, receipt);
        paymentCashDialog.setLocationRelativeTo(this);
        paymentCashDialog.setVisible(true);
        paymentCashDialog.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosed(WindowEvent e) {
            }
        });
    }

    private void openPaymentPromptpayDialog() {
        PaymentPromptpayDialog paymentPromptpayDialog = new PaymentPromptpayDialog(this, receipt);
        paymentPromptpayDialog.setLocationRelativeTo(this);
        paymentPromptpayDialog.setVisible(true);
        paymentPromptpayDialog.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosed(WindowEvent e) {
            }
        });
    }
    private void btnRegisterActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRegisterActionPerformed
        registerMember();


    }//GEN-LAST:event_btnRegisterActionPerformed

    private void btnClearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnClearActionPerformed
        receipt.delRecieptDetail();
        btnClear.setVisible(false);
        btnCancelMember.setVisible(false);
        btnUserPoint.setVisible(false);
        refreshReceipt();

    }//GEN-LAST:event_btnClearActionPerformed

    private void btnUserPointActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUserPointActionPerformed
        UsePoint usePoint = new UsePoint(this, receipt);
        usePoint.setLocationRelativeTo(this);
        usePoint.setVisible(true);
        usePoint.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosed(WindowEvent e) {
                lblSubTotal.setText("฿ " + receipt.getSubTotal() + "0");
                lblTotal.setText("฿ " + receipt.getTotal() + "0");
                lblDiscount.setText("฿ " + receipt.getDiscount() + "0");

            }
        });
    }//GEN-LAST:event_btnUserPointActionPerformed

    private void btnCancelMemberActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelMemberActionPerformed
        lblMember.setText("");
        lblDiscount.setText("฿ " + "00.00");
        lblTotal.setText("฿ " + receipt.getSubTotal() + "0");
        receipt.delMemberl();
        btnCancelMember.setVisible(false);
        btnUserPoint.setVisible(false);
    }//GEN-LAST:event_btnCancelMemberActionPerformed

    private void registerMember() {
        editedMember = new Member();
        setObjectToForm();
        JDialog dialog = new JDialog();
        dialog.getContentPane().add(new AddNewMemberDialog(memberService));
        dialog.setSize(450, 250);
        dialog.setLocationRelativeTo(null);
        dialog.setModal(true);
        dialog.setVisible(true);
    }

    public void clearReceipt() {
        receipt = new Reciept();
        receipt.setEmployee(EmployeeService.currentEmployee);
        refreshReceipt();
    }

    private void setObjectToForm() {
        editedMember.setName(editedMember.getName());
        editedMember.setPoint(editedMember.getPoint());
        editedMember.setPhone(editedMember.getPhone());
    }


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCalculate;
    private javax.swing.JButton btnCancelMember;
    private javax.swing.JButton btnClear;
    private javax.swing.JButton btnRegister;
    private javax.swing.JButton btnSearch;
    private javax.swing.JButton btnUserPoint;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JLabel lblDiscount;
    private javax.swing.JLabel lblMember;
    private javax.swing.JLabel lblSubTotal;
    private javax.swing.JLabel lblTotal;
    private javax.swing.JScrollPane scrProductList;
    private javax.swing.JScrollPane srcBakery;
    private javax.swing.JTabbedPane tab;
    private javax.swing.JTable tblReceipDerail;
    // End of variables declaration//GEN-END:variables

    @Override
    public void buy(Product product, String productName, String size, String type, String sweetLevel, float price, int qty) {

        boolean checkProduct = false;
        for (RecieptDetail recieptDetail : receipt.getRecieptDetails()) {
            if (recieptDetail.getProductName().equals(productName)) {
                recieptDetail.setQty(recieptDetail.getQty() + qty);
                receipt.calculateTotal();
                checkProduct = true;
                break;
            }
        }
        if (!checkProduct) {
            receipt.addReceiptDetail(product, productName, size, type, sweetLevel, price, qty);
        }
        refreshReceipt();
        btnClear.setVisible(true);

    }
}
