/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JDialog.java to edit this template
 */
package com.pattarapol.softdevproject.pos;

import com.pattarapol.databaseproject.model.Bill;
import com.pattarapol.databaseproject.model.CheckStock;
import com.pattarapol.databaseproject.model.Reciept;
import com.pattarapol.databaseproject.model.RecieptDetail;
import com.pattarapol.databaseproject.service.EmployeeService;
import com.pattarapol.databaseproject.service.ProductService;
import com.pattarapol.databaseproject.service.RecieptService;
import com.pattarapol.softdevproject.material.BuyMaterial;
import java.awt.Frame;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;

/**
 *
 * @author ASUS
 */
public class PaymentDialog extends javax.swing.JDialog {

    private PosPanelFrame posPanelFram;
    private Reciept reciept;
    private Reciept editedReciept;

    /**
     * Creates new form PaymentDialog
     */
    public PaymentDialog(PosPanelFrame posPanelFram, Reciept reciept) {
        super((Frame) null, true);
        this.posPanelFram = posPanelFram;
        this.reciept = reciept;
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        btnCash = new javax.swing.JButton();
        btnPromptPay = new javax.swing.JButton();
        BtnCredit = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jPanel1.setBackground(new java.awt.Color(204, 204, 255));

        jLabel1.setFont(new java.awt.Font("TH SarabunPSK", 1, 24)); // NOI18N
        jLabel1.setText("วิธีการชำระเงิน");

        btnCash.setBackground(new java.awt.Color(204, 255, 204));
        btnCash.setFont(new java.awt.Font("TH SarabunPSK", 0, 24)); // NOI18N
        btnCash.setText("เงินสด");
        btnCash.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCashActionPerformed(evt);
            }
        });

        btnPromptPay.setBackground(new java.awt.Color(204, 255, 204));
        btnPromptPay.setFont(new java.awt.Font("TH SarabunPSK", 0, 24)); // NOI18N
        btnPromptPay.setText("โอนจ่าย");
        btnPromptPay.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPromptPayActionPerformed(evt);
            }
        });

        BtnCredit.setBackground(new java.awt.Color(204, 255, 204));
        BtnCredit.setFont(new java.awt.Font("TH SarabunPSK", 0, 24)); // NOI18N
        BtnCredit.setText("บัตรเครดิต");
        BtnCredit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnCreditActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel1)
                .addGap(173, 173, 173))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(12, 12, 12)
                .addComponent(btnCash, javax.swing.GroupLayout.PREFERRED_SIZE, 134, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnPromptPay, javax.swing.GroupLayout.PREFERRED_SIZE, 134, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(BtnCredit, javax.swing.GroupLayout.PREFERRED_SIZE, 134, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(17, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnCash, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnPromptPay, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(BtnCredit, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(16, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnCashActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCashActionPerformed
        reciept.setPayment("เงินสด");
        posPanelFram.setReceipt(reciept);
        dispose();


    }//GEN-LAST:event_btnCashActionPerformed

    private void btnPromptPayActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPromptPayActionPerformed
        reciept.setReceive(reciept.getTotal());
        reciept.setPayment("โอนจ่าย");
        posPanelFram.setReceipt(reciept);
        dispose();
    }//GEN-LAST:event_btnPromptPayActionPerformed

    private void BtnCreditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnCreditActionPerformed
        reciept.setReceive(reciept.getTotal());
        reciept.setPayment("บัตรเครดิต");
        posPanelFram.setReceipt(reciept);
        dispose();
    }//GEN-LAST:event_BtnCreditActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton BtnCredit;
    private javax.swing.JButton btnCash;
    private javax.swing.JButton btnPromptPay;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    // End of variables declaration//GEN-END:variables
}
