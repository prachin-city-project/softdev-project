/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JPanel.java to edit this template
 */
package com.pattarapol.softdevproject.material;

import com.pattarapol.databaseproject.model.Bill;
import com.pattarapol.databaseproject.model.BillDetail;
import com.pattarapol.databaseproject.model.Expenses;
import com.pattarapol.databaseproject.model.Material;
import com.pattarapol.databaseproject.report.DateLabelFormatter;
import com.pattarapol.databaseproject.service.BillService;
import com.pattarapol.databaseproject.service.EmployeeService;
import com.pattarapol.databaseproject.service.ExpensesService;
import com.pattarapol.databaseproject.service.MaterialService;
import com.pattarapol.databaseproject.service.ValidateException;
import com.pattarapol.softdevproject.component.BuyMaterialable;
import com.pattarapol.softdevproject.component.MaterialListPanel;
import java.awt.Font;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.table.AbstractTableModel;
import org.jdatepicker.impl.JDatePanelImpl;
import org.jdatepicker.impl.JDatePickerImpl;
import org.jdatepicker.impl.UtilDateModel;

/**
 *
 * @author Windows10
 */
public class BuyMaterial extends javax.swing.JPanel implements BuyMaterialable {

    private final MaterialListPanel materialListPanel;
    ArrayList<Material> materials;
    MaterialService materialService = new MaterialService();
    BillService billService = new BillService();
    EmployeeService employeeService = new EmployeeService();
    ExpensesService expensesService = new ExpensesService();
    Bill bill;
    private UtilDateModel model;

    public void setBill(Bill bill) {
        this.bill = bill;
    }

    public BuyMaterial() {
        
        initComponents();
        btnClear.setVisible(false);
        bill = new Bill();
        initDatePicker();

        bill.setEmployee(EmployeeService.currentEmployee);
        tblBillDetail.getTableHeader().setFont(new Font("TH SarabunPSK", Font.PLAIN, 20));
        tblBillDetail.setRowHeight(30);
        tblBillDetail.setModel(new AbstractTableModel() {
            String heaed[] = {"ชื่อ", "ราคา", "จำนวน", "ราคารวม"};

            @Override
            public String getColumnName(int column) {
                return heaed[column];
            }

            @Override
            public int getRowCount() {
                return bill.getBillDetails().size();
            }

            @Override
            public int getColumnCount() {
                return 4;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                ArrayList<BillDetail> billDetails = bill.getBillDetails();
                BillDetail billDetail = billDetails.get(rowIndex);
                switch (columnIndex) {
                    case 0:
                        return billDetail.getMatName();
                    case 1:
                        return billDetail.getMatPrice();
                    case 2:
                        return billDetail.getMatAmount();
                    case 3:
                        return billDetail.getTotalPrice();

                    default:
                        return "";

                }

            }

            @Override
            public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
                ArrayList<BillDetail> billDetails = bill.getBillDetails();
                BillDetail billDetail = billDetails.get(rowIndex);
                if (columnIndex == 2) {
                    int qty = Integer.parseInt((String) aValue);
                    if (qty < 1) {
                        return;
                    }
                    billDetail.setMatAmount(qty);
                    bill.calculateTotal();
                    refreshBill();
                }

            }

            @Override
            public boolean isCellEditable(int rowIndex, int columnIndex) {
                switch (columnIndex) {
                    case 2:
                        return true;
                    default:
                        return false;
                }
            }

        });
        materialListPanel = new MaterialListPanel();
        materialListPanel.addOnBuyMaterial(this);
        scrMaterialList.setViewportView(materialListPanel);
    }

    private void refreshBill() {
        tblBillDetail.revalidate();
        tblBillDetail.repaint();
        lblSubTotal.setText("฿ " + bill.getTotal());
        lblTotal.setText("฿ " + bill.getTotal());

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel3 = new javax.swing.JPanel();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        scrMaterialList = new javax.swing.JScrollPane();
        jPanel2 = new javax.swing.JPanel();
        lblSubTotal = new javax.swing.JLabel();
        btnCalculate = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        tblBillDetail = new javax.swing.JTable();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        lblTotal = new javax.swing.JLabel();
        btnClear = new javax.swing.JButton();
        cmbSup = new javax.swing.JComboBox<>();
        pnlDate = new javax.swing.JPanel();
        txtDiscount = new javax.swing.JTextField();
        btnCal = new javax.swing.JButton();

        setBackground(new java.awt.Color(204, 204, 255));

        jPanel3.setBackground(new java.awt.Color(204, 204, 255));

        jTabbedPane1.setBackground(new java.awt.Color(204, 255, 255));
        jTabbedPane1.setFont(new java.awt.Font("TH SarabunPSK", 0, 20)); // NOI18N
        jTabbedPane1.addTab("วัตถุดิบ", scrMaterialList);

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addComponent(jTabbedPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 611, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jTabbedPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 871, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(12, Short.MAX_VALUE))
        );

        jPanel2.setBackground(new java.awt.Color(204, 204, 255));

        lblSubTotal.setFont(new java.awt.Font("TH SarabunPSK", 0, 30)); // NOI18N
        lblSubTotal.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblSubTotal.setText("฿ 0.00");

        btnCalculate.setBackground(new java.awt.Color(204, 255, 204));
        btnCalculate.setFont(new java.awt.Font("TH SarabunPSK", 0, 30)); // NOI18N
        btnCalculate.setText("ชำระเงิน");
        btnCalculate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCalculateActionPerformed(evt);
            }
        });

        tblBillDetail.setFont(new java.awt.Font("TH SarabunPSK", 0, 24)); // NOI18N
        tblBillDetail.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tblBillDetail.setDragEnabled(true);
        jScrollPane2.setViewportView(tblBillDetail);

        jLabel2.setFont(new java.awt.Font("TH SarabunPSK", 0, 30)); // NOI18N
        jLabel2.setText("ยอดรวม : ");

        jLabel3.setFont(new java.awt.Font("TH SarabunPSK", 0, 30)); // NOI18N
        jLabel3.setText("ส่วนลด : ");

        jLabel4.setFont(new java.awt.Font("TH SarabunPSK", 0, 30)); // NOI18N
        jLabel4.setText("ยอดสุทธิ : ");

        lblTotal.setFont(new java.awt.Font("TH SarabunPSK", 0, 30)); // NOI18N
        lblTotal.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblTotal.setText("฿ 0.00");

        btnClear.setBackground(new java.awt.Color(255, 204, 204));
        btnClear.setFont(new java.awt.Font("TH SarabunPSK", 0, 30)); // NOI18N
        btnClear.setText("ยกเลิก");
        btnClear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnClearActionPerformed(evt);
            }
        });

        cmbSup.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Makro", "BigC", "Lotus" }));
        cmbSup.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbSupActionPerformed(evt);
            }
        });

        pnlDate.setBackground(new java.awt.Color(204, 204, 255));

        txtDiscount.setFont(new java.awt.Font("TH SarabunPSK", 0, 24)); // NOI18N
        txtDiscount.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtDiscount.setText("0");

        btnCal.setBackground(new java.awt.Color(255, 204, 255));
        btnCal.setFont(new java.awt.Font("TH SarabunPSK", 0, 24)); // NOI18N
        btnCal.setText("คำนวณ");
        btnCal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCalActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 570, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(lblTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 260, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                                .addComponent(btnClear, javax.swing.GroupLayout.PREFERRED_SIZE, 188, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnCalculate, javax.swing.GroupLayout.PREFERRED_SIZE, 188, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                                .addComponent(pnlDate, javax.swing.GroupLayout.PREFERRED_SIZE, 220, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(cmbSup, javax.swing.GroupLayout.PREFERRED_SIZE, 127, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2)
                            .addComponent(jLabel3))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                                .addComponent(btnCal, javax.swing.GroupLayout.PREFERRED_SIZE, 93, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtDiscount, javax.swing.GroupLayout.PREFERRED_SIZE, 123, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(lblSubTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 260, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(45, 45, 45)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(cmbSup)
                    .addComponent(pnlDate, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 543, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblSubTotal)
                    .addComponent(jLabel2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtDiscount)
                    .addComponent(btnCal, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(lblTotal))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnCalculate, javax.swing.GroupLayout.PREFERRED_SIZE, 66, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnClear, javax.swing.GroupLayout.PREFERRED_SIZE, 66, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(16, 16, 16))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(8, 8, 8)
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnCalculateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCalculateActionPerformed
        if (bill.getBillDetails().isEmpty()) {
            JOptionPane.showConfirmDialog(this, "ไม่สามารถชำระเงินได้", "กรุณาเลือกสินค้าก่อน ",
                    JOptionPane.ERROR_MESSAGE, JOptionPane.ERROR_MESSAGE);
        } else {
            getSupId();
            String pattern = "yyyy-MM-dd";
            SimpleDateFormat formater = new SimpleDateFormat(pattern);
            bill.setDate(formater.format(model.getValue()));
            Bill currentBill = billService.addNew(bill);
            updateExistingMaterials(currentBill);
            updateExpenses(currentBill);
            clearBill();
            JDialog dialog = (JDialog) SwingUtilities.getWindowAncestor(this);
            dialog.dispose();

        }

    }//GEN-LAST:event_btnCalculateActionPerformed
    private void updateExpenses(Bill currentBill) {
        ExpensesService expensesService = new ExpensesService();
        Expenses newExpenses = new Expenses();
        newExpenses.setEmployeeId(currentBill.getEmployeeId());
        newExpenses.setTotal(newExpenses.getTotal() + bill.getTotal());
        newExpenses.addExpensesDetail("ค่าวัตถุดิบ", bill.getTotal());
        expensesService.addNew(newExpenses);

    }

    private void updateExistingMaterials(Bill currentBill) {
        for (BillDetail billDetail : currentBill.getBillDetails()) {
            try {
                Material material = materialService.getId(billDetail.getMatID());
                material.setRemain(material.getRemain() + billDetail.getMatAmount());
                materialService.update(material);
            } catch (ValidateException ex) {
                Logger.getLogger(BuyMaterial.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public void getSupId() {
        String selectedValue = (String) cmbSup.getSelectedItem();
        if (selectedValue.equals("Makro")) {
            bill.setSupplierId(1);
        } else if (selectedValue.equals("BigC")) {
            bill.setSupplierId(2);
        } else if (selectedValue.equals("Lotus")) {
            bill.setSupplierId(3);
        }
        System.out.println(bill.getSupplierId());
    }


    private void btnClearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnClearActionPerformed
        bill.delBillDetail();
        txtDiscount.setText("" + 0);
        btnClear.setVisible(false);
        refreshBill();

    }//GEN-LAST:event_btnClearActionPerformed
    public void clearBill() {
        bill = new Bill();
        bill.setEmployee(EmployeeService.currentEmployee);
        refreshBill();
    }

    private void initDatePicker() {
        model = new UtilDateModel();
        Properties p = new Properties();
        p.put("text.today", "Today");
        p.put("text.month", "Month");
        p.put("text.year", "Year");
        JDatePanelImpl datePanel = new JDatePanelImpl(model, p);
        JDatePickerImpl datePicker = new JDatePickerImpl(datePanel, new DateLabelFormatter());
        pnlDate.add(datePicker);
        model.setSelected(true);

    }
    private void cmbSupActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbSupActionPerformed

    }//GEN-LAST:event_cmbSupActionPerformed

    private void btnCalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCalActionPerformed
        if (Float.parseFloat(txtDiscount.getText()) < 0 || Float.parseFloat(txtDiscount.getText()) > bill.getSubTotal()) {
            JOptionPane.showConfirmDialog(this, "กรุณาใส่จำนวนเงินให้ถูกต้อง", "ไม่สามารถชำระเงินได้ ",
                    JOptionPane.ERROR_MESSAGE, JOptionPane.ERROR_MESSAGE);
        } else {
            float discount = Float.parseFloat(txtDiscount.getText());
            bill.setDiscount(discount);
            bill.setTotal(bill.getSubTotal() - bill.getDiscount());
            refreshBill();
        }
    }//GEN-LAST:event_btnCalActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCal;
    private javax.swing.JButton btnCalculate;
    private javax.swing.JButton btnClear;
    private javax.swing.JComboBox<String> cmbSup;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JLabel lblSubTotal;
    private javax.swing.JLabel lblTotal;
    private javax.swing.JPanel pnlDate;
    private javax.swing.JScrollPane scrMaterialList;
    private javax.swing.JTable tblBillDetail;
    private javax.swing.JTextField txtDiscount;
    // End of variables declaration//GEN-END:variables

    @Override
    public void buy(Material material, float matPrice, int qty) {
        btnClear.setVisible(true);
        boolean checkMaterial = false;
        for (BillDetail billDetail : bill.getBillDetails()) {
            if (billDetail.getMatName().equals(material.getName())) {
                billDetail.setMatAmount(billDetail.getMatAmount() + qty);
                bill.calculateTotal();
                checkMaterial = true;
                break;
            }
        }
        if (!checkMaterial) {
//        PriceMaterial priceMaterial = new PriceMaterial(this, material);
//        priceMaterial.setLocationRelativeTo(this);
//        priceMaterial.setVisible(true);
            bill.addBillDetail(material, matPrice, qty);
        }
        refreshBill();
    }
}
