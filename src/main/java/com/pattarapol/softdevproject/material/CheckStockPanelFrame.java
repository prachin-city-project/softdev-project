/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JPanel.java to edit this template
 */
package com.pattarapol.softdevproject.material;

import com.pattarapol.databaseproject.model.Category;
import com.pattarapol.databaseproject.model.CheckMaterialItem;
import com.pattarapol.databaseproject.model.CheckStock;
import com.pattarapol.databaseproject.model.Employee;
import com.pattarapol.databaseproject.model.Material;
import com.pattarapol.databaseproject.report.DateLabelFormatter;
import com.pattarapol.databaseproject.service.CategoryService;
import com.pattarapol.databaseproject.service.CheckStockService;
import com.pattarapol.databaseproject.service.EmployeeService;
import com.pattarapol.databaseproject.service.MaterialService;
import java.awt.Font;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Properties;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.table.AbstractTableModel;
import javax.swing.text.html.CSS;
import org.jdatepicker.impl.JDatePanelImpl;
import org.jdatepicker.impl.JDatePickerImpl;
import org.jdatepicker.impl.UtilDateModel;

/**
 *
 * @author ASUS
 */
public class CheckStockPanelFrame extends javax.swing.JPanel {

    private final MaterialService materialService;
    private List<Material> list;
    private List<CheckMaterialItem> checkMaterialItems;
    private Material editedMaterial;
    private CheckStockService checkStockService = new CheckStockService();
    private List<CheckStock> listCs = checkStockService.getcheckStocks();
    private CheckStock editedCheckStock;
        private AbstractTableModel model;
    private UtilDateModel model1;
    private UtilDateModel model2;

    /**
     * Creates new form MaterialFrame
     */
    public CheckStockPanelFrame() {

        initComponents();
         initDatePicker();
        materialService = new MaterialService();
        list = materialService.getMats();

        tblHis.getTableHeader().setFont(new Font("TH SarabunPSK", Font.PLAIN, 20));
        tblMat.getTableHeader().setFont(new Font("TH SarabunPSK", Font.PLAIN, 20));
        tblHis.setRowHeight(20);
        tblMat.setRowHeight(20);
        LoadTableCs();
        LoadTableMat();

    }

    private void LoadTableMat() {
        tblMat.setModel(new AbstractTableModel() {

            @Override
            public String getColumnName(int column) {
                String[] columnNames = {"ชื่อ", "ขั้นต่ำ", "จำนวนคงเหลือ", "จำนวนปัจจุบัน"};
                return columnNames[column];
            }

            @Override
            public int getRowCount() {
                return checkMaterialItems == null ? 0 : checkMaterialItems.size();
            }

            @Override
            public int getColumnCount() {
                return 4;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                CheckMaterialItem checkMaterialItem = checkMaterialItems.get(rowIndex);
                switch (columnIndex) {
                    case 0:
                        return (checkMaterialItem.getMaterial() == null) ? "" : checkMaterialItem.getMaterial().getName();
                    case 1:
                        return (checkMaterialItem.getMaterial() == null) ? "" : checkMaterialItem.getMaterial().getMin();
                    case 2:
                        return (checkMaterialItem.getMaterial() == null) ? "" : checkMaterialItem.getLastQty();
                    case 3:
                        return (checkMaterialItem.getMaterial() == null) ? "" : checkMaterialItem.getCurrentQty();
                    default:
                        return "Unknown";
                }
            }

        });
    }

    private void LoadTableCs() {
        model = new AbstractTableModel() {

            @Override
            public String getColumnName(int column) {
                String[] columnNames = {"วัน เวลา", "ชื่อ"};
                return columnNames[column];
            }

            @Override
            public int getRowCount() {
                return listCs.size();
            }

            @Override
            public int getColumnCount() {
                return 2;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                CheckStock cs = listCs.get(rowIndex);
                EmployeeService employeeService = new EmployeeService();
                Employee employee = employeeService.getId(cs.getEmployeeId());

                switch (columnIndex) {
                    case 0:
                        return cs.getCreatedDate();
                    case 1:
                        return employee.getName();

                    default:
                        return "Unknown";
                }
            }

        };
          tblHis.setModel(model);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        scoHis = new javax.swing.JScrollPane();
        tblHis = new javax.swing.JTable();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblMat = new javax.swing.JTable();
        btnView = new javax.swing.JButton();
        btnProcess = new javax.swing.JButton();
        pnlDatePicker2 = new javax.swing.JPanel();
        pnlDatePicker1 = new javax.swing.JPanel();
        btnPrint = new javax.swing.JButton();
        btnCheckStock = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();

        setBackground(new java.awt.Color(204, 204, 255));

        tblHis.setFont(new java.awt.Font("TH SarabunPSK", 0, 20)); // NOI18N
        tblHis.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null},
                {null, null},
                {null, null},
                {null, null}
            },
            new String [] {
                "Date Time", "Name"
            }
        ));
        scoHis.setViewportView(tblHis);

        tblMat.setFont(new java.awt.Font("TH SarabunPSK", 0, 20)); // NOI18N
        tblMat.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Name", "Minimum", "Remain", "Unit"
            }
        ));
        jScrollPane1.setViewportView(tblMat);

        btnView.setBackground(new java.awt.Color(149, 216, 244));
        btnView.setFont(new java.awt.Font("TH SarabunPSK", 0, 18)); // NOI18N
        btnView.setText("ดูรายละเอียด");
        btnView.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnViewActionPerformed(evt);
            }
        });

        btnProcess.setBackground(new java.awt.Color(204, 255, 204));
        btnProcess.setFont(new java.awt.Font("TH SarabunPSK", 0, 18)); // NOI18N
        btnProcess.setText("ตกลง");
        btnProcess.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnProcessActionPerformed(evt);
            }
        });

        pnlDatePicker2.setBackground(new java.awt.Color(204, 204, 255));

        pnlDatePicker1.setBackground(new java.awt.Color(204, 204, 255));

        btnPrint.setBackground(new java.awt.Color(149, 216, 244));
        btnPrint.setFont(new java.awt.Font("TH SarabunPSK", 0, 18)); // NOI18N
        btnPrint.setText("พิมพ์รายการวัตถุดิบ");
        btnPrint.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPrintActionPerformed(evt);
            }
        });

        btnCheckStock.setBackground(new java.awt.Color(149, 216, 244));
        btnCheckStock.setFont(new java.awt.Font("TH SarabunPSK", 0, 18)); // NOI18N
        btnCheckStock.setText("ตรวจสอบวัตถุดิบ");
        btnCheckStock.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCheckStockActionPerformed(evt);
            }
        });

        jLabel3.setFont(new java.awt.Font("TH SarabunPSK", 0, 36)); // NOI18N
        jLabel3.setText("เช็ควัตถุดิบ");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(12, 12, 12)
                        .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 116, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(pnlDatePicker1, javax.swing.GroupLayout.PREFERRED_SIZE, 215, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(pnlDatePicker2, javax.swing.GroupLayout.PREFERRED_SIZE, 215, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnProcess)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnView))
                    .addComponent(scoHis, javax.swing.GroupLayout.PREFERRED_SIZE, 772, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btnPrint)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnCheckStock, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 690, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(23, 23, 23)
                        .addComponent(jLabel3))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(btnCheckStock, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(btnPrint, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(btnProcess)
                                .addComponent(btnView))
                            .addComponent(pnlDatePicker2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(pnlDatePicker1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1)
                    .addComponent(scoHis, javax.swing.GroupLayout.DEFAULT_SIZE, 509, Short.MAX_VALUE))
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnViewActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnViewActionPerformed
        int index = tblHis.getSelectedRow();
        if (index >= 0) {
            checkMaterialItems = listCs.get(index).getCheckMaterialItems();
        }
        refreshTable();
        System.out.println(checkMaterialItems);

    }//GEN-LAST:event_btnViewActionPerformed

    private void btnCheckStockActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCheckStockActionPerformed
        editedCheckStock = new CheckStock();
        JDialog dialog = new JDialog();
        dialog.getContentPane().add(new CheckStockDialog(checkStockService));
        dialog.setSize(530, 470);
        dialog.setLocationRelativeTo(null);
        dialog.setModal(true);
        dialog.setVisible(true);
        refreshTable();
    }//GEN-LAST:event_btnCheckStockActionPerformed

    private void btnProcessActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnProcessActionPerformed
        String pattern = "yyyy-MM-dd";
        SimpleDateFormat formater = new SimpleDateFormat(pattern);
        System.out.println("" + formater.format(model1.getValue()) + " " + formater.format(model2.getValue()));
        String begin = formater.format(model1.getValue());
        String end = formater.format(model2.getValue());
        listCs = checkStockService.getcheckStocks(begin, end);
        model.fireTableDataChanged();
    }//GEN-LAST:event_btnProcessActionPerformed

    private void btnPrintActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPrintActionPerformed
           CheckStockSheetDialog css = new CheckStockSheetDialog();
        css.setLocationRelativeTo(this);
        css.setVisible(true);
    }//GEN-LAST:event_btnPrintActionPerformed

    private void setObjectToForm() {
        editedMaterial.setName(editedMaterial.getName());
        editedMaterial.setMin(editedMaterial.getMin());
        editedMaterial.setRemain(editedMaterial.getRemain());
        editedMaterial.setUnit(editedMaterial.getUnit());
    }

    private void refreshTable() {
        list = materialService.getMats();
        ((AbstractTableModel) tblMat.getModel()).fireTableDataChanged();
        listCs = checkStockService.getcheckStocks();
        ((AbstractTableModel) tblHis.getModel()).fireTableDataChanged();
       Collections.sort(checkMaterialItems, Comparator.comparingInt(CheckMaterialItem::getCurrentQty));
    }
      private void initDatePicker() {
        model1 = new UtilDateModel();
        Properties p1 = new Properties();
        p1.put("text.today", "Today");
        p1.put("text.month", "Month");
        p1.put("text.year", "Year");
        JDatePanelImpl datePanel1 = new JDatePanelImpl(model1, p1);
        JDatePickerImpl datePicker1 = new JDatePickerImpl(datePanel1, new DateLabelFormatter());
        pnlDatePicker1.add(datePicker1);
        model1.setSelected(true);

        model2 = new UtilDateModel();
        Properties p2 = new Properties();
        p2.put("text.today", "Today");
        p2.put("text.month", "Month");
        p2.put("text.year", "Year");
        JDatePanelImpl datePanel2 = new JDatePanelImpl(model2, p2);
        JDatePickerImpl datePicker2 = new JDatePickerImpl(datePanel2, new DateLabelFormatter());
        pnlDatePicker2.add(datePicker2);
        model2.setSelected(true);
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCheckStock;
    private javax.swing.JButton btnPrint;
    private javax.swing.JButton btnProcess;
    private javax.swing.JButton btnView;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JPanel pnlDatePicker1;
    private javax.swing.JPanel pnlDatePicker2;
    private javax.swing.JScrollPane scoHis;
    private javax.swing.JTable tblHis;
    public javax.swing.JTable tblMat;
    // End of variables declaration//GEN-END:variables
}
