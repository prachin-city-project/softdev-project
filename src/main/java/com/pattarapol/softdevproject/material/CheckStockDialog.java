/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JPanel.java to edit this template
 */
package com.pattarapol.softdevproject.material;

import com.pattarapol.databaseproject.dao.CheckStockDao;
import com.pattarapol.databaseproject.model.CheckMaterialItem;
import com.pattarapol.databaseproject.model.CheckStock;

import com.pattarapol.databaseproject.model.Material;
import com.pattarapol.databaseproject.service.CheckStockService;
import com.pattarapol.databaseproject.service.EmployeeService;

import com.pattarapol.databaseproject.service.MaterialService;
import java.awt.Component;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author ASUS
 */
public class CheckStockDialog extends javax.swing.JPanel {

    private CheckStock editedCheckStock;
    private CheckStockDao checkStockDao;
    private List<Material> list;
    private List<Integer> Num;
    private AbstractTableModel model;
    private final CheckStockService checkStockService;
    private EmployeeService employeeServce;

    /**
     * Creates new form AddNewMaterialDialog
     */
    public CheckStockDialog(CheckStockService checkStockService) {
        this.checkStockService = checkStockService;
        MaterialService material = new MaterialService();
        initComponents();
        list = material.getMats();
        tblCheckStock.getTableHeader().setFont(new Font("TH SarabunPSK", Font.PLAIN, 20));
        tblCheckStock.setRowHeight(20);

        LoadTable();
        Num = new ArrayList<>();

        for (Material mat : list) {
            Num.add(0);
        }
        showDateTime();
        lblName.setText(EmployeeService.currentEmployee.getName());

    }

    public void showDateTime() {
        javax.swing.Timer timer = new javax.swing.Timer(1000, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                // Get the current real-time date and time
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
                String formattedDate = dateFormat.format(new Date());
                String formattedTime = timeFormat.format(new Date());
                lblDate.setText("วันที่: " + formattedDate);
                lblTime.setText("เวลา: " + formattedTime);

            }
        });
        timer.start();
    }

    private void LoadTable() {
        model = new AbstractTableModel() {


            @Override
            public String getColumnName(int column) {
                String[] columnNames = {"ชื่อ", "ขั้นต่ำ", "จำนวนคงเหลือ", "จำนวนปัจจุบัน"};
                return columnNames[column];
            }

            @Override
            public int getRowCount() {
                return list.size();
            }

            @Override
            public int getColumnCount() {
                return 4;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                Material mat = list.get(rowIndex);

                switch (columnIndex) {
                    case 0:
                        return mat.getName();
                    case 1:
                        return mat.getMin();
                    case 2:
                        return mat.getRemain();
                    case 3:
                        return Num.get(rowIndex);

                    default:
                        return "Unknown";
                }
            }

            @Override
            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return columnIndex == 3;
            }

            @Override
            public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
                try {
                    int newValue = Integer.parseInt(aValue.toString());
                    int currentRemain = list.get(rowIndex).getRemain();
                    if (newValue > currentRemain) {
                        JOptionPane.showMessageDialog(tblCheckStock, "จำนวนปัจจุบันต้องไม่มากกว่าจำนวนคงเหลือ");
                    } else {
                        Num.set(rowIndex, newValue);
                        fireTableDataChanged();
                    }
                } catch (NumberFormatException e) {
                    JOptionPane.showMessageDialog(tblCheckStock, "กรุณากรอกเฉพาะตัวเลข");

                }
            }

        };
        tblCheckStock.setModel(model);

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblCheckStock = new javax.swing.JTable();
        lblName = new javax.swing.JLabel();
        lblDate = new javax.swing.JLabel();
        lblTime = new javax.swing.JLabel();
        SaveBtn = new javax.swing.JButton();
        CancelBtn = new javax.swing.JButton();

        setBackground(new java.awt.Color(204, 204, 255));

        jPanel1.setBackground(new java.awt.Color(204, 204, 255));

        jLabel1.setFont(new java.awt.Font("TH SarabunPSK", 0, 20)); // NOI18N
        jLabel1.setText("ชื่อ :");

        jLabel4.setFont(new java.awt.Font("TH SarabunPSK", 1, 36)); // NOI18N
        jLabel4.setText("เช็ควัตถุดิบ");

        tblCheckStock.setFont(new java.awt.Font("TH SarabunPSK", 0, 20)); // NOI18N
        tblCheckStock.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Name", "Min", "Remain", "Check"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Integer.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        jScrollPane1.setViewportView(tblCheckStock);

        lblName.setBackground(new java.awt.Color(204, 204, 255));
        lblName.setFont(new java.awt.Font("TH SarabunPSK", 0, 20)); // NOI18N
        lblName.setOpaque(true);

        lblDate.setFont(new java.awt.Font("TH SarabunPSK", 0, 18)); // NOI18N

        lblTime.setFont(new java.awt.Font("TH SarabunPSK", 0, 18)); // NOI18N

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 510, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(144, 144, 144)
                        .addComponent(lblDate, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(32, 32, 32)
                        .addComponent(lblTime, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lblName, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(10, 10, 10)))
                .addContainerGap())
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(jLabel4)
                .addGap(198, 198, 198))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel4)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lblName, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(lblTime, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(lblDate, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addGap(20, 20, 20)
                            .addComponent(jLabel1))))
                .addGap(10, 10, 10)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 283, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        SaveBtn.setBackground(new java.awt.Color(204, 255, 204));
        SaveBtn.setFont(new java.awt.Font("TH SarabunPSK", 0, 18)); // NOI18N
        SaveBtn.setText("ยืนยัน");
        SaveBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                SaveBtnActionPerformed(evt);
            }
        });

        CancelBtn.setBackground(new java.awt.Color(255, 204, 204));
        CancelBtn.setFont(new java.awt.Font("TH SarabunPSK", 0, 18)); // NOI18N
        CancelBtn.setText("ยกเลิก");
        CancelBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CancelBtnActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(CancelBtn)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(SaveBtn)
                .addGap(182, 182, 182))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 51, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(CancelBtn)
                    .addComponent(SaveBtn))
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void SaveBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_SaveBtnActionPerformed

        JDialog dialog = (JDialog) SwingUtilities.getWindowAncestor(this);
        CheckStock checkStock = new CheckStock();
        EmployeeService employeeService = new EmployeeService();
        checkStock.setEmployeeId(EmployeeService.currentEmployee.getId());
        int i = 0;
        for (Material mat : list) {
            CheckMaterialItem checkMaterialItem = new CheckMaterialItem();
            checkMaterialItem.setMaterialId(mat.getId());
            checkMaterialItem.setLastQty(mat.getRemain());
            checkMaterialItem.setCurrentQty(Num.get(i));
            checkStock.addCheckMaterialItem(checkMaterialItem);
            i++;
        }
//        setFormToObject();
        checkStockService.addNew(checkStock);
        dialog.dispose();
    }//GEN-LAST:event_SaveBtnActionPerformed

    private void CancelBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CancelBtnActionPerformed
        JDialog dialog = (JDialog) SwingUtilities.getWindowAncestor(this);
        dialog.dispose();

    }//GEN-LAST:event_CancelBtnActionPerformed

    private void refreshTable() {
        ((AbstractTableModel) tblCheckStock.getModel()).fireTableDataChanged();
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton CancelBtn;
    private javax.swing.JButton SaveBtn;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lblDate;
    private javax.swing.JLabel lblName;
    private javax.swing.JLabel lblTime;
    private javax.swing.JTable tblCheckStock;
    // End of variables declaration//GEN-END:variables
}
