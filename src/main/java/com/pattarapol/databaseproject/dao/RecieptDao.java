/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.pattarapol.databaseproject.dao;

import com.pattarapol.databaseproject.helper.DatabaseHelper;
import com.pattarapol.databaseproject.model.Reciept;
import com.pattarapol.databaseproject.model.RecieptDetail;

import com.pattarapol.databaseproject.report.DailySalesReport;
import com.pattarapol.databaseproject.report.ProfitReport;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Admin
 */
public class RecieptDao implements Dao<Reciept> {

    @Override
    public Reciept get(int id) {
        Reciept reciept = null;
        String sql = "SELECT * FROM reciept WHERE reciept_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                reciept = Reciept.fromRS(rs);
                RecieptDetailDao rdd = new RecieptDetailDao();
                ArrayList<RecieptDetail> recieptDetails = (ArrayList<RecieptDetail>) rdd.getAll("reciept_id=" + reciept.getId(), " reciept_detail_id ");
                reciept.setRecieptDetails(recieptDetails);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return reciept;
    }

    @Override
    public List<Reciept> getAll() {
        ArrayList<Reciept> list = new ArrayList();
        String sql = "SELECT * FROM reciept";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                Reciept reciept = Reciept.fromRS(rs);
                list.add(reciept);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

  @Override
    public List<Reciept> getAll(String begin, String end) {
        ArrayList<Reciept> list = new ArrayList();
        String sql = " SELECT * FROM reciept   WHERE DATE(reciept_date) BETWEEN ? AND ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, begin);
            stmt.setString(2, end);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                Reciept obj = Reciept.fromRS(rs);
                list.add(obj);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

   public List<Reciept> getAll(String monthYear) {
    ArrayList<Reciept> list = new ArrayList<>();
    String sql = "SELECT * FROM reciept WHERE strftime('%Y-%m', DATE(reciept_date)) = ? ORDER BY reciept_id DESC" ;
    Connection conn = DatabaseHelper.getConnect();

    try {
        PreparedStatement stmt = conn.prepareStatement(sql);
        stmt.setString(1, monthYear);
        ResultSet rs = stmt.executeQuery();

        while (rs.next()) {
            Reciept expenses = Reciept.fromRS(rs);
            list.add(expenses);
        }
    } catch (SQLException ex) {
        System.out.println(ex.getMessage());
    }
    return list;
}

    @Override
    public Reciept save(Reciept obj) {
        String sql = "INSERT INTO reciept (reciept_subtotal, reciept_discount, reciept_total_qty, reciept_payment, reciept_total,reciept_receive, reciept_change, employees_id, member_id)"
                + "VALUES (?,?,?,?,?,?,?,?,?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setFloat(1, obj.getSubTotal());
            stmt.setFloat(2, obj.getDiscount());
            stmt.setInt(3, obj.getTotalQty());
            stmt.setString(4, obj.getPayment());
            stmt.setFloat(5, obj.getTotal());
            stmt.setFloat(6, obj.getReceive());
            stmt.setFloat(7, obj.getChange());
            stmt.setInt(8, obj.getEmployeeId());
            stmt.setInt(9, obj.getMemberId());
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public Reciept update(Reciept obj) {
        String sql = "UPDATE reciept"
                + " SET reciept_subtotal = ?, reciept_discount = ?, reciept_total_qty = ?, reciept_payment = ?, reciept_total = ?, reciept_receive = ?, reciept_change = ?, employees_id = ?, member_id = ?"
                + " WHERE reciept_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setFloat(1, obj.getSubTotal());
            stmt.setFloat(2, obj.getDiscount());
            stmt.setInt(3, obj.getTotalQty());
            stmt.setString(4, obj.getPayment());
            stmt.setFloat(5, obj.getTotal());
            stmt.setFloat(6, obj.getReceive());
            stmt.setFloat(7, obj.getChange());
            stmt.setInt(8, obj.getEmployeeId());
            stmt.setInt(9, obj.getMemberId());
            stmt.setInt(10, obj.getId());
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(Reciept obj) {
        String sql = "DELETE FROM reciept WHERE reciept_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

    public List<DailySalesReport> getDailySales(int limit) {
        ArrayList<DailySalesReport> list = new ArrayList();
        String sql = """
                      SELECT DATE(reciept_date) AS recieptDate, 
                     SUM(reciept_subtotal) AS Sale 
                     FROM reciept 
                     GROUP BY DATE(recieptDate )
                     ORDER BY DATE(recieptDate) ASC 
                     LIMIT ?;
                     """;;
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, limit);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                DailySalesReport obj = DailySalesReport.fromRS(rs);
                list.add(obj);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<DailySalesReport> DailySales(String begin, String end, int limit) {
        ArrayList<DailySalesReport> list = new ArrayList();
        String sql = """
                  SELECT DATE(reciept_date) AS recieptDate, 
                 SUM(reciept_subtotal) AS Sale 
                 FROM reciept
                 WHERE DATE(reciept_date) BETWEEN ? AND ?
                 GROUP BY DATE(recieptDate) 
                 ORDER BY DATE(recieptDate) ASC 
                 LIMIT ?;
                 """;;
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, begin);
            stmt.setString(2, end);
            stmt.setInt(3, limit);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                DailySalesReport obj = DailySalesReport.fromRS(rs);
                list.add(obj);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<ProfitReport> Profit() {
        ArrayList<ProfitReport> list = new ArrayList();
        String sql = """
                WITH ExpenseTotal AS (
                     SELECT strftime('%Y-%m', expenses_date) AS Month, COALESCE(SUM(expenses_total), 0) AS total_expense
                     FROM expenses
                     WHERE expenses_date BETWEEN '2023-10-01' AND '2023-12-01'
                     GROUP BY strftime('%Y-%m', expenses_date)
                 ),
                 IncomeTotal AS (
                     SELECT strftime('%Y-%m', reciept_date) AS Month, COALESCE(SUM(reciept_total), 0) AS total_income
                     FROM reciept
                     WHERE reciept_date BETWEEN '2023-10-01' AND '2023-12-01'
                     GROUP BY strftime('%Y-%m', reciept_date)
                 )
                 
                 SELECT Month, SUM(total_income) AS total_income, SUM(total_expense) AS total_expense, SUM(total_income) - SUM(total_expense) AS profit
                 FROM (
                     SELECT Month, total_income, 0 AS total_expense FROM IncomeTotal
                     UNION ALL
                     SELECT Month, 0 AS total_income, total_expense FROM ExpenseTotal
                 ) AS CombinedData
                 GROUP BY Month
                 ORDER BY Month;
                     """;
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                ProfitReport obj = ProfitReport.fromRS(rs);
                list.add(obj);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

}
