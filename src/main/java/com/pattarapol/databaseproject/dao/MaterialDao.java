/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.pattarapol.databaseproject.dao;

import com.pattarapol.databaseproject.helper.DatabaseHelper;
import com.pattarapol.databaseproject.report.LowstockReport;
import com.pattarapol.databaseproject.model.Material;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author sanak
 */
public class MaterialDao implements Dao<Material> {

    @Override
    public Material get(int id) {
        Material mat = null;
        String sql = "SELECT * FROM material WHERE mat_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                mat = Material.fromRS(rs);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return mat;
    }

    @Override
    public List<Material> getAll() {
        ArrayList<Material> list = new ArrayList();
        String sql = "SELECT * FROM material ORDER BY mat_remain ASC";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                Material mat = Material.fromRS(rs);
                list.add(mat);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<Material> getAll(String order) {
        ArrayList<Material> list = new ArrayList();
        String sql = "SELECT * FROM material  ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Material material = Material.fromRS(rs);
                list.add(material);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public Material save(Material obj) {
        String sql = "INSERT INTO material (mat_name, mat_min, mat_remain, mat_unit)"
                + "VALUES (?,?,?,?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getName());
            stmt.setInt(2, obj.getMin());
            stmt.setInt(3, obj.getRemain());
            stmt.setString(4, obj.getUnit());
//            stmt.setString(5,obj.getStatus());
//            System.out.println(stmt);
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public Material update(Material obj) {
        String sql = "UPDATE material"
                + " SET mat_name = ?, mat_min = ?, mat_remain = ?, mat_unit = ?"
                + " WHERE mat_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getName());
            stmt.setInt(2, obj.getMin());
            stmt.setInt(3, obj.getRemain());
            stmt.setString(4, obj.getUnit());
            stmt.setInt(5, obj.getId());
            int ret = stmt.executeUpdate();

            // อัปเดตคอลัมน์ mat_status
            updateMatStatus(obj.getId(), conn);

            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

// เมทอดสำหรับอัปเดตคอลัมน์ mat_status
    private void updateMatStatus(int matId, Connection conn) throws SQLException {
        String updateStatusSQL = "UPDATE material SET mat_status = ?"
                + " CASE"
                + " WHEN mat_remain == mat_min THEN 'หมด'"
                + " WHEN mat_remain < mat_min AND mat_remain > 0 THEN 'เหลือน้อย'"
                + " ELSE 'ปกติ'"
                + " END"
                + " WHERE mat_id = ?";

        PreparedStatement statusStmt = conn.prepareStatement(updateStatusSQL);
        statusStmt.setInt(1, matId);
        statusStmt.executeUpdate();
    }

    @Override
    public int delete(Material obj) {
        String sql = "DELETE FROM material WHERE mat_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

    @Override
    public List<Material> getAll(String where, String order) {
        ArrayList<Material> list = new ArrayList();
        String sql = "SELECT * FROM material  WHERE  " + where + " ORDER BY " + order;
        System.out.println(sql);
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Material mat = Material.fromRS(rs);

                list.add(mat);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<LowstockReport> getLowstock(int limit) {
        ArrayList<LowstockReport> list = new ArrayList();
        String sql = """
                     SELECT mat_id,mat_name,mat_remain,mat_min,mat_status
                                         FROM material
                                         WHERE mat_remain < mat_min
                     ORDER BY mat_remain ASC 
                                                               LIMIT ?;
                     """;
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, limit);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                LowstockReport obj = LowstockReport.fromRS(rs);
                list.add(obj);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

}
