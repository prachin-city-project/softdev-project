/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.pattarapol.databaseproject.dao;

import com.pattarapol.databaseproject.helper.DatabaseHelper;
import com.pattarapol.databaseproject.model.Role;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Admin
 */
public class RoleDao implements Dao<Role> {
      @Override
    public Role get(int id) {
        Role role = null;
        String sql = "SELECT * FROM role WHERE role_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                role= Role.fromRS(rs);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return role;
    }

    public Role get(String name) {
        Role role = null;
        String sql = "SELECT * FROM role WHERE role_name=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, name);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                role = Role.fromRS(rs);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return role;
    }

    @Override
    public List<Role> getAll() {
        ArrayList<Role> list = new ArrayList();
        String sql = "SELECT * FROM role";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                Role role = Role.fromRS(rs);
                list.add(role);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public List<Role> getAll(String where, String order) {
        ArrayList<Role> list = new ArrayList();
        String sql = "SELECT * FROM role  WHERE  " + where + " ORDER BY " + order;
        System.out.println(sql);
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Role role = Role.fromRS(rs);

                list.add(role);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;

    }

    @Override
    public Role save(Role obj) {
        String sql = "INSERT INTO role (role_name)"
                + "VALUES (?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getName());
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public Role update(Role obj) {
        String sql = "UPDATE role SET role_name = ? WHERE role_id = ? ";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getName());
            stmt.setInt(2, obj.getId());
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public int delete(Role obj) {
        String sql = "DELETE FROM role WHERE role_id = ? ";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }
}
