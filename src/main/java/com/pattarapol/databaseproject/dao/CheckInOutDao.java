/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.pattarapol.databaseproject.dao;

import com.pattarapol.databaseproject.helper.DatabaseHelper;
import com.pattarapol.databaseproject.model.CheckInOut;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 *
 * @author sanak
 */
public class CheckInOutDao implements Dao<CheckInOut> {

    @Override
    public CheckInOut get(int id) {
        CheckInOut checkInOut = null;
        String sql = "SELECT * FROM check_in_out WHERE cio_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                checkInOut = CheckInOut.fromRS(rs);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return checkInOut;
    }
    public List<CheckInOut> getAllByEmpId(int id) {
        ArrayList<CheckInOut> list = new ArrayList();
        String sql = "SELECT * FROM check_in_out WHERE cio_time_out NOT null AND ss_id IS null AND employees_id="+id+" ORDER BY cio_date DESC";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                CheckInOut checkInOut = CheckInOut.fromRS(rs);
                list.add(checkInOut);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public List<CheckInOut> getAll() {
        List<CheckInOut> checkInOuts = new ArrayList<>();
//        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        Date currentDate = new Date();
//        Calendar cal = Calendar.getInstance();
//        cal.setTime(currentDate);
//        cal.add(Calendar.YEAR, -543);
//        String formattedDate = dateFormat.format(cal.getTime());
//        System.out.println(formattedDate.substring(0, 10));
        String sql = "SELECT * FROM check_in_out WHERE DATE(cio_date) ORDER BY cio_id ASC";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                checkInOuts.add(CheckInOut.fromRS(rs));
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return checkInOuts;
    }

    public List<CheckInOut> getAllHistory() {
        ArrayList<CheckInOut> list = new ArrayList();
        String sql = "SELECT * FROM check_in_out WHERE cio_time_out NOT null ORDER BY cio_id ASC ";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                CheckInOut checkInOut = CheckInOut.fromRS(rs);
                list.add(checkInOut);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public CheckInOut save(CheckInOut obj) {
        CheckInOut checkInOut = null;
        String sql = "INSERT INTO check_in_out (employees_id,cio_status)"
                + "VALUES(?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getEmployeeId());
            stmt.setString(2, obj.getStatus());
//            System.out.println(stmt);
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return checkInOut;
    }

    @Override
    public CheckInOut update(CheckInOut obj) {
        String sql = "UPDATE check_in_out"
                + " SET ss_id = ?"
                + " WHERE cio_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getSalaryId());
            stmt.setInt(2, obj.getId());
            int ret = stmt.executeUpdate();
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    public CheckInOut checkOut(CheckInOut obj) {
        String updateCheckOutTimeSql = "UPDATE check_in_out"
                + " SET cio_time_out=?"
                + " WHERE cio_id = ?";
        String updateWorkHoursSql = "UPDATE  check_in_out"
                + " SET cio_work_hour=?"
                + " WHERE cio_id= ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(updateCheckOutTimeSql);
            // Format checkOutDatetime
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(obj.getCheckOutTime());
            int thaiYear = calendar.get(Calendar.YEAR) ;
            calendar.set(Calendar.YEAR, thaiYear);

            Timestamp checkOutTimestamp = new Timestamp(calendar.getTime().getTime());
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String checkOutDatetimeString = dateFormat.format(checkOutTimestamp);
            stmt.setString(1, checkOutDatetimeString);
            stmt.setInt(2, obj.getId());
            stmt.executeUpdate();
            CheckInOut updatedCheckInOut = get(obj.getId());

            long timeDifference = updatedCheckInOut.getCheckOutTime().getTime() - updatedCheckInOut.getCheckInTime().getTime();
            int totalWorkHours = (int) (timeDifference / 3600000);
            PreparedStatement stmt2 = conn.prepareStatement(updateWorkHoursSql);
            stmt2.setInt(1, totalWorkHours);
            stmt2.setInt(2, updatedCheckInOut.getId());
            stmt2.executeUpdate();
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(CheckInOut obj) {
        String sql = "DELETE FROM check_in_out WHERE cio_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

    @Override
    public List<CheckInOut> getAll(String where, String order) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }
}
