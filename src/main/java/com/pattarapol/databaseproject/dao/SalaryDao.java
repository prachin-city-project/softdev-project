/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.pattarapol.databaseproject.dao;

import com.pattarapol.databaseproject.helper.DatabaseHelper;
import com.pattarapol.databaseproject.model.Salary;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author sanak
 */
public class SalaryDao implements Dao<Salary> {

    @Override
    public Salary get(int id) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public List<Salary> getAll() {
        ArrayList<Salary> list = new ArrayList();
        String sql = "SELECT * FROM summary_salary ORDER BY ss_id DESC";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Salary salary = Salary.fromRS(rs);
                list.add(salary);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    

    public List<Salary> getAll(int employeeId) {
        ArrayList<Salary> list = new ArrayList();
        String sql = "SELECT * FROM summary_salary WHERE employees_id=" + employeeId + " ORDER BY ss_date DESC";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                Salary salary = Salary.fromRS(rs);
                list.add(salary);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    public List<Salary> getAll(String begin,String end,int empId) {
        ArrayList<Salary> list = new ArrayList();
        String sql = "SELECT * FROM summary_salary WHERE employees_id="+empId+" AND DATE(ss_date) BETWEEN "+"'"+begin+"'"+" AND "+"'"+end+"'"+"ORDER BY ss_date DESC";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Salary salary = Salary.fromRS(rs);
                list.add(salary);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    public List<Salary> getAll(String begin,String end) {
        ArrayList<Salary> list = new ArrayList();
        String sql = "SELECT * FROM summary_salary WHERE DATE(ss_date) BETWEEN "+"'"+begin+"'"+" AND "+"'"+end+"'"+"ORDER BY ss_date DESC";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Salary salary = Salary.fromRS(rs);
                list.add(salary);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    

@Override
public Salary save(Salary obj) {
    String sql = "INSERT INTO summary_salary (employees_id, employees_name, employee_hour_wage, ss_total_work, ss_total) VALUES (?, ?, ?, ?, ?)";
    Connection conn = DatabaseHelper.getConnect();
    try {
        PreparedStatement stmt = conn.prepareStatement(sql);
        stmt.setInt(1, obj.getEmployeeId());
        stmt.setString(2, obj.getEmployeeName());
        stmt.setInt(3, obj.getEmployeeHourWage());
        stmt.setInt(4, obj.getTotalWork());
        stmt.setInt(5, obj.getTotal());
        stmt.executeUpdate();
        int id = DatabaseHelper.getInsertedId(stmt);
        obj.setId(id);
        return obj;
    } catch (SQLException ex) {
        System.out.println("Error saving Salary: " + ex.getMessage());
        ex.printStackTrace();
        return null;
    }
}

    @Override
    public Salary update(Salary obj) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public int delete(Salary obj) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

   
}
