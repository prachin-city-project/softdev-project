/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.pattarapol.databaseproject.dao;

import com.pattarapol.databaseproject.helper.DatabaseHelper;
import com.pattarapol.databaseproject.model.Category;
import com.pattarapol.databaseproject.model.CheckStock;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author sanak
 */
public class CheckStockDao implements Dao<CheckStock> {

    @Override
    public CheckStock get(int id) {
        CheckStock mat = null;
        String sql = "SELECT * FROM check_stock WHERE check_stock_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                mat = CheckStock.fromRS(rs);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return mat;
    }
    
    

    @Override
    public List<CheckStock> getAll() {
        ArrayList<CheckStock> list = new ArrayList();
        String sql = "SELECT * FROM check_stock";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                CheckStock mat = CheckStock.fromRS(rs);
                list.add(mat);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
      public List<CheckStock> getAll(String begin, String end) {
        ArrayList<CheckStock> list = new ArrayList();
        String sql = " SELECT * FROM check_stock   WHERE created_date BETWEEN ? AND ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, begin);
            stmt.setString(2, end);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                CheckStock obj = CheckStock.fromRS(rs);
                list.add(obj);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
  
 public List<CheckStock> getAll(String monthYear) {
    ArrayList<CheckStock> list = new ArrayList<>();
    String sql = "SELECT * FROM check_stock WHERE strftime('%Y-%m', created_date) = ? ORDER BY check_stock_id DESC" ;
    Connection conn = DatabaseHelper.getConnect();

    try {
        PreparedStatement stmt = conn.prepareStatement(sql);
        stmt.setString(1, monthYear);
        ResultSet rs = stmt.executeQuery();

        while (rs.next()) {
            CheckStock expenses = CheckStock.fromRS(rs);
            list.add(expenses);
        }
    } catch (SQLException ex) {
        System.out.println(ex.getMessage());
    }
    return list;
}

    @Override
    public CheckStock save(CheckStock obj) {
        String sql = "INSERT INTO check_stock (employee_id)"
                + "VALUES (?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getEmployeeId());

//            stmt.setString(5,obj.getStatus());
//            System.out.println(stmt);
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public CheckStock update(CheckStock obj) {
        String sql = "UPDATE check_stock"
                + " SET employee_id = ?"
                + " WHERE check_stock_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getEmployeeId());
            stmt.setInt(2, obj.getId());

            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(CheckStock obj) {
        String sql = "DELETE FROM check_stock WHERE check_stock_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

   

    public CheckStock get(String name) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

}
