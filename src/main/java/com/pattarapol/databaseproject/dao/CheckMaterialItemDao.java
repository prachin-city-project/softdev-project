/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.pattarapol.databaseproject.dao;

import com.pattarapol.databaseproject.helper.DatabaseHelper;
import com.pattarapol.databaseproject.model.Category;
import com.pattarapol.databaseproject.model.CheckMaterialItem;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author sanak
 */
public class CheckMaterialItemDao implements Dao<CheckMaterialItem> {

    @Override
    public CheckMaterialItem get(int id) {
        CheckMaterialItem cmt = null;
        String sql = "SELECT * FROM check_material_item WHERE cmt_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                cmt = CheckMaterialItem.fromRS(rs);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return cmt;
    }

    @Override
    public List<CheckMaterialItem> getAll() {
        ArrayList<CheckMaterialItem> list = new ArrayList();
        String sql = "SELECT * FROM check_material_item ORDER BY cmt_current_qty ASC";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                CheckMaterialItem mat = CheckMaterialItem.fromRS(rs);
                list.add(mat);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
     public List<CheckMaterialItem> getByCheckMatId(int id) {
        ArrayList<CheckMaterialItem> list = new ArrayList();
        String sql = "SELECT * FROM check_material_item WHERE check_stock_id="+id+"";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                CheckMaterialItem mat = CheckMaterialItem.fromRS(rs);
                list.add(mat);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public CheckMaterialItem save(CheckMaterialItem obj) {
        String sql = "INSERT INTO check_material_item (cmt_last_qty,cmt_current_qty,check_stock_id,mat_id)"
                + "VALUES (?,?,?,?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getLastQty());
            stmt.setInt(2, obj.getCurrentQty());
            stmt.setInt(3, obj.getCheckStockId());
            stmt.setInt(4, obj.getMaterialId()) ;
//            stmt.setString(5,obj.getStatus());
                    //            System.out.println(stmt);
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public CheckMaterialItem update(CheckMaterialItem obj) {
        String sql = "UPDATE check_material_item"
                + " SET  ,cmt_last_qty = ?,cmt_current_qty= ?,check_stock_id = ?,mat_id = ?"
                + " WHERE cmt_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getLastQty());
            stmt.setInt(2, obj.getCurrentQty());
            stmt.setInt(3, obj.getCheckStockId());
            stmt.setInt(4, obj.getMaterialId()) ;
            stmt.setInt(5, obj.getId());

            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(CheckMaterialItem obj) {
        String sql = "DELETE FROM check_material_item WHERE cmt_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

    @Override
    public List<CheckMaterialItem> getAll(String where, String order) {
        ArrayList<CheckMaterialItem> list = new ArrayList();
        String sql = "SELECT * FROM check_material_item  WHERE  " + where + " ORDER BY " + order;
        System.out.println(sql);
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                CheckMaterialItem mat = CheckMaterialItem.fromRS(rs);

                list.add(mat);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public CheckMaterialItem get(String name) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

}
