/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.pattarapol.databaseproject.dao;

import com.pattarapol.databaseproject.helper.DatabaseHelper;
import com.pattarapol.databaseproject.model.Bill;
import com.pattarapol.databaseproject.model.BillDetail;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Admin
 */
public class BillDao implements Dao<Bill> {

    @Override
    public Bill get(int id) {
        Bill bill = null;
        String sql = "SELECT * FROM bill WHERE bill_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                bill = Bill.fromRS(rs);
                BillDetailDao bdd = new BillDetailDao();
                ArrayList<BillDetail> billDetails = (ArrayList<BillDetail>) bdd.getAll("bill_id="+bill.getId(), " bill_detail_id ");
                bill.setBillDetails(billDetails);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return bill;
    }

    @Override
    public List<Bill> getAll() {
        ArrayList<Bill> list = new ArrayList();
        String sql = "SELECT * FROM bill";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                Bill bill = Bill.fromRS(rs);
                list.add(bill);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<Bill> getAll(String begin, String end) {
        ArrayList<Bill> list = new ArrayList();
        String sql = " SELECT * FROM bill  WHERE bill_date BETWEEN ? AND ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, begin);
            stmt.setString(2, end);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                Bill obj = Bill.fromRS(rs);
                list.add(obj);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
  
 public List<Bill> getAll(String monthYear) {
    ArrayList<Bill> list = new ArrayList<>();
    String sql = "SELECT * FROM bill WHERE strftime('%Y-%m', bill_date) = ? ORDER BY bill_id DESC" ;
    Connection conn = DatabaseHelper.getConnect();

    try {
        PreparedStatement stmt = conn.prepareStatement(sql);
        stmt.setString(1, monthYear);
        ResultSet rs = stmt.executeQuery();

        while (rs.next()) {
            Bill expenses = Bill.fromRS(rs);
            list.add(expenses);
        }
    } catch (SQLException ex) {
        System.out.println(ex.getMessage());
    }
    return list;
}

    @Override
    public Bill save(Bill obj) {
        String sql = "INSERT INTO bill (bill_subtotal, bill_discount, bill_total_qty, bill_total, bill_receive, bill_change, employee_id, supplier_id,bill_date)"
                + "VALUES (?,?,?,?,?,?,?,?,?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setFloat(1, obj.getSubTotal());
            stmt.setFloat(2, obj.getDiscount());
            stmt.setFloat(3, obj.getTotalQty());
            stmt.setFloat(4, obj.getTotal());
            stmt.setFloat(5, obj.getReceive());
            stmt.setFloat(6, obj.getChange());
            stmt.setInt(7, obj.getEmployeeId());
            stmt.setInt(8, obj.getSupplierId());
            stmt.setString(9, obj.getDate());
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public Bill update(Bill obj) {
        String sql = "UPDATE bill"
                + " SET bill_subtotal = ?, bill_discount = ?, bill_total_qty = ?, bill_total = ?, bill_receive = ?, bill_change = ?, employee_id = ?, supplier_id = ?,bill_date=?"
                + " WHERE bill_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setFloat(1, obj.getSubTotal());
            stmt.setFloat(2, obj.getDiscount());
            stmt.setFloat(3, obj.getTotalQty());
            stmt.setFloat(4, obj.getTotal());
            stmt.setFloat(5, obj.getReceive());
            stmt.setFloat(6, obj.getChange());
            stmt.setInt(7, obj.getEmployeeId());
            stmt.setInt(8, obj.getSupplierId());
            stmt.setString(9, obj.getDate());
            stmt.setInt(10, obj.getId());
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(Bill obj) {
        String sql = "DELETE FROM bill WHERE bill_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

}
