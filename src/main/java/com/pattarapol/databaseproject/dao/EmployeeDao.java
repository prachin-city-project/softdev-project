/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.pattarapol.databaseproject.dao;

import com.pattarapol.databaseproject.helper.DatabaseHelper;
import com.pattarapol.databaseproject.model.Employee;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Jib
 */
public class EmployeeDao implements Dao<Employee> {

    public Employee getByLogin(String name) {
        Employee employee = null;
        String sql = "SELECT * FROM employees WHERE employees_login=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, name);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                employee = Employee.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return employee;
    }

    @Override
    public Employee get(int id) {

        Employee emp = null;
        String sql = "SELECT * FROM employees WHERE employees_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                emp = Employee.fromRS(rs);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return emp;

    }

    public Employee getOneByUserName(String username) {
        Employee empDao = null;
        String sql = "SELECT * FROM employees WHERE employees_login=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, username);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                empDao = Employee.fromRS(rs);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return empDao;
    }

    public Employee get(String name) {
        Employee empDao = null;
        String sql = "SELECT * FROM employees WHERE employees_name=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, name);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                empDao = Employee.fromRS(rs);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return empDao;
    }

    @Override
    public List<Employee> getAll() {
        ArrayList<Employee> list = new ArrayList();
        String sql = "SELECT * FROM employees";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                Employee emp = Employee.fromRS(rs);
                list.add(emp);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public Employee save(Employee obj) {
        String sql = "INSERT INTO employees (employees_name,employees_gender,employee_hour_wage,employees_tel,employees_login,employees_password, role_id)"
                + "VALUES (?,?,?,?,?,?,?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getName());
            stmt.setString(2, obj.getGender());
            stmt.setInt(3, obj.getHour_wage());
            stmt.setString(4, obj.getTel());
            stmt.setString(5, obj.getLogin());
            stmt.setString(6, obj.getPassword());
            stmt.setInt(7, obj.getRoleId());

//            stmt.setString(5,obj.getStatus());
//            System.out.println(stmt);
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

//    @Override
//    public Employee update(Employee obj) {
//        String sql = "UPDATE employees" + " SET employees_name = ?, employees_gender = ?" + " WHERE employees_id = ?, role_id = ?";
//        Connection conn = DatabaseHelper.getConnect();
//        try {
//            PreparedStatement stmt = conn.prepareStatement(sql);
//            stmt.setString(1, obj.getName());
//            stmt.setString(2, obj.getGender());
//            stmt.setInt(3, obj.getHour_wage());
//            stmt.setString(4, obj.getTel());
//            stmt.setString(5, obj.getLogin());
//            stmt.setString(6, obj.getPassword());
//            stmt.setInt(7, obj.getRoleId());
//            stmt.setInt(8, obj.getId());
//            int ret = stmt.executeUpdate();
//            System.out.println(ret);
//            return obj;
//        } catch (SQLException ex) {
//            System.out.println(ex.getMessage());
//            return null;
//        }
//    }
    @Override
    public Employee update(Employee obj) {
        String sql = "UPDATE employees" + " SET employees_name = ?, employees_gender = ?, employees_tel = ?, employees_login = ?, employees_password = ?, role_id = ?" + " WHERE employees_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getName());
            stmt.setString(2, obj.getGender());
            stmt.setString(3, obj.getTel());
            stmt.setString(4, obj.getLogin());
            stmt.setString(5, obj.getPassword());
            stmt.setInt(6, obj.getRoleId());
            stmt.setInt(7, obj.getId());
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(Employee obj) {
        String sql = "DELETE FROM employees WHERE employees_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

    @Override
    public List<Employee> getAll(String where, String order) {
        ArrayList<Employee> list = new ArrayList();
        String sql = "SELECT * FROM employees  WHERE  " + where + " ORDER BY " + order;
        System.out.println(sql);
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Employee emp = Employee.fromRS(rs);

                list.add(emp);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<Employee> getAll(int role) {
        ArrayList<Employee> list = new ArrayList();
        String sql = "SELECT * FROM product WHERE role_id = " + role;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Employee product = Employee.fromRS(rs);
                list.add(product);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

}
