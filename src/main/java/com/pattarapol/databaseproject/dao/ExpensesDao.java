/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.pattarapol.databaseproject.dao;

import com.pattarapol.databaseproject.helper.DatabaseHelper;
import com.pattarapol.databaseproject.model.Expenses;
import com.pattarapol.databaseproject.model.ExpensesDetail;
import com.pattarapol.databaseproject.report.ExpensesReport;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Admin
 */
public class ExpensesDao implements Dao<Expenses> {

    public ExpensesDao() {
    }

    @Override
    public Expenses get(int id) {
        Expenses expenses = null;
        String sql = "SELECT * FROM expenses WHERE expenses_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                expenses = Expenses.fromRS(rs);
                ExpensesDetailDao edd = new ExpensesDetailDao();
                ArrayList<ExpensesDetail> expensesDetails = (ArrayList<ExpensesDetail>) edd.getAll("expenses_id=" + expenses.getId(), " expenses_detail_id");
                expenses.setExpensesDetails(expensesDetails);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return expenses;
    }

    @Override
    public List<Expenses> getAll() {
        ArrayList<Expenses> list = new ArrayList();
        String sql = "SELECT * FROM expenses";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                Expenses expenses = Expenses.fromRS(rs);
                list.add(expenses);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public List<Expenses> getAll(String begin, String end) {
        ArrayList<Expenses> list = new ArrayList();
        String sql = " SELECT * FROM expenses   WHERE DATE(expenses_date) BETWEEN ? AND ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, begin);
            stmt.setString(2, end);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                Expenses obj = Expenses.fromRS(rs);
                list.add(obj);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<Expenses> getAll(String monthYear) {
        ArrayList<Expenses> list = new ArrayList<>();
        String sql = "SELECT * FROM expenses WHERE strftime('%Y-%m', DATE(expenses_date)) = ? ORDER BY expenses_id DESC";
        Connection conn = DatabaseHelper.getConnect();

        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, monthYear);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                Expenses expenses = Expenses.fromRS(rs);
                list.add(expenses);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<ExpensesReport> getExpenses() {
        ArrayList<ExpensesReport> list = new ArrayList();
        String sql = """
            SELECT strftime('%Y-%m', e.expenses_date) AS year_month,
                                         d.expenses_detail_name,
                                         ROUND(SUM(d.expenses_detail_total), 2) AS total_detail_expenses
                                    FROM expenses e
                                         INNER JOIN
                                         expenses_detail d ON e.expenses_id = d.expenses_id
                                   WHERE strftime('%Y-%m', DATE(e.expenses_date)) >= strftime('%Y-%m', 'now', 'localtime') 
                                   GROUP BY year_month,
                                            d.expenses_detail_name
                                   ORDER BY year_month,
                                            d.expenses_detail_name;
                      """;
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                ExpensesReport obj = ExpensesReport.fromRS(rs);
                list.add(obj);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<ExpensesReport> getExpenses(String begin, String end) {
        ArrayList<ExpensesReport> list = new ArrayList();
        String sql = """
                     SELECT strftime('%Y-%m', e.expenses_date) AS year_month,
                            d.expenses_detail_name,
                            ROUND(SUM(d.expenses_detail_total), 2) AS total_detail_expenses
                       FROM expenses e
                            INNER JOIN
                            expenses_detail d ON e.expenses_id = d.expenses_id
                      WHERE  DATE(e.expenses_date) BETWEEN ? AND ?
                      GROUP BY year_month,
                               d.expenses_detail_name
                      ORDER BY year_month,
                               d.expenses_detail_name;
                   
                
                      """;
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, begin);
            stmt.setString(2, end);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                ExpensesReport obj = ExpensesReport.fromRS(rs);
                list.add(obj);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public Expenses save(Expenses obj) {
        String sql = "INSERT INTO expenses (expenses_total, employee_id)"
                + "VALUES (?,?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setFloat(1, obj.getTotal());
            stmt.setInt(2, obj.getEmployeeId());
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public Expenses update(Expenses obj) {
        String sql = "UPDATE expenses"
                + " SET expenses_total = ?, employee_id = ?"
                + " WHERE expenses_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setFloat(1, obj.getTotal());
            stmt.setInt(2, obj.getEmployeeId());
            stmt.setInt(3, obj.getId());
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(Expenses obj) {
        String sql = "DELETE FROM expenses WHERE expenses_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

}
