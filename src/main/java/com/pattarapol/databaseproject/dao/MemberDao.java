/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.pattarapol.databaseproject.dao;

import com.pattarapol.databaseproject.helper.DatabaseHelper;
import com.pattarapol.databaseproject.model.Member;
import com.pattarapol.databaseproject.report.MemberBuyReport;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Admin
 */
public class MemberDao implements Dao<Member> {

    @Override
    public Member get(int id) {
        Member mem = null;
        String sql = "SELECT * FROM member WHERE member_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                mem = Member.fromRS(rs);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return mem;
    }

    @Override
    public List<Member> getAll() {
        ArrayList<Member> list = new ArrayList();
        String sql = "SELECT * FROM member";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                Member mem = Member.fromRS(rs);
                list.add(mem);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<MemberBuyReport> getMemberBuy() {
        ArrayList<MemberBuyReport> list = new ArrayList();
        String sql = """
                       SELECT member.member_id,
                            member.member_name,
                            COUNT(reciept_id) AS total_reciept,
                            SUM(reciept_subtotal) AS total_spent,
                            member_point
                       FROM member
                            LEFT JOIN
                            reciept ON member.member_id = reciept.member_id
                      GROUP BY member.member_id,
                               member.member_name,
                               member_point
                      ORDER BY total_spent DESC;
                   
                
                      """;
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                MemberBuyReport obj = MemberBuyReport.fromRS(rs);
                list.add(obj);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<MemberBuyReport> getMemberBuy(String begin, String end) {
        ArrayList<MemberBuyReport> list = new ArrayList();
        String sql = """
                       SELECT member.member_id,
                            member.member_name,
                            COUNT(reciept_id) AS total_reciept,
                            SUM(reciept_subtotal) AS total_spent,
                            member_point
                       FROM member
                            LEFT JOIN
                            reciept ON member.member_id = reciept.member_id
                      GROUP BY member.member_id,
                               member.member_name,
                               member_point
                      ORDER BY total_spent DESC;
                   
                
                      """;
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, begin);
            stmt.setString(2, end);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                MemberBuyReport obj = MemberBuyReport.fromRS(rs);
                list.add(obj);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public Member save(Member obj) {
        String sql = "INSERT INTO member (member_name, member_point, member_phone)"
                + "VALUES (?,?,?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getName());
            stmt.setInt(2, obj.getPoint());
            stmt.setString(3, obj.getPhone());
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public Member update(Member obj) {
        String sql = "UPDATE member"
                + " SET member_name = ?, member_point = ?, member_phone = ?"
                + " WHERE member_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getName());
            stmt.setInt(2, obj.getPoint());
            stmt.setString(3, obj.getPhone());
            stmt.setInt(4, obj.getId());
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(Member obj) {
        String sql = "DELETE FROM member WHERE member_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

    @Override
    public List<Member> getAll(String where, String order) {
        ArrayList<Member> list = new ArrayList();
        String sql = "SELECT * FROM member  WHERE  " + where + " ORDER BY " + order;
        System.out.println(sql);
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Member mem = Member.fromRS(rs);

                list.add(mem);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;

    }

    public Member getByPhone(String phone) {
        Member member = null;
        String sql = "SELECT * FROM Member WHERE member_phone=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, phone);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                member = Member.fromRS(rs);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return member;
    }

}
