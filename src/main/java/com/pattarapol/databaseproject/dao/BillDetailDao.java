/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.pattarapol.databaseproject.dao;

import com.pattarapol.databaseproject.helper.DatabaseHelper;
import com.pattarapol.databaseproject.model.BillDetail;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Admin
 */
public class BillDetailDao implements Dao<BillDetail> {

    @Override
    public BillDetail get(int id) {
        BillDetail billDetail = null;
        String sql = "SELECT * FROM bill_detail WHERE bill_detail_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                billDetail = BillDetail.fromRS(rs);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return billDetail;
    }

    @Override
    public List<BillDetail> getAll() {
        ArrayList<BillDetail> list = new ArrayList();
        String sql = "SELECT * FROM bill_detail";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                BillDetail billDetail = BillDetail.fromRS(rs);
                list.add(billDetail);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public List<BillDetail> getAll(String where, String order) {
        ArrayList<BillDetail> list = new ArrayList();
        String sql = "SELECT * FROM bill_detail where " + where + " ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                BillDetail billDetail = BillDetail.fromRS(rs);
                list.add(billDetail);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<BillDetail> getAll(String order) {
        ArrayList<BillDetail> list = new ArrayList();
        String sql = "SELECT * FROM bill_detail ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                BillDetail billDetail = BillDetail.fromRS(rs);
                list.add(billDetail);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public BillDetail save(BillDetail obj) {
        String sql = "INSERT INTO bill_detail (mat_id, mat_name, bill_detail_amount, bill_detail_price, bill_detail_total, bill_id)"
                + "VALUES (?,?,?,?,?,?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getMatID());
            stmt.setString(2, obj.getMatName());
            stmt.setInt(3, obj.getMatAmount());
            stmt.setFloat(4, obj.getMatPrice());
            stmt.setFloat(5, obj.getTotalPrice());
            stmt.setInt(6, obj.getBillId());
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public BillDetail update(BillDetail obj) {
        String sql = "UPDATE bill_detail"
                + " SET mat_id = ?, mat_name = ?, bill_detail_amount = ?, bill_detail_price = ?, bill_detail_total = ?, bill_id = ?"
                + " WHERE bill_detail_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getMatID());
            stmt.setString(2, obj.getMatName());
            stmt.setInt(3, obj.getMatAmount());
            stmt.setFloat(4, obj.getMatPrice());
            stmt.setFloat(5, obj.getTotalPrice());
            stmt.setInt(6, obj.getBillId());
            stmt.setInt(7, obj.getId());
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(BillDetail obj) {
        String sql = "DELETE FROM bill_detail WHERE bill_detail_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }
      public List<BillDetail> getByCheckBillId(int id) {
        ArrayList<BillDetail> list = new ArrayList();
        String sql = "SELECT * FROM bill_detail WHERE bill_id="+id+"";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                BillDetail billDetail = BillDetail.fromRS(rs);
                list.add(billDetail);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

}
