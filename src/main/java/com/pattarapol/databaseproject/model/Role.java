/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.pattarapol.databaseproject.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Admin
 */
public class Role {

    private int id;
    private String name;

    public Role(String name) {
        this.id = -1;
        this.name = name;

    }

    public Role() {
        this.id = -1;
        this.name = "";

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Role{" + "id=" + id + ", name=" + name + '}';
    }

    public static Role fromRS(ResultSet rs) {
        Role role = new Role();
        try {
            role.setId(rs.getInt("role_id"));
            role.setName(rs.getString("role_name"));

        } catch (SQLException ex) {
            Logger.getLogger(Member.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return role;
    }

    public boolean isValid() {
        return this.name.length() > 0;
    }
}
