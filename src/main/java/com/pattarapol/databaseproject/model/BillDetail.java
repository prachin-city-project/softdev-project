/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.pattarapol.databaseproject.model;


import com.pattarapol.databaseproject.dao.MaterialDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Windows10
 */
public class BillDetail {

    private int Id;
    private int matID;
    private String matName;
    private int matAmount;
    private float matPrice;
    private float totalPrice;
    private int billId;
    private Material material;

    public BillDetail(int Id, int matID, String matName, int matAmount, float matPrice, float totalPrice, int billId) {
        this.Id = Id;
        this.matID = matID;
        this.matName = matName;
        this.matAmount = matAmount;
        this.matPrice = matPrice;
        this.totalPrice = totalPrice;
        this.billId = billId;

    }
    
    public BillDetail(int matID, String matName, int matAmount, float matPrice, float totalPrice, int billId) {
        this.Id = -1;
        this.matID = matID;
        this.matName = matName;
        this.matAmount = matAmount;
        this.matPrice = matPrice;
        this.totalPrice = totalPrice;
        this.billId = billId;
    }
    
    public BillDetail() {
        this.Id = -1;
        this.matID = 0;
        this.matName = "";
        this.matAmount = 0;
        this.matPrice = 0;
        this.totalPrice = 0;
        this.billId = 0;
    }

    public int getId() {
        return Id;
    }

    public void setId(int Id) {
        this.Id = Id;
    }

    public int getMatID() {
        return matID;
    }

    public void setMatID(int matID) {
        this.matID = matID;
    }

    public String getMatName() {
        return matName;
    }

    public void setMatName(String matName) {
        this.matName = matName;
    }

    public int getMatAmount() {
        return matAmount;
    }

    public void setMatAmount(int matAmount) {
        this.matAmount = matAmount;
        totalPrice = matAmount*matPrice;
    }

    public float getMatPrice() {
        return matPrice;
    }

    public void setMatPrice(float matPrice) {
        this.matPrice = matPrice;
    }

    public float getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(float totalPrice) {
        this.totalPrice = totalPrice;
         
        
    }

    public int getBillId() {
        return billId;
    }

    public void setBillId(int billId) {
        this.billId = billId;
    }

    public Material getMaterial() {
        return material;
    }

    public void setMaterial(Material material) {
        this.material = material;
    }

    @Override
    public String toString() {
        return "BillDetail{" + "Id=" + Id + ", matID=" + matID + ", matName=" + matName + ", matAmount=" + matAmount + ", matPrice=" + matPrice + ", totalPrice=" + totalPrice + ", billId=" + billId + ", material=" + material + '}';
    }

   
    
    public static BillDetail fromRS(ResultSet rs) {
        BillDetail billDetail = new BillDetail();
        try {
            billDetail.setId(rs.getInt("bill_detail_id"));
            billDetail.setMatID(rs.getInt("mat_id"));
            billDetail.setMatName(rs.getString("mat_name"));
            billDetail.setMatAmount(rs.getInt("bill_detail_amount"));
            billDetail.setMatPrice(rs.getFloat("bill_detail_price"));
            billDetail.setTotalPrice(rs.getFloat("bill_detail_total"));
            billDetail.setBillId(rs.getInt("bill_id"));
            //pop
            MaterialDao materialDao = new MaterialDao();
            Material material = materialDao.get(rs.getInt("mat_id"));
            billDetail.setMaterial(material);
 
            
             
        } catch (SQLException ex) {
            Logger.getLogger(BillDetail.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return billDetail;
    }
}
