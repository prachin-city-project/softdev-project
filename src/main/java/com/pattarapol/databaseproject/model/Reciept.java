/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.pattarapol.databaseproject.model;

import com.pattarapol.databaseproject.dao.EmployeeDao;
import com.pattarapol.databaseproject.dao.MemberDao;
import com.pattarapol.databaseproject.dao.RecieptDetailDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Windows10
 */
public class Reciept {

    private int id;
    private Date createdDate;
    private float subTotal;
    private float discount;
    private int totalQty;
    private String payment;
    private float total;
    private float receive;
    private float change;
    private int employeeId;
    private int memberId;
    private String employeeName;
    private String memberName;
    private Employee employee;
    private Member member;
    private ArrayList<RecieptDetail> recieptDetails = new ArrayList<RecieptDetail>();

    public Reciept(int id, Date createdDate, float subTotal, float discount, int totalQty, String payment, float total, float receive, float change, int employeeId, int memberId) {
        this.id = id;
        this.createdDate = createdDate;
        this.subTotal = subTotal;
        this.discount = discount;
        this.totalQty = totalQty;
        this.payment = payment;
        this.total = total;
        this.receive = receive;
        this.change = change;
        this.employeeId = employeeId;
        this.memberId = memberId;
    }

    public Reciept(Date createdDate, float subTotal, float discount, int totalQty, String payment, float total, float receive, float change, int employeeId, int memberId) {
        this.id = -1;
        this.createdDate = createdDate;
        this.subTotal = subTotal;
        this.discount = discount;
        this.totalQty = totalQty;
        this.payment = payment;
        this.total = total;
        this.receive = receive;
        this.change = change;
        this.employeeId = employeeId;
        this.memberId = memberId;
    }

    public Reciept(float subTotal, float discount, int totalQty, String payment, float total, float receive, float change, int employeeId, int memberId) {
        this.id = -1;
        this.createdDate = null;
        this.subTotal = subTotal;
        this.discount = discount;
        this.totalQty = totalQty;
        this.payment = payment;
        this.total = total;
        this.receive = receive;
        this.change = change;
        this.employeeId = employeeId;
        this.memberId = memberId;
    }

    public Reciept(float discount, String payment, float receive, float change, int employeeId, int memberId) {
        this.id = -1;
        this.createdDate = null;
        this.subTotal = 0;
        this.discount = discount;
        this.totalQty = 0;
        this.payment = payment;
        this.total = 0;
        this.receive = receive;
        this.change = change;
        this.employeeId = employeeId;
        this.memberId = memberId;
    }

    public Reciept() {
        this.id = -1;
        this.createdDate = null;
        this.subTotal = 0;
        this.totalQty = 0;
        this.discount = 0;
        this.totalQty = 0;
        this.payment = "";
        this.total = 0;
        this.receive = 0;
        this.change = 0;
        this.employeeId = 0;
        this.memberId = 0;
    }

    public float getSubTotal() {
        return subTotal;
    }

    public void setSubTotal(float subTotal) {
        this.subTotal = subTotal;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public float getTotal() {
        return total;
    }

    public void setTotal(float total) {
        this.total = total;
    }

    public float getDiscount() {
        return discount;
    }

    public void setDiscount(float discount) {
        this.discount = discount;
    }

    public int getTotalQty() {
        return totalQty;
    }

    public void setTotalQty(int totalQty) {
        this.totalQty = totalQty;
    }

    public String getPayment() {
        return payment;
    }

    public void setPayment(String payment) {
        this.payment = payment;
    }

    public float getReceive() {
        return receive;
    }

    public void setReceive(float receive) {
        this.receive = receive;
    }

    public float getChange() {
        return change;
    }

    public void setChange(float change) {
        this.change = change;
    }

    public int getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(int employeeId) {
        this.employeeId = employeeId;
    }

    public int getMemberId() {
        return memberId;
    }

    public void setMemberId(int memberId) {
        this.memberId = memberId;
    }

    public Employee getEmployee() {
        return employee;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
        this.employeeId = employee.getId();
        this.employeeName = employee.getName();
    }

    public Member getMember() {
        return member;
    }

    public String getMemberName() {
        return memberName;
    }

    public void setMember(Member member) {
        this.member = member;
        this.memberId = member.getId();
        this.memberName = member.getName();
    }

    public ArrayList<RecieptDetail> getRecieptDetails() {
        return recieptDetails;
    }

    public void setRecieptDetails(ArrayList recieptDetails) {
        this.recieptDetails = recieptDetails;
    }

    @Override
    public String toString() {
        return "Reciept{" + "id=" + id + ", createdDate=" + createdDate + ", subTotal=" + subTotal + ", discount=" + discount + ", totalQty=" + totalQty + ", payment=" + payment + ", total=" + total + ", receive=" + receive + ", change=" + change + ", employeeId=" + employeeId + ", memberId=" + memberId + ", employee=" + employee + ", member=" + member + ", recieptDetails=" + recieptDetails + '}';
    }

    public void addRecieptDetail(RecieptDetail recieptDetail) {
        recieptDetails.add(recieptDetail);
        calculateTotal();
    }
    public void delRecieptDetail( ) {
        recieptDetails.clear();
        calculateTotal();
    }
     public void delMemberl( ) {
          this.member = null;
          this.memberId = 0;
          this.memberName = "";
          calculateTotal();
    }


    public void addReceiptDetail(Product product,String productName,String size,String type,String sweetLevel,float price, int qty) {
        RecieptDetail rd = new RecieptDetail(productName, size, type, sweetLevel, price, qty, qty *price, -1, product.getId());
        recieptDetails.add(rd);
        calculateTotal();
    }
 

    public void delRecieptDetail(RecieptDetail recieptDetail) {
        recieptDetails.remove(recieptDetail);
        calculateTotal();
    }

    public void calculateTotal() {
        float subTotal = 0.0f;
        int totalQty = 0;
        float discount = 0.0f;
        float total = 0.0f;
        for (RecieptDetail rd : recieptDetails) {
            subTotal += rd.getTotalPrice();
            totalQty += rd.getQty();
            total += rd.getTotalPrice() - discount;
            
        }
        this.discount = discount;
        this.subTotal = subTotal;
        this.totalQty = totalQty;
        this.total = total;

    }

    public static Reciept fromRS(ResultSet rs) {
        Reciept reciept = new Reciept();
        try {
            reciept.setId(rs.getInt("reciept_id"));
            reciept.setCreatedDate(rs.getTimestamp("reciept_date"));
            reciept.setSubTotal(rs.getFloat("reciept_subtotal"));
            reciept.setDiscount(rs.getFloat("reciept_discount"));
            reciept.setTotalQty(rs.getInt("reciept_total_qty"));
            reciept.setPayment(rs.getString("reciept_payment"));
            reciept.setTotal(rs.getFloat("reciept_total"));
            reciept.setReceive(rs.getFloat("reciept_receive"));
            reciept.setChange(rs.getFloat("reciept_change"));
            reciept.setEmployeeId(rs.getInt("employees_id"));
            reciept.setMemberId(rs.getInt("member_id"));
            //Population
            EmployeeDao employeeDao = new EmployeeDao();
            MemberDao memberDao = new MemberDao();
            Employee employee = employeeDao.get(reciept.getEmployeeId());
            Member member = memberDao.get(reciept.getMemberId());
            RecieptDetailDao recieptDetailDao = new RecieptDetailDao();
            reciept.setRecieptDetails((ArrayList<RecieptDetail>) recieptDetailDao.getByCheckRecieptId(reciept.getId()));
            
            
            reciept.setEmployee(employee);
            if (member != null) {
                reciept.setMember(member);
            }
        } catch (SQLException ex) {
            Logger.getLogger(Reciept.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return reciept;
    }
}
