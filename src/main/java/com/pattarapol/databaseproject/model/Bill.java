/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.pattarapol.databaseproject.model;

import com.pattarapol.databaseproject.dao.BillDetailDao;
import com.pattarapol.databaseproject.dao.EmployeeDao;
import com.pattarapol.databaseproject.dao.MaterialDao;
import com.pattarapol.databaseproject.dao.SupplierDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Windows10
 */
public class Bill {

    private int id;
//    private Date createdDate;
    private String date;
    private float subTotal;
    private float discount;
    private int totalQty;
    private float total;
    private float receive;
    private float change;
    private int employeeId;
    private int supplierId;
    private Employee employee;
    private Supplier supplier;
        private ArrayList<BillDetail> billDetails = new ArrayList();

    public Bill(int id, String date, float subTotal, float discount, int totalQty, float total, float receive, float change, int employeeId, int supplierId) {
        this.id = id;
        this.date = date;
        this.subTotal = subTotal;
        this.discount = discount;
        this.totalQty = totalQty;
        this.total = total;
        this.receive = receive;
        this.change = change;
        this.employeeId = employeeId;
        this.supplierId = supplierId;
    }

    public Bill(String date, float subTotal, float discount, int totalQty, float total, float receive, float change, int employeeId, int supplierId) {
        this.id = -1;
        this.date = date;
        this.subTotal = subTotal;
        this.discount = discount;
        this.totalQty = totalQty;
        this.total = total;
        this.receive = receive;
        this.change = change;
        this.employeeId = employeeId;
        this.supplierId = supplierId;
    }

    public Bill(float subTotal, float discount, int totalQty, float total, float receive, float change, int employeeId, int supplierId) {
        this.id = -1;
        this.date = null;
        this.subTotal = subTotal;
        this.discount = discount;
        this.totalQty = totalQty;
        this.total = total;
        this.receive = receive;
        this.change = change;
        this.employeeId = employeeId;
        this.supplierId = supplierId;
    }

    public Bill(float discount, float receive, float change, int employeeId, int supplierId) {
        this.id = -1;
        this.date = null;
        this.subTotal = 0;
        this.discount = discount;
        this.totalQty = 0;
        this.total = 0;
        this.receive = receive;
        this.change = change;
        this.employeeId = employeeId;
        this.supplierId = supplierId;
    }

    public Bill() {
        this.id = -1;
        this.date = null;
        this.subTotal = 0;
        this.discount = 0;
        this.totalQty = 0;
        this.receive = 0;
        this.total = 0;
        this.change = 0;
        this.employeeId = 0;
        this.supplierId = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public float getSubTotal() {
        return subTotal;
    }

    public void setSubTotal(float subTotal) {
        this.subTotal = subTotal;
    }

    public float getDiscount() {
        return discount;
    }

    public void setDiscount(float discount) {
        this.discount = discount;
    }

    public int getTotalQty() {
        return totalQty;
    }

    public void setTotalQty(int totalQty) {
        this.totalQty = totalQty;
    }

    public float getTotal() {
        return total;
    }

    public void setTotal(float total) {
        this.total = total;
    }

    public float getReceive() {
        return receive;
    }

    public void setReceive(float receive) {
        this.receive = receive;
    }

    public float getChange() {
        return change;
    }

    public void setChange(float change) {
        this.change = change;
    }

    public int getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(int employeeId) {
        this.employeeId = employeeId;
    }

    public int getSupplierId() {
        return supplierId;
    }

    public void setSupplierId(int supplierId) {
        this.supplierId = supplierId;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
        this.employeeId = employee.getId();
    }

    public Supplier getSupplier() {
        return supplier;
    }

    public void setSupplier(Supplier supplier) {
        this.supplier = supplier;
        this.supplierId = supplier.getId();
//        this.supplierId = 1;
    }

    public ArrayList<BillDetail> getBillDetails() {
        return billDetails;
    }

    public void setBillDetails(ArrayList billDetails) {
        this.billDetails = billDetails;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "Bill{" + "id=" + id + ", date=" + date + ", subTotal=" + subTotal + ", discount=" + discount + ", totalQty=" + totalQty + ", total=" + total + ", receive=" + receive + ", change=" + change + ", employeeId=" + employeeId + ", supplierId=" + supplierId + ", employee=" + employee + ", supplier=" + supplier + ", billDetails=" + billDetails + '}';
    }

    public void delBillDetail() {
        billDetails.clear();
        discount = 0;
        calculateTotal();
    }

    public void addBillDetail(Material material, float matPrice, int qty) {
        BillDetail bd = new BillDetail(material.getId(), material.getName(), qty, matPrice, qty * matPrice, -1);
        billDetails.add(bd);
        calculateTotal();
    }

    public void addBillDetail(BillDetail billDetail) {
        billDetails.add(billDetail);
        calculateTotal();
    }

    public void delBillDetail(BillDetail billDetail) {
        billDetails.remove(billDetail);
        calculateTotal();
    }

    public void calculateTotal() {
        int totalQty = 0;
        float subtotal = 0.0f;
        float total = 0.0f;
        for (BillDetail bd : billDetails) {
            subtotal += bd.getTotalPrice();
            totalQty += bd.getMatAmount();
        }
        this.totalQty = totalQty;
        this.subTotal = subtotal;
        this.total = subtotal;
    }

    public static Bill fromRS(ResultSet rs) {
        Bill bill = new Bill();
        try {
            bill.setId(rs.getInt("bill_id"));
            bill.setDate(rs.getString("bill_date"));
            bill.setSubTotal(rs.getFloat("bill_subtotal"));
            bill.setDiscount(rs.getFloat("bill_discount"));
            bill.setTotalQty(rs.getInt("bill_total_qty"));
            bill.setReceive(rs.getFloat("bill_receive"));
            bill.setTotal(rs.getFloat("bill_total"));
            bill.setChange(rs.getFloat("bill_change"));
            bill.setEmployeeId(rs.getInt("employee_id"));
            bill.setSupplierId(rs.getInt("supplier_id"));

            EmployeeDao employeeDao = new EmployeeDao();
            Employee employee = employeeDao.get(bill.getEmployeeId());
            bill.setEmployee(employee);
            
            BillDetailDao billDetailDao = new BillDetailDao();
            bill.setBillDetails((ArrayList<BillDetail>) billDetailDao.getByCheckBillId(bill.getId()));
            
            SupplierDao supplierDao = new SupplierDao();
            Supplier supplier = supplierDao.get(bill.getSupplierId());
            bill.setSupplier(supplier);
            
            MaterialDao materialDao = new MaterialDao();
            for (BillDetail b : bill.getBillDetails()) {
                Material mat = materialDao.get(b.getMatID());
                b.setMaterial(mat);
            }
        } catch (SQLException ex) {
            Logger.getLogger(Bill.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return bill;
    }
}
