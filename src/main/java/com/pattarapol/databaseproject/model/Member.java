/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.pattarapol.databaseproject.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Admin
 */
public class Member {

    private int id;
    private String name;
    private String phone;
    private int point;

    public Member(String name, String phone, int point) {
        this.id = -1;
        this.name = name;
        this.phone = phone;
        this.point = point;

    }

    public Member() {
        this.id = -1;
        this.name = "";
        this.phone = "";
        this.point = 0;

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPoint() {
        return point;
    }

    public void setPoint(int point) {
        this.point = point;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Override
    public String toString() {
        return "Member{" + "id=" + id + ", name=" + name + ", point=" + point + ", phone=" + phone + '}';
    }

    public static Member fromRS(ResultSet rs) {
        Member mem = new Member();
        try {
            mem.setId(rs.getInt("member_id"));
            mem.setName(rs.getString("member_name"));
            mem.setPhone(rs.getString("member_phone"));
            mem.setPoint(rs.getInt("member_point"));

        } catch (SQLException ex) {
            Logger.getLogger(Member.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return mem;
    }

    public boolean isValid() {
        ///Business Rule
        ///Name > 0
        ///Phone = 10
        return this.name.length() > 0
                && this.phone.length() == 10
                && this.phone.startsWith("0");
    }
}
