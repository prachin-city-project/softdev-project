/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.pattarapol.databaseproject.model;

import com.pattarapol.databaseproject.dao.EmployeeDao;
import com.pattarapol.databaseproject.dao.ExpensesDao;
import com.pattarapol.databaseproject.dao.ExpensesDetailDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Windows10
 */
public class Expenses {

    private int id;
    private Date createdDate;
    private float total;
    private int employeeId;
    private Employee employee;
    private ArrayList<ExpensesDetail> expensesDetails = new ArrayList();

    public Expenses(int id, Date createdDate, float total, int employeeId) {
        this.id = id;
        this.createdDate = createdDate;
        this.total = total;
        this.employeeId = employeeId;
    }

    public Expenses(Date createdDate, float total, int employeeId) {
        this.id = -1;
        this.createdDate = createdDate;
        this.total = total;
        this.employeeId = employeeId;
    }

    public Expenses(float total, int employeeId) {
        this.id = -1;
        this.createdDate = null;
        this.total = total;
        this.employeeId = employeeId;
    }

    public Expenses(int employeeId) {
        this.id = -1;
        this.createdDate = null;
        this.total = 0;
        this.employeeId = employeeId;
    }

    public Expenses() {
        this.id = -1;
        this.createdDate = null;
        this.total = 0;
        this.employeeId = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public float getTotal() {
        return total;
    }

    public void setTotal(float total) {
        this.total = total;
    }

    public int getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(int employeeId) {
        this.employeeId = employeeId;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
        this.employeeId = employee.getId();
    }

    public ArrayList<ExpensesDetail> getExpensesDetails() {
        return expensesDetails;
    }

    public void setExpensesDetails(ArrayList<ExpensesDetail> expensesDetails) {
        this.expensesDetails = expensesDetails;
    }

    public void addExpensesDetail(String name, Float total) {
        ExpensesDetail e = new ExpensesDetail(name, total);
        expensesDetails.add(e);
        calculateTotal();
    }

    public void delExpensesDetail() {
        expensesDetails.clear();
        calculateTotal();
    }

    public void calculateTotal() {
        Float total = 0.0f;
        for (ExpensesDetail ed : expensesDetails) {
            total += ed.getTotal();
        }
        this.total = total;
    }

//    public void calculateTotal(Float total) {
//        total = 0.0f;
//        for (ExpensesDetail ed : expensesDetails) {
//            total += ed.getTotal();
//        }
//        this.total = total;
//    }

    @Override
    public String toString() {
        return "Expenses{" + "id=" + id + ", createdDate=" + createdDate + ", total=" + total + ", employeeId=" + employeeId + ", employee=" + employee + ", expensesDetails=" + expensesDetails + '}';
    }

    public static Expenses fromRS(ResultSet rs) {
        Expenses expenses = new Expenses();
        try {
            expenses.setId(rs.getInt("expenses_id"));
            expenses.setCreatedDate(rs.getTimestamp("expenses_date"));
            expenses.setTotal(rs.getFloat("expenses_total"));
            expenses.setEmployeeId(rs.getInt("employee_id"));

            EmployeeDao employeeDao = new EmployeeDao();
            Employee employee = employeeDao.get(expenses.getEmployeeId());
            expenses.setEmployee(employee);

            ExpensesDetailDao expensesDetailDao = new ExpensesDetailDao();
            expenses.setExpensesDetails((ArrayList<ExpensesDetail>) expensesDetailDao.getByCheckExpensesId(expenses.getId()));

        } catch (SQLException ex) {
            Logger.getLogger(Expenses.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return expenses;
    }
}


