/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.pattarapol.databaseproject.model;

import com.pattarapol.databaseproject.dao.EmployeeDao;
import com.pattarapol.databaseproject.dao.MemberDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Windows10
 */
public class RecieptDetail {

    private int Id;
    private String productName;
    private String size;
    private String type;
    private String sweet_level;
    private float productPrice;
    private int qty;
    private float totalPrice;
    private int recieptId;
    private int productId;

    public RecieptDetail(int Id, String productName, String size, String type, String sweet_level, float productPrice, int qty, float totalPrice, int recieptId, int productId) {
        this.Id = Id;
        this.productName = productName;
        this.size = size;
        this.type = type;
        this.sweet_level = sweet_level;
        this.productPrice = productPrice;
        this.qty = qty;
        this.totalPrice = totalPrice;
        this.recieptId = recieptId;
        this.productId = productId;
    }
     public RecieptDetail(String productName, String size, String type, String sweet_level, float productPrice, int qty, float totalPrice, int recieptId, int productId) {
        this.Id = -1;
        this.productName = productName;
        this.size = size;
        this.type = type;
        this.sweet_level = sweet_level;
        this.productPrice = productPrice;
        this.qty = qty;
        this.totalPrice = totalPrice;
        this.recieptId = recieptId;
        this.productId = productId;
    }
      public RecieptDetail() {
        this.Id = -1;
        this.productName = "";
        this.size = "";
        this.type = "";
        this.sweet_level = "";
        this.productPrice = 0;
        this.qty = 0;
        this.totalPrice = 0;
        this.recieptId = 0;
        this.productId = 0;
    }

    public int getId() {
        return Id;
    }

    public void setId(int Id) {
        this.Id = Id;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSweet_level() {
        return sweet_level;
    }

    public void setSweet_level(String sweet_level) {
        this.sweet_level = sweet_level;
    }

    public float getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(float productPrice) {
        this.productPrice = productPrice;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
        totalPrice = qty *productPrice;
    }

    public float getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(float totalPrice) {
        this.totalPrice = totalPrice;
    }

    public int getRecieptId() {
        return recieptId;
    }

    public void setRecieptId(int recieptId) {
        this.recieptId = recieptId;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    @Override
    public String toString() {
        return "RecieptDetail{" + "Id=" + Id + ", productName=" + productName + ", size=" + size + ", type=" + type + ", sweet_level=" + sweet_level + ", productPrice=" + productPrice + ", qty=" + qty + ", totalPrice=" + totalPrice + ", recieptId=" + recieptId + ", productId=" + productId + '}';
    }
    
    public static RecieptDetail fromRS(ResultSet rs) {
        RecieptDetail recieptDetail = new RecieptDetail();
        try {
            recieptDetail.setId(rs.getInt("reciept_detail_id"));
            recieptDetail.setProductName(rs.getString("product_name"));
            recieptDetail.setSize(rs.getString("size"));
            recieptDetail.setType(rs.getString("type"));
            recieptDetail.setSweet_level(rs.getString("sweet_level"));
            recieptDetail.setProductPrice(rs.getFloat("product_price"));
            recieptDetail.setQty(rs.getInt("qty"));
            recieptDetail.setTotalPrice(rs.getFloat("total_price"));
            recieptDetail.setRecieptId(rs.getInt("reciept_id"));
            recieptDetail.setProductId(rs.getInt("product_id"));
           
        } catch (SQLException ex) {
            Logger.getLogger(RecieptDetail.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return recieptDetail;
    }
}
