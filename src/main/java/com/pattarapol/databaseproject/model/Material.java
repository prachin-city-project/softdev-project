/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.pattarapol.databaseproject.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author sanak
 */
public class Material {

    private int id;
    private String name;
    private int min;
    private int remain;
    private String unit;
    private String status;

    public Material(String name, int min, int remain, String unit, String status) {
        this.id = -1;
        this.name = name;
        this.min = min;
        this.remain = remain;
        this.unit = unit;
        this.status = status;
    }

    public Material() {
        this.id = -1;
        this.name = "";
        this.min = 0;
        this.remain = 0;
        this.unit = "";
        this.status = "";
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getMin() {
        return min;
    }

    public void setMin(int min) {
        this.min = min;
    }

    public int getRemain() {
        return remain;
    }

    public void setRemain(int remain) {
        this.remain = remain;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Material{" + "id=" + id + ", name=" + name + ", min=" + min + ", remain=" + remain + ", unit=" + unit + ", status=" + status + '}';
    }

    public static Material fromRS(ResultSet rs) {
        Material mat = new Material();
        try {
            mat.setId(rs.getInt("mat_id"));
            mat.setName(rs.getString("mat_name"));
            mat.setMin(rs.getInt("mat_min"));
            mat.setRemain(rs.getInt("mat_remain"));
            mat.setUnit(rs.getString("mat_unit"));
            mat.setStatus(rs.getString("mat_status"));
        } catch (SQLException ex) {
            Logger.getLogger(Material.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return mat;
    }

    public boolean isValid() {
        return this.name.length() > 0
                && this.min > 0
                && this.remain >= 0
                && this.unit.length() > 0;
    }

}
