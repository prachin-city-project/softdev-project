/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.pattarapol.databaseproject.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Asus
 */
public class Category {

    private int id;
    private String name;

    public Category(String name, int price) {
        this.id = -1;
        this.name = name;

    }

    public Category(String name) {
        this.id = -1;
        this.name = name;

    }

    public Category() {
        this.id = -1;
        this.name = "";

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Category{" + "id=" + id + ", name=" + name + '}';
    }

    public static Category fromRS(ResultSet rs) {
        Category cat = new Category();
        try {
            cat.setId(rs.getInt("category_id"));
            cat.setName(rs.getString("category_name"));

        } catch (SQLException ex) {
            Logger.getLogger(Category.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return cat;
    }

    public boolean isValid() {
        return this.name.length() > 0;
    }
}
