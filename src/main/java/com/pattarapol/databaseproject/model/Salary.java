/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.pattarapol.databaseproject.model;

import com.pattarapol.databaseproject.dao.EmployeeDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author sanak
 */
public class Salary {

    private int id;
    private Date ss_date;
    private int employeeId;
    private String employeeName;
    private int employeeHourWage;
    private int totalWork;
    private int total;
    private Employee employee;
    private List<CheckInOut> checkInOuts;


    public Salary(int id, Date ss_date, int employeeId, String employeeName, int employeeHourWage, int totalWork, int total,List<CheckInOut> checkInOuts) {
        this.id = id;
        this.ss_date = ss_date;
        this.employeeId = employeeId;
        this.employeeName = employeeName;
        this.employeeHourWage = employeeHourWage;
        this.totalWork = totalWork;
        this.total = total;
         this.checkInOuts = checkInOuts;
    }

    public Salary() {
        this.id = -1;
        this.ss_date = null;
        this.employeeId = 0;
        this.employeeName = "";
        this.employeeHourWage = 0;
        this.totalWork = 0;
        this.total = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getSs_date() {
        return ss_date;
    }

    public void setSs_date(Date ss_date) {
        this.ss_date = ss_date;
    }

    public int getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(int employeeId) {
        this.employeeId = employeeId;
    }


    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public int getEmployeeHourWage() {
        return employeeHourWage;
    }

    public void setEmployeeHourWage(int employeeHourWage) {
        this.employeeHourWage = employeeHourWage;
    }

    public int getTotalWork() {
        return totalWork;
    }

    public void setTotalWork(int totalWork) {
        this.totalWork = totalWork;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public List<CheckInOut> getCheckInOuts() {
        return checkInOuts;
    }

    public void setCheckInOuts(List<CheckInOut> checkInOuts) {
        this.checkInOuts = checkInOuts;
    }

    @Override
    public String toString() {
        return "Salary{" + "id=" + id + ", ss_date=" + ss_date + ", employeeId=" + employeeId + ", employeeName=" + employeeName + ", employeeHourWage=" + employeeHourWage + ", totalWork=" + totalWork + ", total=" + total + '}';
    }

    public static Salary fromRS(ResultSet rs) {
        Salary salary = new Salary();
        try {
            salary.setId(rs.getInt("ss_id"));
            salary.setSs_date(rs.getTimestamp("ss_date"));
            salary.setEmployeeName(rs.getString("employees_name"));
            EmployeeDao employeeDao = new EmployeeDao();
            Employee emp = employeeDao.get(rs.getInt("employees_id"));
            salary.setEmployee(emp);
            salary.setEmployeeHourWage(rs.getInt("employee_hour_wage"));
            salary.setTotalWork(rs.getInt("ss_total_work"));
            salary.setTotal(rs.getInt("ss_total"));
        } catch (SQLException ex) {
            Logger.getLogger(CheckInOut.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return salary;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
        this.employeeId = employee.getId();
    }

}
