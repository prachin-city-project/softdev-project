/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.pattarapol.databaseproject.model;

import com.pattarapol.databaseproject.dao.EmployeeDao;
import com.pattarapol.databaseproject.dao.ExpensesDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Windows10
 */
public class ExpensesDetail {

    private int id;
    private String name;
    private float total;
    private int expensesDetailId;

    public ExpensesDetail(int id, String name, float total, int expensesDetailId) {
        this.id = id;
        this.name = name;
        this.total = total;
        this.expensesDetailId = expensesDetailId;
    }
    
    public ExpensesDetail(String name, float total, int expensesDetailId) {
        this.id = -1;
        this.name = name;
        this.total = total;
        this.expensesDetailId = expensesDetailId;
    }
     public ExpensesDetail(String name, float total) {
        this.id = -1;
        this.name = name;
        this.total = total;
        this.expensesDetailId = 0;
    }
    
    public ExpensesDetail() {
        this.id = -1;
        this.name = "";
        this.total = 0;
        this.expensesDetailId = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getTotal() {
        return total;
    }

    public void setTotal(float total) {
        this.total = total;
    }

    public int getExpensesId() {
        return expensesDetailId;
    }

    public void setExpensesId(int expensesDetailId) {
        this.expensesDetailId = expensesDetailId;
    }

    public int getExpensesDetailId() {
        return expensesDetailId;
    }

    public void setExpensesDetailId(int expensesDetailId) {
        this.expensesDetailId = expensesDetailId;
    }

    @Override
    public String toString() {
        return "ExpensesDetail{" + "id=" + id + ", name=" + name + ", total=" + total + ", expensesDetailId=" + expensesDetailId + '}';
    }

    public static ExpensesDetail fromRS(ResultSet rs) {
        ExpensesDetail expensesDetail = new ExpensesDetail();
        try {
            expensesDetail.setId(rs.getInt("expenses_detail_id"));
            expensesDetail.setName(rs.getString("expenses_detail_name"));
            expensesDetail.setTotal(rs.getFloat("expenses_detail_total"));
            expensesDetail.setExpensesId(rs.getInt("expenses_id"));
        } catch (SQLException ex) {
            Logger.getLogger(ExpensesDetail.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return expensesDetail;
    }
}
