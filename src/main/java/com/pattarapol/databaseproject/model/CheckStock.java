/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.pattarapol.databaseproject.model;

import com.pattarapol.databaseproject.dao.CheckMaterialItemDao;
import com.pattarapol.databaseproject.dao.EmployeeDao;
import com.pattarapol.databaseproject.dao.MaterialDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author sanak
 */
public class CheckStock {
    private int id;
    private int employeeId;
    private Employee employee;
    private Date createdDate;
    private ArrayList<CheckMaterialItem> checkMaterialItems = new ArrayList();

   
    public void setCheckMaterialItem(ArrayList<CheckMaterialItem> checkMaterialItem) {
        this.checkMaterialItems = checkMaterialItem;
    }

    public CheckStock(int id, int employeeId, Date createdDate) {
        this.id = id;
        this.employeeId = employeeId;
        this.createdDate = createdDate;
    }
     public CheckStock( int employeeId, Date createdDate) {
        this.id = -1;
        this.employeeId = employeeId;
        this.createdDate = createdDate;
    }
      public CheckStock() {
        this.id = -1;
        this.employeeId = 0;
        this.createdDate = null;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(int employeeId) {
        this.employeeId = employeeId;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public ArrayList<CheckMaterialItem> getCheckMaterialItems() {
        return checkMaterialItems;
    }

    public void setCheckMaterialItems(ArrayList<CheckMaterialItem> checkMaterialItems) {
        this.checkMaterialItems = checkMaterialItems;
    }

    @Override
    public String toString() {
        return "CheckStock{" + "id=" + id + ", employeeId=" + employeeId + ", createdDate=" + createdDate + '}';
    }
    public void addCheckMaterialItem(CheckMaterialItem checkMaterialItem){
        checkMaterialItems.add(checkMaterialItem);
    }
    
        public static CheckStock fromRS(ResultSet rs) {
        CheckStock checkStock = new CheckStock();
        try {
            checkStock.setId(rs.getInt("check_stock_id"));
            checkStock.setEmployeeId(rs.getInt("employee_id"));
            checkStock.setCreatedDate(rs.getTimestamp("created_date"));
            
              EmployeeDao employeeDao = new EmployeeDao();
            Employee emp = employeeDao.get(checkStock.getEmployeeId());
            checkStock.setEmployee(emp);
            CheckMaterialItemDao checkMaterialItemDao = new CheckMaterialItemDao();
            checkStock.setCheckMaterialItem((ArrayList<CheckMaterialItem>) checkMaterialItemDao.getByCheckMatId(checkStock.getId()));
            MaterialDao materialDao =  new MaterialDao();
            for(CheckMaterialItem c:checkStock.getCheckMaterialItems()){          
                Material mat = materialDao.get(c.getMaterialId());
                c.setMaterial(mat);
            }
           
        } catch (SQLException ex) {
            Logger.getLogger(CheckStock.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return checkStock;
    }
    
    
}
