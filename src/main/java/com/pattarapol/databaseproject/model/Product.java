/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.pattarapol.databaseproject.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Windows10
 */
public class Product {

    private int id;
    private String name;
    private int price;
    private String type;
    private int category_id;

    public Product(String name, int price, String type, int category_id) {
        this.id = -1;
        this.name = name;
        this.price = price;
        this.type = type;
        this.category_id = category_id;
    }

    public Product() {
        this.id = -1;
        this.name = "";
        this.price = 0;
        this.type = "";
        this.category_id = 0;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getCategoryId() {
        return category_id;
    }

    public void setCategoryId(int category_id) {
        this.category_id = category_id;
    }

    @Override
    public String toString() {
        return "Product{" + "id=" + id + ", name=" + name + ", price=" + price + ", type=" + type + ", category_id=" + category_id + '}';
    }

    public static Product fromRS(ResultSet rs) {
        Product pro = new Product();
        try {
            pro.setId(rs.getInt("product_id"));
            pro.setName(rs.getString("product_name"));
            pro.setPrice(rs.getInt("product_price"));
            pro.setType(rs.getString("product_type"));
            pro.setCategoryId(rs.getInt("category_id"));
        } catch (SQLException ex) {
            Logger.getLogger(Product.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return pro;
    }

    public boolean isValid() {
        return this.name.length() > 0 &&
               this.price > 0;
               
    }
}
