/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.pattarapol.databaseproject.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Jib
 */
public class Supplier {

    private int id;
    private String name;

    public Supplier(int id, String name) {
        this.id = -1;
        this.name = name;
    }

    public Supplier(String name) {
        this.name = name;
    }

    public Supplier() {
        this.id = -1;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Suppiler{" + "id=" + id + ", name=" + name + '}';
    }

    public static Supplier fromRS(ResultSet rs) {
        Supplier sub = new Supplier();

        try {
            sub.setId(rs.getInt("supplier_id"));
            sub.setName(rs.getString("supplier_name"));

        } catch (SQLException ex) {
            Logger.getLogger(Supplier.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return sub;
    }

    public boolean isValid() {
        return this.name.length() > 0;
    }
}
