/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.pattarapol.databaseproject.model;

import com.pattarapol.databaseproject.dao.MaterialDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author sanak
 */
public class CheckMaterialItem {
    private int id;
    private int lastQty;
    private int currentQty;
    private int checkStockId;
    private int materialId;
    private Material material;

    public Material getMaterial() {
        return material;
    }

    public void setMaterial(Material material) {
        this.material = material;
    }
    

   

    public CheckMaterialItem(int id, int lastQty, int currentQty, int checkStockId, int materialId) {
        this.id = id;
        this.lastQty = lastQty;
        this.currentQty = currentQty;
        this.checkStockId = checkStockId;
        this.materialId = materialId;
    }
    public CheckMaterialItem( int lastQty, int currentQty, int checkStockId, int materialId) {
        this.id = -1;
        this.lastQty = lastQty;
        this.currentQty = currentQty;
        this.checkStockId = checkStockId;
        this.materialId = materialId;
    }
     public CheckMaterialItem() {
        this.id = -1;
        this.lastQty = 0;
        this.currentQty = 0;
        this.checkStockId = 0;
        this.materialId = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getLastQty() {
        return lastQty;
    }

    public void setLastQty(int lastQty) {
        this.lastQty = lastQty;
    }

    public int getCurrentQty() {
        return currentQty;
    }

    public void setCurrentQty(int currentQty) {
        this.currentQty = currentQty;
    }

    public int getCheckStockId() {
        return checkStockId;
    }

    public void setCheckStockId(int checkStockId) {
        this.checkStockId = checkStockId;
    }

    public int getMaterialId() {
        return materialId;
    }

    public void setMaterialId(int materialId) {
        this.materialId = materialId;
    }

    @Override
    public String toString() {
        return "CheckMaterialItem{" + "id=" + id + ", lastQty=" + lastQty + ", currentQty=" + currentQty + ", checkStockId=" + checkStockId + ", materialId=" + materialId + ", material=" + material + '}';
    }


   
    
        public static CheckMaterialItem fromRS(ResultSet rs) {
        CheckMaterialItem cmt = new CheckMaterialItem();
        try {
            cmt.setId(rs.getInt("cmt_id"));
            cmt.setLastQty(rs.getInt("cmt_last_qty"));
            MaterialDao matDao = new MaterialDao();
            Material material = matDao.get(cmt.getMaterialId());
            cmt.setMaterial(material);
            cmt.setCurrentQty(rs.getInt("cmt_current_qty"));
            cmt.setCheckStockId(rs.getInt("check_stock_id"));
            cmt.setMaterialId(rs.getInt("mat_id"));
           
        } catch (SQLException ex) {
            Logger.getLogger(CheckMaterialItem.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return cmt;
    }
    
    
}
