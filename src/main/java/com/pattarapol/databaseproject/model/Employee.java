/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.pattarapol.databaseproject.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Jib
 */
public class Employee {

    private int id;
    private String name;
    private int role_id;
    private String gender;
    private int hour_wage;
    private String tel;
    private String login;
    private String password;

    public Employee(String name, int role_id, String gender, int hour_wage, String tel, String login, String password) {
        this.id = -1;
        this.name = name;
        this.gender = gender;
        this.hour_wage = hour_wage;
        this.tel = tel;
        this.login = login;
        this.password = password;
        this.role_id = -1;
    }

    public Employee() {
        this.id = -1;
        this.name = "";
        this.gender = "";
        this.hour_wage = 0;
        this.tel = "";
        this.login = "";
        this.password = "";
        this.role_id = -1;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getRoleId() {
        return role_id;
    }

    public void setRoleId(int role_id) {
        this.role_id = role_id;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public int getHour_wage() {
        return hour_wage;
    }

    public void setHour_wage(int hour_wage) {
        this.hour_wage = hour_wage;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "Employee{" + "id=" + id + ", name=" + name + ", role_id=" + role_id + ", gender=" + gender + ", hour_wage=" + hour_wage + ", tel=" + tel + ", login=" + login + ", password=" + password + '}';
    }

    public static Employee fromRS(ResultSet rs) {
        Employee emp = new Employee();
        try {
            emp.setId(rs.getInt("employees_id"));
            emp.setName(rs.getString("employees_name"));
            emp.setGender(rs.getString("employees_gender"));
            emp.setHour_wage(rs.getInt("employee_hour_wage"));
            emp.setTel(rs.getString("employees_tel"));
            emp.setLogin(rs.getString("employees_login"));
            emp.setPassword(rs.getString("employees_password"));
            emp.setRoleId(rs.getInt("role_id"));

        } catch (SQLException ex) {
            Logger.getLogger(Employee.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return emp;
    }

    public boolean isValid() {
        return this.name.length() > 0
                && this.tel.length() == 10
                && this.tel.startsWith("0")
                && this.login.length() > 0
                && this.password.length() > 0;

    }
}
