/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.pattarapol.databaseproject.model;

import com.pattarapol.databaseproject.dao.EmployeeDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author sanak
 */
public class CheckInOut {

    private int id;
    private Date date;
    private Date checkInTime, checkOutTime;
    private String status;
    private int workHour;
    private int employeeId, salaryId;
     private ArrayList<Salary>salarys  = new ArrayList();
    private Employee employee;

    public CheckInOut(Date date, Date checkInTime, Date checkOutTime, String status, int workHour, int employeeId, int salaryId) {
        this.date = date;
        this.checkInTime = checkInTime;
        this.checkOutTime = checkOutTime;
        this.status = status;
        this.workHour = workHour;
        this.employeeId = employeeId;
        this.salaryId = salaryId;
    }

    public int getEmployeeId() {
        return employeeId;
    }

    public ArrayList<Salary> getSalarys() {
        return salarys;
    }

    public void setSalarys(ArrayList<Salary> salarys) {
        this.salarys = salarys;
    }

    public void setEmployeeId(int employeeId) {
        this.employeeId = employeeId;
    }

    public CheckInOut() {
        this.checkOutTime = null;
        this.status = "";
        this.workHour = 0;
        this.employeeId = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Date getCheckInTime() {
        return checkInTime;
    }

    public void setCheckInTime(Date checkInTime) {
        this.checkInTime = checkInTime;
    }

    public Date getCheckOutTime() {
        return checkOutTime;
    }

    public void setCheckOutTime(Date checkOutTime) {
        this.checkOutTime = checkOutTime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getWorkHour() {
        return workHour;
    }

    public void setWorkHour(int workHour) {
        this.workHour = workHour;
    }

    public int getSalaryId() {
        return salaryId;
    }

    public void setSalaryId(int salaryId) {
        this.salaryId = salaryId;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    @Override
    public String toString() {
        return "CheckInOut{" + "id=" + id + ", date=" + date + ", checkInTime=" + checkInTime + ", checkOutTime=" + checkOutTime + ", status=" + status + ", workHour=" + workHour + ", employeeId=" + employeeId + ", salaryId=" + salaryId + '}';
    }
    public static CheckInOut fromRS(ResultSet rs) {
        CheckInOut checkInOut = new CheckInOut();
        try {
            checkInOut.setId(rs.getInt("cio_id"));
            checkInOut.setDate(rs.getTimestamp("cio_date"));
            checkInOut.setCheckInTime(rs.getTimestamp("cio_time_in"));
            checkInOut.setCheckOutTime(rs.getTimestamp("cio_time_out"));
            checkInOut.setStatus(rs.getString("cio_status"));
            checkInOut.setWorkHour(rs.getInt("cio_work_hour"));
            checkInOut.setEmployeeId(rs.getInt("employees_id"));
            checkInOut.setSalaryId(rs.getInt("ss_id"));
            //population
            EmployeeDao employeeDao = new EmployeeDao();
            Employee employee = employeeDao.get(checkInOut.getEmployeeId());
            checkInOut.setEmployee(employee);
        } catch (SQLException ex) {
            Logger.getLogger(CheckInOut.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return checkInOut;
    }
}
