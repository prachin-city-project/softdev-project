/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.pattarapol.databaseproject.service;

import com.pattarapol.databaseproject.dao.SupplierDao;
import com.pattarapol.databaseproject.model.Supplier;
import java.util.List;

/**
 *
 * @author Jib
 */
public class SupplierService {

    public List<Supplier> getSubs() {
        SupplierDao subDao = new SupplierDao();
        return subDao.getAll();
    }

    public Supplier getName(String name) {
        SupplierDao subDao = new SupplierDao();
        return subDao.get(name);
    }

    public Supplier getId(int id) {
        SupplierDao subDao = new SupplierDao();
        return subDao.get(id);
    }

    public Supplier addNew(Supplier editedSuppiler) throws ValidateException {
        if (!editedSuppiler.isValid()) {
            throw new ValidateException("Supplier is valid!!!");
        }
        SupplierDao subDao = new SupplierDao();
        return subDao.save(editedSuppiler);
    }

    public Supplier update(Supplier editedSuppiler) throws ValidateException {
        if (!editedSuppiler.isValid()) {
            throw new ValidateException("Supplier is valid!!!");
        }
        if (editedSuppiler != null) {
            SupplierDao subDao = new SupplierDao();
            return subDao.update(editedSuppiler);
        } else {
            System.err.println("Cannot update a null supplier.");
            return null;
        }
    }

    public int delete(Supplier editedSuppiler) {
        SupplierDao subDao = new SupplierDao();
        return subDao.delete(editedSuppiler);
    }

}
