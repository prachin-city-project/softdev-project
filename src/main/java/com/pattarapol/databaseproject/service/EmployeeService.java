/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.pattarapol.databaseproject.service;

import com.pattarapol.databaseproject.dao.EmployeeDao;
import com.pattarapol.databaseproject.model.Employee;
import java.util.List;

/**
 *
 * @author Jib
 */
public class EmployeeService {

    public static Employee currentEmployee;

    public Employee checkIn(String name, String password) {
        EmployeeDao employeeDao = new EmployeeDao();
        Employee employee = employeeDao.getOneByUserName(name);
        if (employee != null && employee.getPassword().equals(password)) {
            return employee;
        }
        return null;
    }

    public Employee login(String login, String password) {
        EmployeeDao employeeDao = new EmployeeDao();
        Employee employee = employeeDao.getByLogin(login);
        if (employee != null && employee.getPassword().equals(password)) {
            currentEmployee = employee;
            System.out.println("Success");
            System.out.println(currentEmployee.getName());
            System.out.println(currentEmployee);
            return employee;
        } else {
            System.out.println("No");
        }
        return null;
    }

    public List<Employee> getEmployee() {
        EmployeeDao empDao = new EmployeeDao();
        return empDao.getAll();
    }

    public List<Employee> getEmployeeByRole(int role) {
        EmployeeDao empDao = new EmployeeDao();
        return (List<Employee>) empDao.getAll(role);
    }

    public Employee addNew(Employee editedEmployee) throws ValidateException {
        if (!editedEmployee.isValid()) {
            throw new ValidateException("Employee is valid!!!");
        }
        EmployeeDao empDao = new EmployeeDao();
        return empDao.save(editedEmployee);
    }

//    public Employee getCurrentEmployee() {
//        EmployeeDao empDao = new EmployeeDao();
//        return empDao.get(8);
//    }
    public Employee update(Employee editedEmployee) throws ValidateException {
        if (!editedEmployee.isValid()) {
            throw new ValidateException("Employee is valid!!!");
        }
        if (editedEmployee != null) {
            EmployeeDao empDao = new EmployeeDao();
            return empDao.update(editedEmployee);
        } else {

            System.err.println("Cannot update a null employee.");
            return null;
        }
    }

    public int delete(Employee editedEmployee) {
        EmployeeDao empDao = new EmployeeDao();
        return empDao.delete(editedEmployee);
    }

    public Employee getName(String name) {
        EmployeeDao empDao = new EmployeeDao();
        return empDao.get(name);
    }

    public Employee getId(int id) {
        EmployeeDao empDao = new EmployeeDao();
        return empDao.get(id);
    }

}
