/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.pattarapol.databaseproject.service;

import com.pattarapol.databaseproject.dao.RecieptDao;
import com.pattarapol.databaseproject.dao.RecieptDetailDao;
import com.pattarapol.databaseproject.model.Member;
import com.pattarapol.databaseproject.model.Reciept;
import com.pattarapol.databaseproject.model.RecieptDetail;

import com.pattarapol.databaseproject.report.DailySalesReport;
import com.pattarapol.databaseproject.report.ProfitReport;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Admin
 */
public class RecieptService {
    
     public List<ProfitReport> Profit() {
        RecieptDao recieptDao = new RecieptDao();
        return recieptDao.Profit();
    }
    
    public List<DailySalesReport> getDailySales() {
        RecieptDao recieptDao = new RecieptDao();
        return recieptDao.getDailySales(10);
    }

    public List<DailySalesReport> getDailySales(String begin, String end) {
       RecieptDao recieptDao = new RecieptDao();
        return recieptDao.DailySales(begin, end, 10);
    }

    
    public Reciept getById(int id){
        RecieptDao recieptDao = new RecieptDao();
        return recieptDao.get(id);
    }

     public List<Reciept> getReciepts() {
        ArrayList<Reciept> list = new ArrayList<>();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM");
        Date currentDate = new Date();
        String currentMonth = dateFormat.format(currentDate);

       RecieptDao recieptDao = new RecieptDao();
        return recieptDao.getAll(currentMonth);
    }

    public List<Reciept> getReciepts(String begin, String end) {
        RecieptDao recieptDao = new RecieptDao();
        return recieptDao.getAll(begin, end);
    }
     
     
      public Reciept addNew(Reciept editedReciept) {
        RecieptDao recieptDao = new RecieptDao();
        MemberService memberService = new MemberService();
        RecieptDetailDao recieptDetailDao = new RecieptDetailDao();
        Reciept reciept = recieptDao.save(editedReciept);
        for(RecieptDetail rd: editedReciept.getRecieptDetails()){
            rd.setRecieptId(reciept.getId());
            recieptDetailDao.save(rd);
        }
        if(reciept.getMemberId() > 0){
            Member member = memberService.getMemberId(reciept.getMemberId());
            member.setPoint(reciept.getMember().getPoint()+reciept.getTotalQty());
            try {
                memberService.update(member);
            } catch (ValidateException ex) {
                Logger.getLogger(RecieptService.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return reciept;
    }

    public Reciept update(Reciept editedReciept) {
        if (editedReciept != null) {
            RecieptDao recieptDao = new RecieptDao();
            return recieptDao.update(editedReciept);
        } else {
            System.err.println("Cannot update a null reciept.");
            return null;
        }
    }

    public int delete(Reciept editedReciept) {
        RecieptDao recieptDao = new RecieptDao();
        return recieptDao.delete(editedReciept);
    }
}
