/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.pattarapol.databaseproject.service;

import com.pattarapol.databaseproject.dao.ProductDao;
import com.pattarapol.databaseproject.model.Product;
import com.pattarapol.databaseproject.report.ProductReport;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Windows10
 */
public class ProductService {

    ProductDao productDao = new ProductDao();

    public List<ProductReport> getProductBestSeller() {
        return productDao.getProductBestSeller(10);
    }

    public List<ProductReport> getProductBestSeller(String begin, String end) {
        return productDao.getProductBestSeller(begin, end, 10);
    }

    public List<Product> getPros() {
        return productDao.getAll();
    }

    public ArrayList<Product> getProductsOrderByName() {
        return (ArrayList<Product>) productDao.getAll(" product_name asc");
    }

    public ArrayList<Product> getProductOderByName(int cat) {
        return (ArrayList<Product>) productDao.getAll(cat);
    }

    public Product addNew(Product editedProduct) throws ValidateException {
        if (!editedProduct.isValid()) {
            throw new ValidateException("Product is Valid!!!");
        }
        return productDao.save(editedProduct);
    }

    public Product update(Product editedProduct) throws ValidateException {
        if (!editedProduct.isValid()) {
            throw new ValidateException("Product is Valid!!!");
        }
        if (editedProduct != null) {
            ProductDao proDao = new ProductDao();
            return proDao.update(editedProduct);
        } else {
            System.err.println("Cannot update a null product.");
            return null;
        }
    }

    public int delete(Product editedProduct) {
        ProductDao proDao = new ProductDao();
        return proDao.delete(editedProduct);
    }

}
