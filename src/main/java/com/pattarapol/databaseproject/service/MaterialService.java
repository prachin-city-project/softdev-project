/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.pattarapol.databaseproject.service;

import com.pattarapol.databaseproject.dao.MaterialDao;
import com.pattarapol.databaseproject.report.LowstockReport;
import com.pattarapol.databaseproject.model.Material;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author sanak
 */
public class MaterialService {
     MaterialDao matDao = new MaterialDao();
    public List<LowstockReport> getLowstock() {
        return matDao.getLowstock(10);
    }

    public List<Material> getMats() {
        MaterialDao matDao = new MaterialDao();
        return matDao.getAll();
    }
    public ArrayList<Material> getMaterialsOrderByName() {
        MaterialDao matDao = new MaterialDao();
        return (ArrayList<Material>) matDao.getAll(" mat_name asc");
    }

    public Material addNew(Material editedMaterial) throws ValidateException {
        if (!editedMaterial.isValid()) {
            throw new ValidateException("Material is valid!!!");
        }
        MaterialDao matDao = new MaterialDao();
        return matDao.save(editedMaterial);
    }

    public Material update(Material editedMaterial) throws ValidateException {
        if (!editedMaterial.isValid()) {
            throw new ValidateException("Material is valid!!!");
        }
        if (editedMaterial != null) {
            MaterialDao matDao = new MaterialDao();
            return matDao.update(editedMaterial);
        } else {

            System.err.println("Cannot update a null material.");
            return null;
        }
    }

    public int delete(Material editedMaterial) {
        MaterialDao matDao = new MaterialDao();
        return matDao.delete(editedMaterial);
    }
    
    public Material getId(int id){
        MaterialDao matDao = new MaterialDao();
        return matDao.get(id);
    
    }
}
