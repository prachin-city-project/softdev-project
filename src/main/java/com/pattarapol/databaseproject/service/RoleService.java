/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.pattarapol.databaseproject.service;

import com.pattarapol.databaseproject.dao.RoleDao;
import com.pattarapol.databaseproject.model.Role;
import java.util.List;

/**
 *
 * @author Admin
 */
public class RoleService {

    public List<Role> getRoles() {
        RoleDao roleDao = new RoleDao();
        return roleDao.getAll();
    }

    public Role getName(String name) {
        RoleDao roleDao = new RoleDao();
        return roleDao.get(name);
    }

    public Role getId(int id) {
        RoleDao roleDao = new RoleDao();
        return roleDao.get(id);
    }

    public Role addNew(Role editedRole) throws ValidateException {
        if (!editedRole.isValid()) {
            throw new ValidateException("Role is Valid!!!");
        }
        RoleDao roleDao = new RoleDao();
        return roleDao.save(editedRole);
    }

    public Role update(Role editedRole) throws ValidateException {
        if (!editedRole.isValid()) {
            throw new ValidateException("Role is Valid!!!");
        }
        if (editedRole != null) {
            RoleDao roleDao = new RoleDao();
            return roleDao.update(editedRole);
        } else {
            System.err.println("Cannot update a null role.");
            return null;
        }
    }

    public int delete(Role editedRole) {
        RoleDao roleDao = new RoleDao();
        return roleDao.delete(editedRole);
    }
}
