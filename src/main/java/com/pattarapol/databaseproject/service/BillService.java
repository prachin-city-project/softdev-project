/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.pattarapol.databaseproject.service;

import com.pattarapol.databaseproject.dao.BillDao;
import com.pattarapol.databaseproject.dao.BillDetailDao;
import com.pattarapol.databaseproject.model.Bill;
import com.pattarapol.databaseproject.model.BillDetail;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Admin
 */
public class BillService {

    public Bill getById(int id) {
        BillDao billDao = new BillDao();
        return billDao.get(id);
    }

    public List<Bill> getBills() {
        ArrayList<Bill> list = new ArrayList<>();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM");
        Date currentDate = new Date();
        String currentMonth = dateFormat.format(currentDate);

        BillDao billDao = new BillDao();
        return billDao.getAll(currentMonth);
    }

    public List<Bill> getBills(String begin, String end) {
        BillDao billDao = new BillDao();
        return billDao.getAll(begin, end);
    }

    public Bill addNew(Bill editedBill) {
        BillDao billDao = new BillDao();
        BillDetailDao billDetailDao = new BillDetailDao();
        Bill bill = billDao.save(editedBill);
        for (BillDetail bd : editedBill.getBillDetails()) {
            bd.setBillId(bill.getId());
            billDetailDao.save(bd);
        }
        return bill;
    }

    public Bill update(Bill editedBill) {
        if (editedBill != null) {
            BillDao billDao = new BillDao();
            return billDao.update(editedBill);
        } else {
            System.err.println("Cannot update a null bill.");
            return null;
        }
    }

    public int delete(Bill editedBill) {
        BillDao billDao = new BillDao();
        return billDao.delete(editedBill);
    }
}
