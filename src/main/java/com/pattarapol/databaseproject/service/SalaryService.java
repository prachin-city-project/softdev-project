/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.pattarapol.databaseproject.service;

import com.pattarapol.databaseproject.dao.SalaryDao;
import com.pattarapol.databaseproject.model.CheckInOut;
import com.pattarapol.databaseproject.model.Salary;
import java.util.List;

/**
 *
 * @author sanak
 */
public class SalaryService {

    SalaryDao salaryDao = new SalaryDao();

    public List<Salary> getSalaries() {
        return salaryDao.getAll();
    }

    public List<Salary> getSalaries(int id) {
        return salaryDao.getAll(id);
    }
    public List<Salary> getSalaries(String begin, String end) {
        return salaryDao.getAll(begin, end);
    }
    public List<Salary> getSalaries(String begin, String end, int empId){
        return salaryDao.getAll(begin, end,empId);
    }

    public Salary addNew(Salary salary) {
        if (salary.getCheckInOuts() != null && !salary.getCheckInOuts().isEmpty()) {
            Salary savedSalary = salaryDao.save(salary);
            CheckInOutService checkInOutService = new CheckInOutService();
            for (CheckInOut c : salary.getCheckInOuts()) {
                c.setSalaryId(savedSalary.getId());
                checkInOutService.update(c);
            }
            return savedSalary;
        } else {
           
            return salaryDao.save(salary);
        }
    }
}
