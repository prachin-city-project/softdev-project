/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.pattarapol.databaseproject.service;

import com.pattarapol.databaseproject.dao.CheckInOutDao;
import com.pattarapol.databaseproject.model.CheckInOut;
import java.util.Date;
import java.util.List;

/**
 *
 * @author sanak
 */
public class CheckInOutService {

    CheckInOutDao checkInOutDao = new CheckInOutDao();

    public CheckInOut getOne(int id) {
        return checkInOutDao.get(id);
    }

    public List<CheckInOut> getCheckInOuts() {
        return checkInOutDao.getAll();
    }

    public List<CheckInOut> getCheckInOutsHistory() {
        return checkInOutDao.getAllHistory();
    }

    public List<CheckInOut> getCheckInOutsByEmployeeId(int id) {
        return checkInOutDao.getAllByEmpId(id);
    }
    public CheckInOut addNew(CheckInOut editedCheckInOut) {
        return checkInOutDao.save(editedCheckInOut);
    }

    public CheckInOut update(CheckInOut editedCheckInOut) {
        return checkInOutDao.update(editedCheckInOut);
    }

    public int delete(CheckInOut editedCheckInOut) {
        return checkInOutDao.delete(editedCheckInOut);
    }

    public CheckInOut checkOut(CheckInOut editedCheckInOut) {
        Date now = new Date();
        editedCheckInOut.setCheckOutTime(now);
        return checkInOutDao.checkOut(editedCheckInOut);
    }
}
