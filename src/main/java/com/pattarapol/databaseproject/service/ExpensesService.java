/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.pattarapol.databaseproject.service;

import com.pattarapol.databaseproject.dao.ExpensesDao;
import com.pattarapol.databaseproject.dao.ExpensesDetailDao;
import com.pattarapol.databaseproject.model.Expenses;
import com.pattarapol.databaseproject.model.ExpensesDetail;
import com.pattarapol.databaseproject.report.ExpensesReport;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Admin
 */
public class ExpensesService {
    public Expenses getById(int id){
        ExpensesDao expensesDao = new ExpensesDao();
        return expensesDao.get(id);
    }
public List<Expenses> getExpenses() {
    ArrayList<Expenses> list = new ArrayList<>();
    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM");
    Date currentDate = new Date();
    String currentMonth = dateFormat.format(currentDate);

    ExpensesDao expensesDao = new ExpensesDao();
    return expensesDao.getAll(currentMonth);
}
  public List<Expenses> getExpenses(String begin, String end){
        ExpensesDao expensesDao = new ExpensesDao();
        return expensesDao.getAll(begin, end);
    }
  

        public List<ExpensesReport> getAllExpenses(){
        ExpensesDao expensesDao = new ExpensesDao();
        return expensesDao.getExpenses();
    }
          public List<ExpensesReport> getAllExpenses(String begin, String end){
        ExpensesDao expensesDao = new ExpensesDao();
        return expensesDao.getExpenses(begin, end);
    }
      public Expenses addNew(Expenses editedExpenses) {
        ExpensesDao expensesDao = new ExpensesDao();
        ExpensesDetailDao expensesDetailDao = new ExpensesDetailDao();
        Expenses expenses = expensesDao.save(editedExpenses);
        for(ExpensesDetail ed: editedExpenses.getExpensesDetails()){
            ed.setExpensesId(expenses.getId());
            expensesDetailDao.save(ed);
        }
        return expenses;
    }
      
      

    public Expenses update(Expenses editedExpenses) {
        if (editedExpenses != null) {
            ExpensesDao expensesDao = new ExpensesDao();
            return expensesDao.update(editedExpenses);
        } else {
            System.err.println("Cannot update a null expenses.");
            return null;
        }
    }

    public int delete(Expenses editedExpenses) {
        ExpensesDao expensesDao = new ExpensesDao();
        return expensesDao.delete(editedExpenses);
    }
}
