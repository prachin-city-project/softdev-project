/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.pattarapol.databaseproject.service;

import com.pattarapol.databaseproject.dao.CategoryDao;
import com.pattarapol.databaseproject.dao.CheckMaterialItemDao;
import com.pattarapol.databaseproject.dao.CheckStockDao;
import com.pattarapol.databaseproject.model.Category;
import com.pattarapol.databaseproject.model.CheckMaterialItem;
import com.pattarapol.databaseproject.model.CheckStock;
import com.pattarapol.databaseproject.model.Material;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author sanak
 */
public class CheckStockService {

    public List<CheckStock> getcheckStocks() {
        ArrayList<CheckStock> list = new ArrayList<>();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM");
        Date currentDate = new Date();
        String currentMonth = dateFormat.format(currentDate);

        CheckStockDao checkStockDao = new CheckStockDao();
        return checkStockDao.getAll(currentMonth);
    }

    public List<CheckStock> getcheckStocks(String begin, String end) {
        CheckStockDao checkStockDao = new CheckStockDao();
        return checkStockDao.getAll(begin, end);
    }

    public CheckStock addNew(CheckStock editedCheckStock) {
        MaterialService materialService = new MaterialService();
        CheckStockDao checkStockDao = new CheckStockDao();
        CheckMaterialItemDao checkMaterialItemDao = new CheckMaterialItemDao();
        CheckStock checkStock = checkStockDao.save(editedCheckStock);
        for (CheckMaterialItem cm : editedCheckStock.getCheckMaterialItems()) {
            try {
                Material mat = materialService.getId(cm.getMaterialId());
                mat.setRemain(cm.getCurrentQty());
                if (cm.getCurrentQty() > 0 && cm.getCurrentQty() < mat.getMin()) {
                    mat.setStatus("MinStock");
                } else if (cm.getCurrentQty() == 0) {
                    mat.setStatus("Out of Stock");
                } else {
                    mat.setStatus("Normal");
                }
                materialService.update(mat);
                cm.setCheckStockId(checkStock.getId());
                checkMaterialItemDao.save(cm);
            } catch (ValidateException ex) {
                Logger.getLogger(CheckStockService.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return checkStock;
    }

    public CheckStock update(CheckStock editedCheckStock) {
        if (editedCheckStock != null) {
            CheckStockDao checkStockDao = new CheckStockDao();
            return checkStockDao.update(editedCheckStock);
        } else {

            System.err.println("Cannot update a null checkStockerial.");
            return null;
        }
    }

    public int delete(CheckStock editedCheckStock) {
        CheckStockDao checkStockDao = new CheckStockDao();
        return checkStockDao.delete(editedCheckStock);
    }

    public CheckStock getName(String name) {
        CheckStockDao CsDao = new CheckStockDao();
        return CsDao.get(name);
    }

    public CheckStock getId(int id) {
        CheckStockDao CsDao = new CheckStockDao();
        return CsDao.get(id);
    }

}
