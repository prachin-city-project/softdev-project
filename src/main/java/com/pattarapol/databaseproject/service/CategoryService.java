/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.pattarapol.databaseproject.service;

import com.pattarapol.databaseproject.dao.CategoryDao;
import com.pattarapol.databaseproject.model.Category;
import java.util.List;

/**
 *
 * @author Windows10
 */
public class CategoryService {

    public List<Category> getCats() {
        CategoryDao catDao = new CategoryDao();
        return catDao.getAll();
    }

    public Category getName(String name) {
        CategoryDao catDao = new CategoryDao();
        return catDao.get(name);
    }

    public Category getId(int id) {
        CategoryDao catDao = new CategoryDao();
        return catDao.get(id);
    }

    public Category addNew(Category editedCategory) throws ValidateException {
        if (!editedCategory.isValid()) {
            throw new ValidateException("Category is Valid!!!");
        }
        CategoryDao catDao = new CategoryDao();
        return catDao.save(editedCategory);
    }

    public Category update(Category editedCategory)throws ValidateException{
        if (!editedCategory.isValid()) {
            throw new ValidateException("Category is Valid!!!");
        }
        if (editedCategory != null) {
            CategoryDao catDao = new CategoryDao();
            return catDao.update(editedCategory);
        } else {
            System.err.println("Cannot update a null category.");
            return null;
        }
    }

    public int delete(Category editedCategory) {
        CategoryDao catDao = new CategoryDao();
        return catDao.delete(editedCategory);
    }

}
