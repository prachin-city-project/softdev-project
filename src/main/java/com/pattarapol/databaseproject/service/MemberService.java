/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.pattarapol.databaseproject.service;

import com.pattarapol.databaseproject.dao.MemberDao;
import com.pattarapol.databaseproject.model.Member;
import com.pattarapol.databaseproject.report.MemberBuyReport;
import java.util.List;

/**
 *
 * @author Admin
 */
public class MemberService {
    
        public List<MemberBuyReport> getMemberBuy() {
        MemberDao memberDao = new MemberDao();
        return memberDao.getMemberBuy();
    }

    public List<MemberBuyReport> getMemberBuy(String begin, String end) {
       MemberDao memberDao = new MemberDao();
        return memberDao.getMemberBuy(begin, end);
    }
    
     public List<Member> getMember() {
        MemberDao memDao = new MemberDao();
        return memDao.getAll();
    }
      public Member getMemberId(int id) {
        MemberDao memDao = new MemberDao();
        return memDao.get(id);
    }
     public Member getByPhone(String phone){
        MemberDao memDao = new MemberDao();
       return  memDao.getByPhone(phone);
    }
    public Member addNew(Member editedMember)throws ValidateException{
        if(!editedMember.isValid()){
            throw new ValidateException("Member is valid!!!");
        }
        MemberDao memDao = new MemberDao();
        return memDao.save(editedMember);
    }

    public Member update(Member editedMember) throws ValidateException {
                if(!editedMember.isValid()){
            throw new ValidateException("Member is valid!!!");
        }
        if (editedMember != null) {
            MemberDao memDao = new MemberDao();
            return memDao.update(editedMember);
        } else {
            System.err.println("Cannot update a null member.");
            return null;
        }
    }

    public int delete(Member editedMember) {
        MemberDao memDao = new MemberDao();
        return memDao.delete(editedMember);
    }
}
