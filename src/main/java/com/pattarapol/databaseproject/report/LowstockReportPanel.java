/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JPanel.java to edit this template
 */
package com.pattarapol.databaseproject.report;

import com.pattarapol.databaseproject.service.MaterialService;
import com.pattarapol.databaseproject.service.ProductService;
import java.awt.Font;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Properties;
import javax.swing.table.AbstractTableModel;
import org.jdatepicker.impl.JDatePanelImpl;
import org.jdatepicker.impl.JDatePickerImpl;
import org.jdatepicker.impl.UtilDateModel;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.general.DefaultPieDataset;

/**
 *
 * @author Windows10
 */
public class LowstockReportPanel extends javax.swing.JPanel {

    private final MaterialService materialService;
    private List<LowstockReport> lowstockList;
    private AbstractTableModel model;
    private UtilDateModel model1;
    private UtilDateModel model2;
    private DefaultPieDataset pieDataset;
    private DefaultCategoryDataset barDataset;

    /**
     * Creates new form ProductReportPanel
     */
    public LowstockReportPanel() {
        initComponents();
        materialService = new MaterialService();
        lowstockList = materialService.getLowstock();
        initTable();
        initDatePicker();
//        initPieChart();
//        loadPieDataset();
//        initBarChart();
//        loadBarDataset();
    }

    private void initTable() {
        tblMat.getTableHeader().setFont(new Font("TH SarabunPSK", Font.PLAIN, 22));
        model = new AbstractTableModel() {
            String[] colNames = {"ID", "ชื่อสินค้า", "วัตถุดิบที่เหลือ", "สถานะ"};

            @Override
            public String getColumnName(int column) {
                return colNames[column];
            }

            @Override
            public int getRowCount() {
                return  lowstockList.size();
            }

            @Override
            public int getColumnCount() {
                return 4;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                LowstockReport low =  lowstockList.get(rowIndex);
                switch (columnIndex) {
                    case 0:
                        return low.getId();
                    case 1:
                        return low.getName();
                    case 2:
                        return low.getCurrentQuality();
                    case 3:
                        return low.getStatus();
                    default:
                        return "";
                }
            }
        };
        tblMat.setModel(model);
    }
    
//     private void initPieChart() {
//        pieDataset = new DefaultPieDataset();
//        JFreeChart chart = ChartFactory.createPieChart("Product Sales", pieDataset, true, true, false
//        );
//        ChartPanel chartPanel = new ChartPanel( chart );
//        pnlPieGraph.add(chartPanel);
//    }

//    private void loadPieDataset() {
//        pieDataset.clear();
//        for(ProductReport a:productList){
//            pieDataset.setValue(a.getName(), a.getTotalPrice());
//        }
//    }
//    private void initBarChart() {
//        barDataset = new DefaultCategoryDataset();
//        JFreeChart chart = ChartFactory.createBarChart("Top Ten Products", "Product Name","Total Price",barDataset,PlotOrientation.VERTICAL,true,true,false
//        );
//        ChartPanel chartPanel = new ChartPanel( chart );
//        pnlBarGraph.add(chartPanel);
//    }
//
//    private void loadBarDataset() {
//        barDataset.clear();
//        for(ProductReport a:productList){
//            barDataset.setValue(a.getTotalPrice(),"Total Price",a.getName());
//        }
//        for(ProductReport a:productList){
//            barDataset.setValue(a.getTotalQuantity(),"Total Quantity",a.getName());
//        }
//    }

    private void initDatePicker() {
        model1 = new UtilDateModel();
        Properties p1 = new Properties();
        p1.put("text.today", "Today");
        p1.put("text.month", "Month");
        p1.put("text.year", "Year");
        JDatePanelImpl datePanel1 = new JDatePanelImpl(model1, p1);
        JDatePickerImpl datePicker1 = new JDatePickerImpl(datePanel1, new DateLabelFormatter());
//        pnlDatePicker1.add(datePicker1);
        model1.setSelected(true);

        model2 = new UtilDateModel();
        Properties p2 = new Properties();
        p2.put("text.today", "Today");
        p2.put("text.month", "Month");
        p2.put("text.year", "Year");
        JDatePanelImpl datePanel2 = new JDatePanelImpl(model2, p2);
        JDatePickerImpl datePicker2 = new JDatePickerImpl(datePanel2, new DateLabelFormatter());
//        pnlDatePicker2.add(datePicker2);
        model2.setSelected(true);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblMat = new javax.swing.JTable();

        jPanel1.setBackground(new java.awt.Color(204, 204, 255));

        jLabel1.setFont(new java.awt.Font("TH SarabunPSK", 0, 36)); // NOI18N
        jLabel1.setText("วัตถุดิบใกล้หมด");

        tblMat.setFont(new java.awt.Font("TH SarabunPSK", 0, 20)); // NOI18N
        tblMat.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(tblMat);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(26, 26, 26)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 224, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 1282, Short.MAX_VALUE))
                .addGap(21, 21, 21))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(18, 18, 18)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 908, Short.MAX_VALUE)
                .addGap(23, 23, 23))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tblMat;
    // End of variables declaration//GEN-END:variables
}
