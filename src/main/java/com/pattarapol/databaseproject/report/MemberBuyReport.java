/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.pattarapol.databaseproject.report;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Admin
 */
public class MemberBuyReport {

    private int id;
    private String name;
    private int totalRecipet;
    private float totalSpent;
    private int point;

    public MemberBuyReport(int id, String name, int totalRecipet, int totalSpent, int point) {
        this.id = id;
        this.name = name;
        this.totalRecipet = totalRecipet;
        this.totalSpent = totalSpent;
        this.point = point;
    }

    public MemberBuyReport(String name, int totalRecipet, int totalSpent, int point) {
        this.id = -1;
        this.name = name;
        this.totalRecipet = totalRecipet;
        this.totalSpent = totalSpent;
        this.point = point;
    }

    public MemberBuyReport() {
        this(-1, "", 0, 0, 0);

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getTotalRecipet() {
        return totalRecipet;
    }

    public void setTotalRecipet(int totalRecipet) {
        this.totalRecipet = totalRecipet;
    }

    public float getTotalSpent() {
        return totalSpent;
    }

    public void setTotalSpent(float totalSpent) {
        this.totalSpent = totalSpent;
    }

    public int getPoint() {
        return point;
    }

    public void setPoint(int point) {
        this.point = point;
    }

    @Override
    public String toString() {
        return "MemberBuyReport{" + "id=" + id + ", name=" + name + ", totalRecipet=" + totalRecipet + ", totalSpent=" + totalSpent + ", point=" + point + '}';
    }

    public static MemberBuyReport fromRS(ResultSet rs) {
        MemberBuyReport obj = new MemberBuyReport();
        try {
            obj.setId(rs.getInt("member_id"));
            obj.setName(rs.getString("member_name"));
            obj.setTotalRecipet(rs.getInt("Total_Reciept"));
            obj.setTotalSpent(rs.getInt("Total_Spent"));
            obj.setPoint(rs.getInt("member_point"));
        } catch (SQLException ex) {
            Logger.getLogger(ProductReport.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return obj;
    }
}
