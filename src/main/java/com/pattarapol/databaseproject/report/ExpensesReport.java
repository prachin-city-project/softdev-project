/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.pattarapol.databaseproject.report;

import com.pattarapol.databaseproject.model.*;
import com.pattarapol.databaseproject.dao.EmployeeDao;
import com.pattarapol.databaseproject.dao.ExpensesDao;
import com.pattarapol.databaseproject.dao.ExpensesDetailDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Windows10
 */
public class ExpensesReport {

    private int id;
    private String name;
    private float total;
    private String date;
    private ArrayList<ExpensesDetail> expensesDetails ;

    public ExpensesReport(int id,String date,String name, float total) {
        this.id = id;
        this.date =date;
        this.name= name;
        this.total = total;
        
    }

    public ExpensesReport( float total , String name) {
        this.id = -1;
        this.date = "" ;
        this.name= name;
        this.total = total;
    }

      public ExpensesReport() {
        this( -1, "", "" , 0) ;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }




    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    public float getTotal() {
        return total;
    }

    public void setTotal(float total) {
        this.total = total;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
     





    public ArrayList<ExpensesDetail> getExpensesDetails() {
        return expensesDetails;
    }

    public void setExpensesDetails(ArrayList<ExpensesDetail> expensesDetails) {
        this.expensesDetails = expensesDetails;
    }

    @Override
    public String toString() {
        return "ExpensesReport{" + "id=" + id + ", name=" + name + ", total=" + total + ", date=" + date + ", expensesDetails=" + expensesDetails + '}';
    }

   





  



    public static ExpensesReport fromRS(ResultSet rs) {
        ExpensesReport expenses = new ExpensesReport();
        try {
            expenses.setDate(rs.getString("year_month"));
            expenses.setName(rs.getString("expenses_detail_name"));
            expenses.setTotal(rs.getFloat("total_detail_expenses"));


        } catch (SQLException ex) {
            Logger.getLogger(ExpensesReport.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return expenses;
    }
}
