/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.pattarapol.databaseproject.report;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Admin
 */
public class DailySalesReport {
    private String recieptDate;
    private float Sale;

    public DailySalesReport(String recieptDate, float Sale) {
        this.recieptDate = recieptDate;
        this.Sale = Sale;
    }
    public DailySalesReport() {
        this.recieptDate = null;
        this.Sale = 0;
    }


    public DailySalesReport(float Sale) {
        this.Sale = Sale;
    }

    public String getRecieptDate() {
        return recieptDate;
    }

    public void setRecieptDate(String recieptDate) {
        this.recieptDate = recieptDate;
    }

    public float getSale() {
        return Sale;
    }

    public void setSale(float Sale) {
        this.Sale = Sale;
    }

    @Override
    public String toString() {
        return "RecieptReport{" + "recieptDate=" + recieptDate + ", Sale=" + Sale + '}';
    }

    public static DailySalesReport fromRS(ResultSet rs) {
        DailySalesReport obj = new DailySalesReport();
        try {
            obj.setRecieptDate(rs.getString("recieptDate"));
            obj.setSale(rs.getFloat("Sale"));

        } catch (SQLException ex) {
            Logger.getLogger(DailySalesReport.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return obj;
    }
}
