/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.pattarapol.databaseproject.report;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Jib
 */
public class LowstockReport {

   private int id;
    private String name;
    private int CurrentQuality;
    private String status;

    public LowstockReport(int id, String name, int CurrentQuality, String status) {
        this.id = id;
        this.name = name;
        this.CurrentQuality = CurrentQuality;
        this.status = status;
    }
    public LowstockReport(String name, int CurrentQuality, String status) {
        this.id = -1;
        this.name = name;
        this.CurrentQuality = CurrentQuality;
        this.status = status;
    }

    public LowstockReport() {
        this( -1, " ",  0, " ") ;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCurrentQuality() {
        return CurrentQuality;
    }

    public void setCurrentQuality(int CurrentQuality) {
        this.CurrentQuality = CurrentQuality;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "LowstockReport{" + "id=" + id + ", name=" + name + ", CurrentQuality=" + CurrentQuality + ", status=" + status + '}';
    }
     public static LowstockReport fromRS(ResultSet rs) {
        LowstockReport obj = new LowstockReport();
        try {
            obj.setId(rs.getInt("mat_id"));
            obj.setName(rs.getString("mat_name"));
            obj.setCurrentQuality(rs.getInt("mat_remain"));
            obj.setStatus(rs.getString("mat_status"));
        } catch (SQLException ex) {
            Logger.getLogger(ProductReport.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return obj;
    }
}
