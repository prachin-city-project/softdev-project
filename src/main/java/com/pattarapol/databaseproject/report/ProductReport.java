/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.pattarapol.databaseproject.report;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Windows10
 */
public class ProductReport {
    private int id;
    private String name;
    private int totalQuantity;
    private float totalPrice;
    public ProductReport(int id, String name, int totalQuantity, float totalPrice) {
        this.id = id;
        this.name = name;
        this.totalQuantity = totalQuantity;
        this.totalPrice = totalPrice;
    }
    public ProductReport(String name, int totalQuantity, float totalPrice) {
        this.id = -1;
        this.name = name;
        this.totalQuantity = totalQuantity;
        this.totalPrice = totalPrice;
    }
    public ProductReport() {
        this(-1, "", 0, 0);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getTotalQuantity() {
        return totalQuantity;
    }

    public void setTotalQuantity(int totalQuantity) {
        this.totalQuantity = totalQuantity;
    }

    public float getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(float totalPrice) {
        this.totalPrice = totalPrice;
    }

    @Override
    public String toString() {
        return "ProductReport{" + "id=" + id + ", name=" + name + ", totalQuantity=" + totalQuantity + ", totalPrice=" + totalPrice + '}';
    }
    
    public static ProductReport fromRS(ResultSet rs) {
        ProductReport obj = new ProductReport();
        try {
            obj.setId(rs.getInt("product_id"));
            obj.setName(rs.getString("product_name"));
            obj.setTotalQuantity(rs.getInt("TotalQuantity"));
            obj.setTotalPrice(rs.getInt("TotalPrice"));
        } catch (SQLException ex) {
            Logger.getLogger(ProductReport.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return obj;
    }
}
